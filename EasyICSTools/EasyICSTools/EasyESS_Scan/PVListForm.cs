﻿using EpicsSharp.ChannelAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyESS_Scan
{
    public partial class PVListForm : Form
    {
        public class PV
        {
            public string name { get; set; }
            public string desc { get; set; }
            public string value { get; set; }
            public string engu { get; set; }
            public string type { get; set; }
            public string IP { get; set; }
            public string IOCName { get; set; }
            public PV()
            {
                name = string.Empty;
                desc = string.Empty;
                engu = string.Empty;
                value = string.Empty;
                type = string.Empty;
                IP = string.Empty;
                IOCName = string.Empty;
            }
        }
        public List<PV> PVs = new List<PV>();
        public PV PVItem = new PV();
        int SelectedID = -1;
        public List<int> SelectedPVID { get; set; }
        public List<string> SelectedPVName { get; set; }
        public List<string> SelectedPVType { get; set; }
        public List<string> SelectedPVIP { get; set; }

        private CAClient client2 = new CAClient();

        private string gateway = null;

        public string Gateway
        {
            get
            {
                return gateway;
            }

            set
            {
                if (value == null) return;
                client2.Configuration.SearchAddress = value;
            }
        }

        public PVListForm()
        {
            InitializeComponent();
            Application.EnableVisualStyles();
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Font = SystemFonts.DefaultFont;
             
            this.AutoScaleDimensions = new SizeF(6F, 13F); SelectedID = -1;
        }

        private void PVMenu_Opening(object sender, CancelEventArgs e)
        {

        }

        private void ReadPVValue_Click(object sender, EventArgs e)
        {
            if (SelectedID >= 0)
            {
                Gateway = FRM_IOC_IP.Text;
                //Gateway = PVs[SelectedID].IP;
                //PVs[SelectedID].type = GetPV(FRM_PVList.SelectedItems[0].Text, "INP");
                //if (PVs[SelectedID].type.Contains("T="))
                //{
                //    PVs[SelectedID].type = PVs[SelectedID].type.Remove(0, PVs[SelectedID].type.IndexOf("T=") + 2);
                //}
                //if (PVs[SelectedID].type == "FLOAT32")
                //{
                float PVVal = 0.0F;
                if (GetPVReal(FRM_PVList.SelectedItems[0].Text, "",out PVVal))
                {
                    PVs[SelectedID].value = PVVal.ToString("0.0000");
                }
                else
                {
                    PVs[SelectedID].value = "DISC";

                }

                //} else
                //{
                //    PVs[SelectedID].value = GetPV(FRM_PVList.SelectedItems[0].Text, "");
                //}
                // PVs[SelectedID].engu = GetPV(FRM_PVList.SelectedItems[0].Text, "EGU");
                PVs[SelectedID].desc = GetPV(FRM_PVList.SelectedItems[0].Text, "DESC");
                FRM_PVList.Items[FRM_PVList.SelectedItems[0].Index].SubItems[1].Text = PVs[SelectedID].value;
                FRM_PVList.Items[FRM_PVList.SelectedItems[0].Index].SubItems[2].Text = PVs[SelectedID].type;
                FRM_PVList.Items[FRM_PVList.SelectedItems[0].Index].SubItems[4].Text = PVs[SelectedID].desc;
            }

        }

        private void FRM_LoadOffline_Click(object sender, EventArgs e)
        {
            FilterTimer.Enabled = false;
            this.Close();
        }

        public string GetPV(string PVName, string Field)
        {
            string retval = "N/A";

            try
            {
                Channel<string> channel2 = client2.CreateChannel<string>(PVName + "." + Field);
                client2.Configuration.WaitTimeout = 1000;
                retval = channel2.Get<string>();
                channel2.Dispose();
            }
            catch (Exception e)
            {

            }
            return retval;

        }

        public bool GetPVReal(string PVName, string Field, out float RealVal)
        {
            bool retval = true;
            RealVal = 0.0F;
            try
            {
                Channel<float> channel2 = client2.CreateChannel<float>(PVName + "." + Field);
                client2.Configuration.WaitTimeout = 1500;
                RealVal = channel2.Get<float>();
                channel2.Dispose();
            }
            catch (Exception e)
            {
                retval = false;
            }
            return retval;

        }

        private void PVListForm_Shown(object sender, EventArgs e)
        {
            if (FRM_IOC_IP.Text != string.Empty)
            {
                Gateway = FRM_IOC_IP.Text;
            }
            FRM_SearchPV.Text = string.Empty;
            FilterTimer.Stop();
            FilterTimer.Start();
        }

        private static String WildCardToRegular(String value)
        {
            return @"^.*" + Regex.Escape(value).Replace("\\?", ".").Replace("\\*", ".*") + ".*$";
        }

        public void RefreshPVs(string Filter)
        {
            FRM_ChoosePV.Enabled = false;
            FRM_Wait.Visible = true;
            FRM_Wait.Refresh();
            Application.DoEvents();
            int Match = 0;
            if (PVs.Count > 0)
            {
                FRM_PVList.BeginUpdate();
                FRM_PVList.Items.Clear();
                for (int i = 0; i < PVs.Count; i++)
                {
  
                    if (Regex.IsMatch(PVs[i].name.ToUpper(), WildCardToRegular(Filter.ToUpper())) || (Filter == String.Empty))
                    {
                        Match++;
                        ListViewItem _Item = new ListViewItem(PVs[i].name);
                        _Item.ForeColor = Color.Black;
                        _Item.UseItemStyleForSubItems = false;

                        ListViewItem.ListViewSubItem _SubItem = new ListViewItem.ListViewSubItem(_Item, PVs[i].value);
                        _SubItem.ForeColor = Color.OrangeRed;
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, PVs[i].type);
                        _SubItem.ForeColor = Color.Navy;
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, PVs[i].engu);
                        _SubItem.ForeColor = Color.OrangeRed;
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, PVs[i].desc);
                        _SubItem.ForeColor = Color.DarkGray;
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, PVs[i].IP);
                        _SubItem.ForeColor = Color.DarkGray;
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, PVs[i].IOCName);
                        _SubItem.ForeColor = Color.DarkGray;
                        _Item.SubItems.Add(_SubItem);

                        FRM_PVList.Items.Add(_Item);
                    }
                }
                if (Match < 25)
                {
                    FRM_ReadPVs.Enabled = true;
                }
                else
                {
                    FRM_ReadPVs.Enabled = false;

                }
                FRM_PVList.EndUpdate();

            }
            FRM_Found.Text = Match.ToString() + " match found.";
            FRM_Wait.Visible = false;
            FRM_Wait.Refresh();
            Application.DoEvents();
            FRM_ChoosePV.Enabled = true;
        }

        private void FRM_SearchPV_TextChanged(object sender, EventArgs e)
        {
            FilterTimer.Stop();
            FilterTimer.Start();

        }

        private void FilterTimer_Tick(object sender, EventArgs e)
        {
            FilterTimer.Interval = 2000;
            FilterTimer.Stop();
            RefreshPVs(FRM_SearchPV.Text);

        }

        private void FRM_PVList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedID = -1;
            if (FRM_PVList.SelectedItems.Count == 1)
            {
                SelectedPVType = new List<string>();
                SelectedPVName = new List<string>();
                SelectedPVIP = new List<string>();
                SelectedPVID = new List<int>();
                PVMenu.Enabled = true;
                FRM_ChoosePV.Text = "Choose this PV";
                for (int i = 0; i < PVs.Count; i++)
                {
                    if (PVs[i].name.Equals(FRM_PVList.SelectedItems[0].Text))
                    {
                        SelectedID = i;
                        SelectedPVID.Add(i);
                        FRM_SelectedPVName.Text = FRM_PVList.SelectedItems[0].Text;
                        SelectedPVName.Add(PVs[i].name);
                        SelectedPVIP.Add(PVs[i].IP);
                    }
                }
            }
            if (FRM_PVList.SelectedItems.Count > 1)
            {
                SelectedPVType = new List<string>();
                SelectedPVName = new List<string>();
                SelectedPVIP = new List<string>();
                SelectedPVID = new List<int>();
                PVMenu.Enabled = false;
                FRM_ChoosePV.Text = "Choose these PV(s)";
                FRM_SelectedPVName.Text = "Multiple PVs (" + FRM_PVList.SelectedItems.Count.ToString() + ") selected.";
                for (int ii = 0; ii < FRM_PVList.SelectedItems.Count; ii++)
                {
                    SelectedPVName.Add(FRM_PVList.SelectedItems[ii].Text);
                    SelectedPVIP.Add(FRM_PVList.SelectedItems[ii].SubItems[5].Text);
                    SelectedPVID.Add(ii);

                }
            }
        }



        private void FRM_ReadPVs_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < FRM_PVList.Items.Count; i++)
            {
                if (FRM_IOC_IP.Text == string.Empty)
                {
                    Gateway = FRM_PVList.Items[i].SubItems[5].Text;
                } else
                {
                    //Gateway = PVs[i].IP;
                    Gateway = FRM_IOC_IP.Text;
                }
                float PVVal = 0.0F;
                string PVValue = string.Empty;
                if (GetPVReal(FRM_PVList.Items[i].SubItems[0].Text, "", out PVVal))
                {

                    PVValue = PVVal.ToString("0.0000");
                }
                else
                {
                    PVValue = "DISC";
                }
                string PVDesc = GetPV(FRM_PVList.Items[i].SubItems[0].Text, "DESC");
                string PVType = GetPV(FRM_PVList.Items[i].SubItems[0].Text, "INP");
                if (PVType.Contains("T="))
                {
                    PVType = PVType.Remove(0, PVType.IndexOf("T=") + 2);
                }
                if (PVType=="N/A")
                {
                    PVType = "Parameter";
                }
                FRM_PVList.Items[i].SubItems[1].Text = PVValue;
                FRM_PVList.Items[i].SubItems[2].Text = PVType;
                FRM_PVList.Items[i].SubItems[4].Text = PVDesc;

            }

        }

        private void FRM_PVListIOCName_Click(object sender, EventArgs e)
        {

        }

        private void copyPVNameToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (SelectedID >= 0)
            {

                System.Windows.Forms.Clipboard.SetText(FRM_PVList.SelectedItems[0].Text);
            }
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void FRM_ChoosePV_Click(object sender, EventArgs e)
        {
            FRM_SelectedPVName.Text = "Reading PV types, please wait...";
            FRM_SelectedPVName.Refresh();

            if (FRM_PVList.SelectedItems.Count == 1)
            {
                SelectedPVType = new List<string>();
                if ((SelectedID >= 0) && (SelectedID < PVs.Count))
                {
                    //Gateway = PVs[SelectedID].IP;
                    Gateway = FRM_IOC_IP.Text;
                    PVs[SelectedID].type = GetPV(FRM_PVList.SelectedItems[0].Text, "INP");
                    if (PVs[SelectedID].type.Contains("T="))
                    {
                        PVs[SelectedID].type = PVs[SelectedID].type.Remove(0, PVs[SelectedID].type.IndexOf("T=") + 2);
                    }
                    SelectedPVType.Add(PVs[SelectedID].type);

                    this.Close();
                }
            }
            if (FRM_PVList.SelectedItems.Count > 1)
            {
                SelectedPVType = new List<string>();
                SelectedPVID = new List<int>();
                //Gateway = PVs[SelectedID].IP;
                Gateway = FRM_IOC_IP.Text;
                for (int i = 0; i < PVs.Count; i++)
                {

                    for (int ii = 0; ii < SelectedPVName.Count; ii++)
                    {
                        if (PVs[i].name.Equals(SelectedPVName[ii]))
                        {
                            SelectedPVID.Add(i);
                        }
                    }
                }
                for (int ii = 0; ii < SelectedPVName.Count; ii++)
                {
                    if ((SelectedPVID[ii] >= 0) && (SelectedPVID[ii] < PVs.Count))
                    {
                        PVs[SelectedPVID[ii]].type = GetPV(FRM_PVList.SelectedItems[ii].Text, "INP");
                        if (PVs[SelectedPVID[ii]].type.Contains("T="))
                        {
                            PVs[SelectedPVID[ii]].type = PVs[SelectedPVID[ii]].type.Remove(0, PVs[SelectedPVID[ii]].type.IndexOf("T=") + 2);
                        }
                        SelectedPVType.Add(PVs[SelectedPVID[ii]].type);
                    }
                }

                this.Close();
            }
        }

        private void PVListForm_Load(object sender, EventArgs e)
        {

        }

        private void PVListForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            FilterTimer.Enabled = false;
        }

        private void FRM_CopyClipboard_Click(object sender, EventArgs e)
        {
            if (FRM_PVList.Items.Count > 0)
            {
                string Text = string.Empty;
                for (int i = 0; i < FRM_PVList.Items.Count; i++)
                {
                    Text += FRM_PVList.Items[i].Text+"\n";
                }
                System.Windows.Forms.Clipboard.SetText(Text);
                MessageBox.Show("("+FRM_PVList.Items.Count.ToString()+") PVs are succesfully copied to the clipboard!");
            }
            else
            {
                MessageBox.Show("There no PVs in the list!");
            }
        }
    }
}

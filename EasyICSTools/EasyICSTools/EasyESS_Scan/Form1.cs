﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyESS_Scan
{
    public partial class FRM_MainForm : Form
    {
        public FRM_MainForm()
        {
            InitializeComponent();
        }

        List<string> CSEntryInterfaces = new List<string>();
        public bool CheckCSEntryAvailability()
        {
            bool retval = false;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://csentry.esss.lu.se");
            request.AllowAutoRedirect = false; // find out if this site is up and don't follow a redirector
            request.Method = "HEAD";
            try
            {
               WebResponse response = request.GetResponse();
               if (response.Headers.ToString().Contains("https://csentry.esss.lu.se"))
               {
                    retval = true;
               }
           }
            catch (WebException wex)
            {

                //set flag if there was a timeout or some other issues
            }

            return retval;
        }

        private void FRM_GetCSEntry_Click(object sender, EventArgs e)
        {

            if (CheckCSEntryAvailability())
            {
                using (WebClient wc = new WebClient())
                {
                    string Token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Mjg3OTkzNjEsIm5iZiI6MTUyODc5OTM2MSwianRpIjoiMGFkNDU2MTgtODllYS00NjNjLTg2YTQtOTM4ZTFlMjQ1YzA1IiwiaWRlbnRpdHkiOjE5LCJmcmVzaCI6ZmFsc2UsInR5cGUiOiJhY2Nlc3MifQ.xHgkXSQsX1N_Gcv5xtF1AybmMv4UeZJQNovc_eppqig";
                    string URI = "https://csentry.esss.lu.se/api/v1/network/interfaces?page=11";
                    wc.Headers.Add("Content-Type", "application/json");
                    wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + Token;
                    string HtmlResult = wc.DownloadString(URI);
                    string HtmlLinks = wc.ResponseHeaders.ToString();
                    MessageBox.Show(HtmlLinks);
                    CSEntryInterfaces = HtmlLinks.Split('\n').ToList();

                    for (int i = 0; i < CSEntryInterfaces.Count; i++)
                    {
                        FRM_ListBox.Items.Add(CSEntryInterfaces[i]);
                    }


                }
            }
            else
            { MessageBox.Show("CSEntry is not accessible at the moment!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
    }
}

﻿namespace EasyESS_Scan
{
    partial class FRM_MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FRM_GetCSEntry = new System.Windows.Forms.Button();
            this.FRM_ListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // FRM_GetCSEntry
            // 
            this.FRM_GetCSEntry.Location = new System.Drawing.Point(12, 26);
            this.FRM_GetCSEntry.Name = "FRM_GetCSEntry";
            this.FRM_GetCSEntry.Size = new System.Drawing.Size(185, 53);
            this.FRM_GetCSEntry.TabIndex = 0;
            this.FRM_GetCSEntry.Text = "CSEntry";
            this.FRM_GetCSEntry.UseVisualStyleBackColor = true;
            this.FRM_GetCSEntry.Click += new System.EventHandler(this.FRM_GetCSEntry_Click);
            // 
            // FRM_ListBox
            // 
            this.FRM_ListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_ListBox.FormattingEnabled = true;
            this.FRM_ListBox.ItemHeight = 16;
            this.FRM_ListBox.Location = new System.Drawing.Point(203, 26);
            this.FRM_ListBox.Name = "FRM_ListBox";
            this.FRM_ListBox.Size = new System.Drawing.Size(634, 388);
            this.FRM_ListBox.TabIndex = 1;
            // 
            // FRM_MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 427);
            this.Controls.Add(this.FRM_ListBox);
            this.Controls.Add(this.FRM_GetCSEntry);
            this.Name = "FRM_MainForm";
            this.Text = "EasyESS Scan";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button FRM_GetCSEntry;
        private System.Windows.Forms.ListBox FRM_ListBox;
    }
}


﻿namespace EasyESS_Scan
{
    partial class FRM_MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRM_MainForm));
            this.FRM_GetCSEntry = new System.Windows.Forms.Button();
            this.FRM_LogList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.FRM_QueryPage = new System.Windows.Forms.TabPage();
            this.FRM_ProxyPort = new System.Windows.Forms.Label();
            this.FRM_ProxyURL = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.FRM_MainGateway = new System.Windows.Forms.Label();
            this.FRM_DPI = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.FRM_CCDB_WAIT = new System.Windows.Forms.PictureBox();
            this.FRM_CSEntry_WAIT = new System.Windows.Forms.PictureBox();
            this.FRM_CCDB_OK = new System.Windows.Forms.PictureBox();
            this.FRM_CSEntry_OK = new System.Windows.Forms.PictureBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.FRM_GetCCDB = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.FRM_LoadLocal_OK = new System.Windows.Forms.PictureBox();
            this.FRM_LoadOffline = new System.Windows.Forms.Button();
            this.FRM_OnlineSave = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.FRM_PVFinder = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.FRM_Header1 = new System.Windows.Forms.PictureBox();
            this.FRM_FindIOCPage = new System.Windows.Forms.TabPage();
            this.FRM_Header2 = new System.Windows.Forms.PictureBox();
            this.FRM_SearchCCDBGroup = new System.Windows.Forms.GroupBox();
            this.FRM_SearchCCDB = new System.Windows.Forms.TextBox();
            this.FRM_ListIPCs = new System.Windows.Forms.ListBox();
            this.FRM_SearchNameGroup = new System.Windows.Forms.GroupBox();
            this.FRM_SearchInterface = new System.Windows.Forms.TextBox();
            this.FRM_UserGroup = new System.Windows.Forms.GroupBox();
            this.FRM_UserEmail = new System.Windows.Forms.Label();
            this.FRM_UserName = new System.Windows.Forms.Label();
            this.FRM_UserID = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.FRM_UserCombo = new System.Windows.Forms.ComboBox();
            this.FRM_InterfaceGroup = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.FRM_IOCs = new System.Windows.Forms.ComboBox();
            this.FRM_InterfaceUpdate = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.FRM_InterfaceOwner = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.FRM_InterfaceMAC = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.FRM_InterfaceIP = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.FRM_InterfaceHost = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.FRM_InterfaceDomain = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.FRM_InterfaceName = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.FRM_InterfaceIcon = new System.Windows.Forms.PictureBox();
            this.FRM_ListInterfaces = new System.Windows.Forms.ListBox();
            this.FRM_Overall = new System.Windows.Forms.TabPage();
            this.FRM_ManualAdd = new System.Windows.Forms.Button();
            this.FRM_AddRemoveIOCGroup = new System.Windows.Forms.GroupBox();
            this.FRM_AddIOCHostName = new System.Windows.Forms.TextBox();
            this.labal121 = new System.Windows.Forms.Label();
            this.FRM_AddIOC = new System.Windows.Forms.Button();
            this.FRM_AddIOCIP = new System.Windows.Forms.TextBox();
            this.FRM_AddIOCName = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.FRM_OpenPVListArrow = new System.Windows.Forms.PictureBox();
            this.FRM_OpenPVListButton = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.FRM_FilterIOC = new System.Windows.Forms.TextBox();
            this.FRM_IOCOnlineGroup = new System.Windows.Forms.GroupBox();
            this.FRM_AnalyseWait = new System.Windows.Forms.PictureBox();
            this.IOC_HashPLC = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.IOC_HashIOC = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.IOC_IOCConnected = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.IOC_PVCount = new System.Windows.Forms.Label();
            this.IOC_IOCFactoryTag = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.IOC_HashOK = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.IOC_PLCtoIOC = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.IOC_IOCtoPLC = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.IOC_PLCFactory = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.IOC_Ping = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.FRM_OverallGroup = new System.Windows.Forms.GroupBox();
            this.FRM_OverallOwner = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.FRM_OverallMAC = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.FRM_OverallIP = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.FRM_OverallHost = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.FRM_OverallDomain = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.FRM_OverallName = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.FRM_RefreshOverall = new System.Windows.Forms.Button();
            this.FRM_Header3 = new System.Windows.Forms.PictureBox();
            this.FRM_OverallList = new System.Windows.Forms.ListView();
            this.columnHeader0 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IOCMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Menu_LookupIP = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_GetPVList = new System.Windows.Forms.ToolStripMenuItem();
            this.analyseIOCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageIOCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FRM_SMSPage = new System.Windows.Forms.TabPage();
            this.FRM_RulePanel = new System.Windows.Forms.Panel();
            this.PN_Help = new System.Windows.Forms.Button();
            this.PN_SaveRule = new System.Windows.Forms.Button();
            this.PN_NeedDisconnect = new System.Windows.Forms.CheckBox();
            this.PN_SMSText = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.PN_PhoneSet = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.PN_RuleName = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.PN_RuleErrors = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.PN_EditorPanel = new System.Windows.Forms.TextBox();
            this.PN_statusBar = new System.Windows.Forms.StatusStrip();
            this.editor = new System.Windows.Forms.ToolStripStatusLabel();
            this.FRM_RuleGroup = new System.Windows.Forms.GroupBox();
            this.FRM_RemoveRuleButton = new System.Windows.Forms.Button();
            this.FRM_AddRuleButton = new System.Windows.Forms.Button();
            this.FRM_FRM_AddRuleGroup = new System.Windows.Forms.GroupBox();
            this.label54 = new System.Windows.Forms.Label();
            this.FRM_AddNewRuleButtonCancel = new System.Windows.Forms.Button();
            this.FRM_RuleName = new System.Windows.Forms.TextBox();
            this.FRM_AddNewRuleButton = new System.Windows.Forms.Button();
            this.FRM_RuleList = new System.Windows.Forms.ListView();
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RuleMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.RuleImageList = new System.Windows.Forms.ImageList(this.components);
            this.FRM_PVManagementGroup = new System.Windows.Forms.GroupBox();
            this.FRM_AddPVGroup = new System.Windows.Forms.GroupBox();
            this.FRM_NewPVType = new System.Windows.Forms.Label();
            this.FRM_NewPVIP = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.FRM_OpenPVSearchList = new System.Windows.Forms.Button();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.FRM_AddNewPVButtonCancel = new System.Windows.Forms.Button();
            this.FRM_NewPVName = new System.Windows.Forms.TextBox();
            this.FRM_AddNewPVButton = new System.Windows.Forms.Button();
            this.FRM_RemovePV = new System.Windows.Forms.Button();
            this.FRM_AddPV = new System.Windows.Forms.Button();
            this.FRM_PVList = new System.Windows.Forms.ListView();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PVMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyPVNameToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getPVValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FRM_LoginGroup = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.FRM_FullUserName = new System.Windows.Forms.Label();
            this.FRM_LogOutButton = new System.Windows.Forms.Button();
            this.FRM_LoginButton = new System.Windows.Forms.Button();
            this.FRM_LoginPassword = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.FRM_LoginUsername = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.FRM_RedirectToWeb = new System.Windows.Forms.LinkLabel();
            this.label44 = new System.Windows.Forms.Label();
            this.FRM_TestSMSGroup = new System.Windows.Forms.GroupBox();
            this.FRM_TestSMSSend = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.FRM_TestSMSMessage = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.FRM_TestSMSPhoneNumber = new System.Windows.Forms.TextBox();
            this.FRM_PhoneBook = new System.Windows.Forms.GroupBox();
            this.FRM_NewPhoneNumberGroup = new System.Windows.Forms.GroupBox();
            this.label50 = new System.Windows.Forms.Label();
            this.FRM_NewPhoneNumber = new System.Windows.Forms.TextBox();
            this.FRM_AddNewPhoneNumberButtonCancel = new System.Windows.Forms.Button();
            this.FRM_AddNewPhoneNumberButton = new System.Windows.Forms.Button();
            this.label49 = new System.Windows.Forms.Label();
            this.FRM_NewPhoneOwner = new System.Windows.Forms.TextBox();
            this.FRM_AddPhoneSetGroup = new System.Windows.Forms.GroupBox();
            this.FRM_AddPhoneSetButtonCancel = new System.Windows.Forms.Button();
            this.FRM_AddNewPhoneSetButton = new System.Windows.Forms.Button();
            this.label48 = new System.Windows.Forms.Label();
            this.FRM_NewPhoneSetName = new System.Windows.Forms.TextBox();
            this.FRM_RemovePhoneNumber = new System.Windows.Forms.Button();
            this.FRM_AddPhoneNumber = new System.Windows.Forms.Button();
            this.FRM_RemovePhoneSetButton = new System.Windows.Forms.Button();
            this.FRM_AddPhoneSetButton = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label47 = new System.Windows.Forms.Label();
            this.FRM_PhoneSetList = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.FRM_PhoneNumberList = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PhoneMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.PhoneNumberImageList = new System.Windows.Forms.ImageList(this.components);
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.LoadTimer = new System.Windows.Forms.Timer(this.components);
            this.loadExternalPVListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.FRM_QueryPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_CCDB_WAIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_CSEntry_WAIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_CCDB_OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_CSEntry_OK)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_LoadLocal_OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Header1)).BeginInit();
            this.FRM_FindIOCPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Header2)).BeginInit();
            this.FRM_SearchCCDBGroup.SuspendLayout();
            this.FRM_SearchNameGroup.SuspendLayout();
            this.FRM_UserGroup.SuspendLayout();
            this.FRM_InterfaceGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_InterfaceIcon)).BeginInit();
            this.FRM_Overall.SuspendLayout();
            this.FRM_AddRemoveIOCGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_OpenPVListArrow)).BeginInit();
            this.FRM_IOCOnlineGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_AnalyseWait)).BeginInit();
            this.FRM_OverallGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Header3)).BeginInit();
            this.IOCMenu.SuspendLayout();
            this.FRM_SMSPage.SuspendLayout();
            this.FRM_RulePanel.SuspendLayout();
            this.PN_statusBar.SuspendLayout();
            this.FRM_RuleGroup.SuspendLayout();
            this.FRM_FRM_AddRuleGroup.SuspendLayout();
            this.RuleMenu.SuspendLayout();
            this.FRM_PVManagementGroup.SuspendLayout();
            this.FRM_AddPVGroup.SuspendLayout();
            this.PVMenu.SuspendLayout();
            this.FRM_LoginGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.FRM_TestSMSGroup.SuspendLayout();
            this.FRM_PhoneBook.SuspendLayout();
            this.FRM_NewPhoneNumberGroup.SuspendLayout();
            this.FRM_AddPhoneSetGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.PhoneMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // FRM_GetCSEntry
            // 
            this.FRM_GetCSEntry.Enabled = false;
            this.FRM_GetCSEntry.Location = new System.Drawing.Point(45, 52);
            this.FRM_GetCSEntry.Name = "FRM_GetCSEntry";
            this.FRM_GetCSEntry.Size = new System.Drawing.Size(185, 53);
            this.FRM_GetCSEntry.TabIndex = 0;
            this.FRM_GetCSEntry.Text = "CSEntry";
            this.FRM_GetCSEntry.UseVisualStyleBackColor = true;
            this.FRM_GetCSEntry.Click += new System.EventHandler(this.FRM_GetCSEntry_Click);
            // 
            // FRM_LogList
            // 
            this.FRM_LogList.AutoArrange = false;
            this.FRM_LogList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.FRM_LogList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.FRM_LogList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_LogList.FullRowSelect = true;
            this.FRM_LogList.Location = new System.Drawing.Point(0, 710);
            this.FRM_LogList.MultiSelect = false;
            this.FRM_LogList.Name = "FRM_LogList";
            this.FRM_LogList.Size = new System.Drawing.Size(1543, 150);
            this.FRM_LogList.TabIndex = 5;
            this.FRM_LogList.UseCompatibleStateImageBehavior = false;
            this.FRM_LogList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Time";
            this.columnHeader1.Width = 135;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Message";
            this.columnHeader2.Width = 760;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.FRM_QueryPage);
            this.tabControl1.Controls.Add(this.FRM_FindIOCPage);
            this.tabControl1.Controls.Add(this.FRM_Overall);
            this.tabControl1.Controls.Add(this.FRM_SMSPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1543, 710);
            this.tabControl1.TabIndex = 8;
            // 
            // FRM_QueryPage
            // 
            this.FRM_QueryPage.Controls.Add(this.FRM_ProxyPort);
            this.FRM_QueryPage.Controls.Add(this.FRM_ProxyURL);
            this.FRM_QueryPage.Controls.Add(this.label63);
            this.FRM_QueryPage.Controls.Add(this.FRM_MainGateway);
            this.FRM_QueryPage.Controls.Add(this.FRM_DPI);
            this.FRM_QueryPage.Controls.Add(this.label31);
            this.FRM_QueryPage.Controls.Add(this.groupBox2);
            this.FRM_QueryPage.Controls.Add(this.groupBox1);
            this.FRM_QueryPage.Controls.Add(this.label38);
            this.FRM_QueryPage.Controls.Add(this.FRM_PVFinder);
            this.FRM_QueryPage.Controls.Add(this.pictureBox2);
            this.FRM_QueryPage.Controls.Add(this.label40);
            this.FRM_QueryPage.Controls.Add(this.label36);
            this.FRM_QueryPage.Controls.Add(this.label5);
            this.FRM_QueryPage.Controls.Add(this.pictureBox1);
            this.FRM_QueryPage.Controls.Add(this.FRM_Header1);
            this.FRM_QueryPage.Location = new System.Drawing.Point(4, 25);
            this.FRM_QueryPage.Name = "FRM_QueryPage";
            this.FRM_QueryPage.Padding = new System.Windows.Forms.Padding(3);
            this.FRM_QueryPage.Size = new System.Drawing.Size(1535, 681);
            this.FRM_QueryPage.TabIndex = 0;
            this.FRM_QueryPage.Text = "Load Configuration from ESS tools";
            this.FRM_QueryPage.UseVisualStyleBackColor = true;
            this.FRM_QueryPage.Click += new System.EventHandler(this.FRM_QueryPage_Click);
            // 
            // FRM_ProxyPort
            // 
            this.FRM_ProxyPort.AutoSize = true;
            this.FRM_ProxyPort.BackColor = System.Drawing.Color.Transparent;
            this.FRM_ProxyPort.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_ProxyPort.ForeColor = System.Drawing.Color.Silver;
            this.FRM_ProxyPort.Location = new System.Drawing.Point(8, 385);
            this.FRM_ProxyPort.Name = "FRM_ProxyPort";
            this.FRM_ProxyPort.Size = new System.Drawing.Size(69, 16);
            this.FRM_ProxyPort.TabIndex = 52;
            this.FRM_ProxyPort.Text = "ProxyPort";
            this.FRM_ProxyPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_ProxyURL
            // 
            this.FRM_ProxyURL.AutoSize = true;
            this.FRM_ProxyURL.BackColor = System.Drawing.Color.Transparent;
            this.FRM_ProxyURL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_ProxyURL.ForeColor = System.Drawing.Color.Silver;
            this.FRM_ProxyURL.Location = new System.Drawing.Point(8, 366);
            this.FRM_ProxyURL.Name = "FRM_ProxyURL";
            this.FRM_ProxyURL.Size = new System.Drawing.Size(71, 16);
            this.FRM_ProxyURL.TabIndex = 51;
            this.FRM_ProxyURL.Text = "ProxyURL";
            this.FRM_ProxyURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label63.ForeColor = System.Drawing.Color.Navy;
            this.label63.Location = new System.Drawing.Point(754, 191);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(501, 16);
            this.label63.TabIndex = 36;
            this.label63.Text = "WARNING: by reloading CSEntry or CCDB the actual PV list will be DELETED!";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_MainGateway
            // 
            this.FRM_MainGateway.AutoSize = true;
            this.FRM_MainGateway.BackColor = System.Drawing.Color.Transparent;
            this.FRM_MainGateway.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_MainGateway.ForeColor = System.Drawing.Color.Silver;
            this.FRM_MainGateway.Location = new System.Drawing.Point(8, 335);
            this.FRM_MainGateway.Name = "FRM_MainGateway";
            this.FRM_MainGateway.Size = new System.Drawing.Size(94, 16);
            this.FRM_MainGateway.TabIndex = 50;
            this.FRM_MainGateway.Text = "MainGateway";
            this.FRM_MainGateway.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_DPI
            // 
            this.FRM_DPI.AutoSize = true;
            this.FRM_DPI.BackColor = System.Drawing.Color.Transparent;
            this.FRM_DPI.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_DPI.ForeColor = System.Drawing.Color.Silver;
            this.FRM_DPI.Location = new System.Drawing.Point(8, 308);
            this.FRM_DPI.Name = "FRM_DPI";
            this.FRM_DPI.Size = new System.Drawing.Size(213, 16);
            this.FRM_DPI.TabIndex = 49;
            this.FRM_DPI.Text = "Created 2018 by Miklos BOROS";
            this.FRM_DPI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(419, 352);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(180, 16);
            this.label31.TabIndex = 36;
            this.label31.Text = "Reload OFFLINE database";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.FRM_CCDB_WAIT);
            this.groupBox2.Controls.Add(this.FRM_CSEntry_WAIT);
            this.groupBox2.Controls.Add(this.FRM_CCDB_OK);
            this.groupBox2.Controls.Add(this.FRM_CSEntry_OK);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.FRM_GetCCDB);
            this.groupBox2.Controls.Add(this.FRM_GetCSEntry);
            this.groupBox2.Location = new System.Drawing.Point(402, 80);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(346, 227);
            this.groupBox2.TabIndex = 48;
            this.groupBox2.TabStop = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.White;
            this.label28.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(11, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(159, 16);
            this.label28.TabIndex = 35;
            this.label28.Text = "Load ONLINE database";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_CCDB_WAIT
            // 
            this.FRM_CCDB_WAIT.Image = global::EasyESS_Scan.Properties.Resources.wait;
            this.FRM_CCDB_WAIT.Location = new System.Drawing.Point(235, 150);
            this.FRM_CCDB_WAIT.Name = "FRM_CCDB_WAIT";
            this.FRM_CCDB_WAIT.Size = new System.Drawing.Size(50, 50);
            this.FRM_CCDB_WAIT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_CCDB_WAIT.TabIndex = 34;
            this.FRM_CCDB_WAIT.TabStop = false;
            this.FRM_CCDB_WAIT.Visible = false;
            // 
            // FRM_CSEntry_WAIT
            // 
            this.FRM_CSEntry_WAIT.Image = global::EasyESS_Scan.Properties.Resources.wait;
            this.FRM_CSEntry_WAIT.Location = new System.Drawing.Point(235, 54);
            this.FRM_CSEntry_WAIT.Name = "FRM_CSEntry_WAIT";
            this.FRM_CSEntry_WAIT.Size = new System.Drawing.Size(50, 50);
            this.FRM_CSEntry_WAIT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_CSEntry_WAIT.TabIndex = 33;
            this.FRM_CSEntry_WAIT.TabStop = false;
            this.FRM_CSEntry_WAIT.Visible = false;
            // 
            // FRM_CCDB_OK
            // 
            this.FRM_CCDB_OK.Image = global::EasyESS_Scan.Properties.Resources.ok;
            this.FRM_CCDB_OK.Location = new System.Drawing.Point(235, 149);
            this.FRM_CCDB_OK.Name = "FRM_CCDB_OK";
            this.FRM_CCDB_OK.Size = new System.Drawing.Size(50, 50);
            this.FRM_CCDB_OK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_CCDB_OK.TabIndex = 32;
            this.FRM_CCDB_OK.TabStop = false;
            this.FRM_CCDB_OK.Visible = false;
            // 
            // FRM_CSEntry_OK
            // 
            this.FRM_CSEntry_OK.Image = global::EasyESS_Scan.Properties.Resources.ok;
            this.FRM_CSEntry_OK.Location = new System.Drawing.Point(235, 54);
            this.FRM_CSEntry_OK.Name = "FRM_CSEntry_OK";
            this.FRM_CSEntry_OK.Size = new System.Drawing.Size(50, 50);
            this.FRM_CSEntry_OK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_CSEntry_OK.TabIndex = 31;
            this.FRM_CSEntry_OK.TabStop = false;
            this.FRM_CSEntry_OK.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(45, 124);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(85, 16);
            this.label24.TabIndex = 30;
            this.label24.Text = "Read CCDB";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(45, 29);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(98, 16);
            this.label18.TabIndex = 29;
            this.label18.Text = "Read CSEntry";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_GetCCDB
            // 
            this.FRM_GetCCDB.Enabled = false;
            this.FRM_GetCCDB.Location = new System.Drawing.Point(45, 147);
            this.FRM_GetCCDB.Name = "FRM_GetCCDB";
            this.FRM_GetCCDB.Size = new System.Drawing.Size(185, 53);
            this.FRM_GetCCDB.TabIndex = 7;
            this.FRM_GetCCDB.Text = "CCDB";
            this.FRM_GetCCDB.UseVisualStyleBackColor = true;
            this.FRM_GetCCDB.Click += new System.EventHandler(this.FRM_GetCCDB_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.FRM_LoadLocal_OK);
            this.groupBox1.Controls.Add(this.FRM_LoadOffline);
            this.groupBox1.Controls.Add(this.FRM_OnlineSave);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Location = new System.Drawing.Point(402, 352);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(343, 179);
            this.groupBox1.TabIndex = 47;
            this.groupBox1.TabStop = false;
            // 
            // FRM_LoadLocal_OK
            // 
            this.FRM_LoadLocal_OK.Image = global::EasyESS_Scan.Properties.Resources.ok;
            this.FRM_LoadLocal_OK.Location = new System.Drawing.Point(232, 90);
            this.FRM_LoadLocal_OK.Name = "FRM_LoadLocal_OK";
            this.FRM_LoadLocal_OK.Size = new System.Drawing.Size(50, 50);
            this.FRM_LoadLocal_OK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_LoadLocal_OK.TabIndex = 40;
            this.FRM_LoadLocal_OK.TabStop = false;
            this.FRM_LoadLocal_OK.Visible = false;
            // 
            // FRM_LoadOffline
            // 
            this.FRM_LoadOffline.Location = new System.Drawing.Point(41, 90);
            this.FRM_LoadOffline.Name = "FRM_LoadOffline";
            this.FRM_LoadOffline.Size = new System.Drawing.Size(185, 53);
            this.FRM_LoadOffline.TabIndex = 39;
            this.FRM_LoadOffline.Text = "Load LOCAL";
            this.FRM_LoadOffline.UseVisualStyleBackColor = true;
            this.FRM_LoadOffline.Click += new System.EventHandler(this.FRM_LoadOffline_Click);
            // 
            // FRM_OnlineSave
            // 
            this.FRM_OnlineSave.AutoSize = true;
            this.FRM_OnlineSave.BackColor = System.Drawing.Color.Transparent;
            this.FRM_OnlineSave.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_OnlineSave.ForeColor = System.Drawing.Color.SeaGreen;
            this.FRM_OnlineSave.Location = new System.Drawing.Point(181, 57);
            this.FRM_OnlineSave.Name = "FRM_OnlineSave";
            this.FRM_OnlineSave.Size = new System.Drawing.Size(31, 16);
            this.FRM_OnlineSave.TabIndex = 38;
            this.FRM_OnlineSave.Text = "N/A";
            this.FRM_OnlineSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(39, 57);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(115, 16);
            this.label33.TabIndex = 37;
            this.label33.Text = "Last update was:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label38.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(869, 402);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(114, 24);
            this.label38.TabIndex = 45;
            this.label38.Text = "PV FINDER";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_PVFinder
            // 
            this.FRM_PVFinder.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_PVFinder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.FRM_PVFinder.Location = new System.Drawing.Point(853, 454);
            this.FRM_PVFinder.Name = "FRM_PVFinder";
            this.FRM_PVFinder.Size = new System.Drawing.Size(185, 41);
            this.FRM_PVFinder.TabIndex = 41;
            this.FRM_PVFinder.Text = "SEARCH";
            this.FRM_PVFinder.UseVisualStyleBackColor = true;
            this.FRM_PVFinder.Click += new System.EventHandler(this.FRM_PVFinder_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::EasyESS_Scan.Properties.Resources.bback;
            this.pictureBox2.Location = new System.Drawing.Point(797, 377);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(288, 154);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 44;
            this.pictureBox2.TabStop = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(772, 352);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(168, 16);
            this.label40.TabIndex = 46;
            this.label40.Text = "Click to launch PV Finder";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label36.ForeColor = System.Drawing.Color.Silver;
            this.label36.Location = new System.Drawing.Point(8, 282);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(240, 16);
            this.label36.TabIndex = 43;
            this.label36.Text = "source @ spallationsource BitBucket";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.Color.Silver;
            this.label5.Location = new System.Drawing.Point(8, 260);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(213, 16);
            this.label5.TabIndex = 42;
            this.label5.Text = "Created 2018 by Miklos BOROS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::EasyESS_Scan.Properties.Resources.ess;
            this.pictureBox1.Location = new System.Drawing.Point(8, 80);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(350, 174);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // FRM_Header1
            // 
            this.FRM_Header1.Dock = System.Windows.Forms.DockStyle.Top;
            this.FRM_Header1.Image = global::EasyESS_Scan.Properties.Resources.header_loadess;
            this.FRM_Header1.Location = new System.Drawing.Point(3, 3);
            this.FRM_Header1.Name = "FRM_Header1";
            this.FRM_Header1.Size = new System.Drawing.Size(1529, 57);
            this.FRM_Header1.TabIndex = 8;
            this.FRM_Header1.TabStop = false;
            // 
            // FRM_FindIOCPage
            // 
            this.FRM_FindIOCPage.Controls.Add(this.FRM_Header2);
            this.FRM_FindIOCPage.Controls.Add(this.FRM_SearchCCDBGroup);
            this.FRM_FindIOCPage.Controls.Add(this.FRM_ListIPCs);
            this.FRM_FindIOCPage.Controls.Add(this.FRM_SearchNameGroup);
            this.FRM_FindIOCPage.Controls.Add(this.FRM_UserGroup);
            this.FRM_FindIOCPage.Controls.Add(this.FRM_InterfaceGroup);
            this.FRM_FindIOCPage.Controls.Add(this.FRM_ListInterfaces);
            this.FRM_FindIOCPage.Location = new System.Drawing.Point(4, 25);
            this.FRM_FindIOCPage.Name = "FRM_FindIOCPage";
            this.FRM_FindIOCPage.Padding = new System.Windows.Forms.Padding(3);
            this.FRM_FindIOCPage.Size = new System.Drawing.Size(1535, 681);
            this.FRM_FindIOCPage.TabIndex = 1;
            this.FRM_FindIOCPage.Text = "Query";
            this.FRM_FindIOCPage.UseVisualStyleBackColor = true;
            this.FRM_FindIOCPage.Click += new System.EventHandler(this.FRM_FindIOCPage_Click);
            // 
            // FRM_Header2
            // 
            this.FRM_Header2.Dock = System.Windows.Forms.DockStyle.Top;
            this.FRM_Header2.Image = global::EasyESS_Scan.Properties.Resources.header_query;
            this.FRM_Header2.Location = new System.Drawing.Point(3, 3);
            this.FRM_Header2.Name = "FRM_Header2";
            this.FRM_Header2.Size = new System.Drawing.Size(1529, 57);
            this.FRM_Header2.TabIndex = 21;
            this.FRM_Header2.TabStop = false;
            // 
            // FRM_SearchCCDBGroup
            // 
            this.FRM_SearchCCDBGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_SearchCCDBGroup.Controls.Add(this.FRM_SearchCCDB);
            this.FRM_SearchCCDBGroup.Location = new System.Drawing.Point(885, 105);
            this.FRM_SearchCCDBGroup.Name = "FRM_SearchCCDBGroup";
            this.FRM_SearchCCDBGroup.Size = new System.Drawing.Size(464, 83);
            this.FRM_SearchCCDBGroup.TabIndex = 20;
            this.FRM_SearchCCDBGroup.TabStop = false;
            this.FRM_SearchCCDBGroup.Text = "Search IPC from CCDB";
            // 
            // FRM_SearchCCDB
            // 
            this.FRM_SearchCCDB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_SearchCCDB.Location = new System.Drawing.Point(11, 39);
            this.FRM_SearchCCDB.Name = "FRM_SearchCCDB";
            this.FRM_SearchCCDB.Size = new System.Drawing.Size(443, 23);
            this.FRM_SearchCCDB.TabIndex = 1;
            this.FRM_SearchCCDB.TextChanged += new System.EventHandler(this.FRM_SearchCCDB_TextChanged);
            // 
            // FRM_ListIPCs
            // 
            this.FRM_ListIPCs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_ListIPCs.FormattingEnabled = true;
            this.FRM_ListIPCs.HorizontalScrollbar = true;
            this.FRM_ListIPCs.ItemHeight = 16;
            this.FRM_ListIPCs.Location = new System.Drawing.Point(885, 194);
            this.FRM_ListIPCs.Name = "FRM_ListIPCs";
            this.FRM_ListIPCs.Size = new System.Drawing.Size(463, 340);
            this.FRM_ListIPCs.TabIndex = 19;
            this.FRM_ListIPCs.SelectedIndexChanged += new System.EventHandler(this.FRM_ListIPCs_SelectedIndexChanged);
            // 
            // FRM_SearchNameGroup
            // 
            this.FRM_SearchNameGroup.Controls.Add(this.FRM_SearchInterface);
            this.FRM_SearchNameGroup.Location = new System.Drawing.Point(8, 105);
            this.FRM_SearchNameGroup.Name = "FRM_SearchNameGroup";
            this.FRM_SearchNameGroup.Size = new System.Drawing.Size(388, 83);
            this.FRM_SearchNameGroup.TabIndex = 18;
            this.FRM_SearchNameGroup.TabStop = false;
            this.FRM_SearchNameGroup.Text = "Search Interface by name:";
            // 
            // FRM_SearchInterface
            // 
            this.FRM_SearchInterface.Location = new System.Drawing.Point(11, 39);
            this.FRM_SearchInterface.Name = "FRM_SearchInterface";
            this.FRM_SearchInterface.Size = new System.Drawing.Size(367, 23);
            this.FRM_SearchInterface.TabIndex = 1;
            this.FRM_SearchInterface.TextChanged += new System.EventHandler(this.FRM_SearchInterface_TextChanged);
            // 
            // FRM_UserGroup
            // 
            this.FRM_UserGroup.Controls.Add(this.FRM_UserEmail);
            this.FRM_UserGroup.Controls.Add(this.FRM_UserName);
            this.FRM_UserGroup.Controls.Add(this.FRM_UserID);
            this.FRM_UserGroup.Controls.Add(this.label4);
            this.FRM_UserGroup.Controls.Add(this.label3);
            this.FRM_UserGroup.Controls.Add(this.label2);
            this.FRM_UserGroup.Controls.Add(this.FRM_UserCombo);
            this.FRM_UserGroup.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_UserGroup.Location = new System.Drawing.Point(410, 105);
            this.FRM_UserGroup.Name = "FRM_UserGroup";
            this.FRM_UserGroup.Size = new System.Drawing.Size(458, 172);
            this.FRM_UserGroup.TabIndex = 16;
            this.FRM_UserGroup.TabStop = false;
            this.FRM_UserGroup.Text = "Search by owner";
            // 
            // FRM_UserEmail
            // 
            this.FRM_UserEmail.BackColor = System.Drawing.Color.Transparent;
            this.FRM_UserEmail.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_UserEmail.Location = new System.Drawing.Point(111, 71);
            this.FRM_UserEmail.Name = "FRM_UserEmail";
            this.FRM_UserEmail.Size = new System.Drawing.Size(324, 20);
            this.FRM_UserEmail.TabIndex = 15;
            this.FRM_UserEmail.Text = "N/A";
            this.FRM_UserEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_UserName
            // 
            this.FRM_UserName.BackColor = System.Drawing.Color.Transparent;
            this.FRM_UserName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_UserName.Location = new System.Drawing.Point(111, 47);
            this.FRM_UserName.Name = "FRM_UserName";
            this.FRM_UserName.Size = new System.Drawing.Size(324, 20);
            this.FRM_UserName.TabIndex = 14;
            this.FRM_UserName.Text = "N/A";
            this.FRM_UserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_UserID
            // 
            this.FRM_UserID.BackColor = System.Drawing.Color.Transparent;
            this.FRM_UserID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_UserID.Location = new System.Drawing.Point(111, 24);
            this.FRM_UserID.Name = "FRM_UserID";
            this.FRM_UserID.Size = new System.Drawing.Size(324, 17);
            this.FRM_UserID.TabIndex = 13;
            this.FRM_UserID.Text = "N/A";
            this.FRM_UserID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(13, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "email:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(13, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "username:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(13, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "ID:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_UserCombo
            // 
            this.FRM_UserCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FRM_UserCombo.FormattingEnabled = true;
            this.FRM_UserCombo.Location = new System.Drawing.Point(17, 117);
            this.FRM_UserCombo.Name = "FRM_UserCombo";
            this.FRM_UserCombo.Size = new System.Drawing.Size(418, 24);
            this.FRM_UserCombo.TabIndex = 9;
            this.FRM_UserCombo.SelectedIndexChanged += new System.EventHandler(this.FRM_UserCombo_SelectedIndexChanged);
            // 
            // FRM_InterfaceGroup
            // 
            this.FRM_InterfaceGroup.Controls.Add(this.label1);
            this.FRM_InterfaceGroup.Controls.Add(this.FRM_IOCs);
            this.FRM_InterfaceGroup.Controls.Add(this.FRM_InterfaceUpdate);
            this.FRM_InterfaceGroup.Controls.Add(this.label21);
            this.FRM_InterfaceGroup.Controls.Add(this.FRM_InterfaceOwner);
            this.FRM_InterfaceGroup.Controls.Add(this.label19);
            this.FRM_InterfaceGroup.Controls.Add(this.FRM_InterfaceMAC);
            this.FRM_InterfaceGroup.Controls.Add(this.label17);
            this.FRM_InterfaceGroup.Controls.Add(this.FRM_InterfaceIP);
            this.FRM_InterfaceGroup.Controls.Add(this.label15);
            this.FRM_InterfaceGroup.Controls.Add(this.FRM_InterfaceHost);
            this.FRM_InterfaceGroup.Controls.Add(this.label13);
            this.FRM_InterfaceGroup.Controls.Add(this.FRM_InterfaceDomain);
            this.FRM_InterfaceGroup.Controls.Add(this.label8);
            this.FRM_InterfaceGroup.Controls.Add(this.FRM_InterfaceName);
            this.FRM_InterfaceGroup.Controls.Add(this.label7);
            this.FRM_InterfaceGroup.Controls.Add(this.label9);
            this.FRM_InterfaceGroup.Controls.Add(this.label10);
            this.FRM_InterfaceGroup.Controls.Add(this.label11);
            this.FRM_InterfaceGroup.Controls.Add(this.FRM_InterfaceIcon);
            this.FRM_InterfaceGroup.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_InterfaceGroup.Location = new System.Drawing.Point(410, 294);
            this.FRM_InterfaceGroup.Name = "FRM_InterfaceGroup";
            this.FRM_InterfaceGroup.Size = new System.Drawing.Size(458, 246);
            this.FRM_InterfaceGroup.TabIndex = 17;
            this.FRM_InterfaceGroup.TabStop = false;
            this.FRM_InterfaceGroup.Text = "Interface details";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(109, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 16);
            this.label1.TabIndex = 27;
            this.label1.Text = "IOCs:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_IOCs
            // 
            this.FRM_IOCs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FRM_IOCs.FormattingEnabled = true;
            this.FRM_IOCs.Location = new System.Drawing.Point(174, 200);
            this.FRM_IOCs.Name = "FRM_IOCs";
            this.FRM_IOCs.Size = new System.Drawing.Size(261, 24);
            this.FRM_IOCs.TabIndex = 16;
            // 
            // FRM_InterfaceUpdate
            // 
            this.FRM_InterfaceUpdate.BackColor = System.Drawing.Color.Transparent;
            this.FRM_InterfaceUpdate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_InterfaceUpdate.Location = new System.Drawing.Point(196, 177);
            this.FRM_InterfaceUpdate.Name = "FRM_InterfaceUpdate";
            this.FRM_InterfaceUpdate.Size = new System.Drawing.Size(239, 20);
            this.FRM_InterfaceUpdate.TabIndex = 26;
            this.FRM_InterfaceUpdate.Text = "N/A";
            this.FRM_InterfaceUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(109, 177);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 16);
            this.label21.TabIndex = 25;
            this.label21.Text = "update:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_InterfaceOwner
            // 
            this.FRM_InterfaceOwner.BackColor = System.Drawing.Color.Transparent;
            this.FRM_InterfaceOwner.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_InterfaceOwner.Location = new System.Drawing.Point(196, 156);
            this.FRM_InterfaceOwner.Name = "FRM_InterfaceOwner";
            this.FRM_InterfaceOwner.Size = new System.Drawing.Size(239, 20);
            this.FRM_InterfaceOwner.TabIndex = 24;
            this.FRM_InterfaceOwner.Text = "N/A";
            this.FRM_InterfaceOwner.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(109, 156);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(50, 16);
            this.label19.TabIndex = 23;
            this.label19.Text = "owner:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_InterfaceMAC
            // 
            this.FRM_InterfaceMAC.BackColor = System.Drawing.Color.Transparent;
            this.FRM_InterfaceMAC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_InterfaceMAC.Location = new System.Drawing.Point(196, 135);
            this.FRM_InterfaceMAC.Name = "FRM_InterfaceMAC";
            this.FRM_InterfaceMAC.Size = new System.Drawing.Size(239, 20);
            this.FRM_InterfaceMAC.TabIndex = 22;
            this.FRM_InterfaceMAC.Text = "N/A";
            this.FRM_InterfaceMAC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(109, 135);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 16);
            this.label17.TabIndex = 21;
            this.label17.Text = "MAC:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_InterfaceIP
            // 
            this.FRM_InterfaceIP.BackColor = System.Drawing.Color.Transparent;
            this.FRM_InterfaceIP.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_InterfaceIP.Location = new System.Drawing.Point(196, 113);
            this.FRM_InterfaceIP.Name = "FRM_InterfaceIP";
            this.FRM_InterfaceIP.Size = new System.Drawing.Size(239, 20);
            this.FRM_InterfaceIP.TabIndex = 20;
            this.FRM_InterfaceIP.Text = "N/A";
            this.FRM_InterfaceIP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(109, 113);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 16);
            this.label15.TabIndex = 19;
            this.label15.Text = "IP:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_InterfaceHost
            // 
            this.FRM_InterfaceHost.BackColor = System.Drawing.Color.Transparent;
            this.FRM_InterfaceHost.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_InterfaceHost.Location = new System.Drawing.Point(196, 91);
            this.FRM_InterfaceHost.Name = "FRM_InterfaceHost";
            this.FRM_InterfaceHost.Size = new System.Drawing.Size(239, 20);
            this.FRM_InterfaceHost.TabIndex = 18;
            this.FRM_InterfaceHost.Text = "N/A";
            this.FRM_InterfaceHost.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(109, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 16);
            this.label13.TabIndex = 17;
            this.label13.Text = "host:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_InterfaceDomain
            // 
            this.FRM_InterfaceDomain.BackColor = System.Drawing.Color.Transparent;
            this.FRM_InterfaceDomain.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_InterfaceDomain.Location = new System.Drawing.Point(196, 69);
            this.FRM_InterfaceDomain.Name = "FRM_InterfaceDomain";
            this.FRM_InterfaceDomain.Size = new System.Drawing.Size(239, 20);
            this.FRM_InterfaceDomain.TabIndex = 16;
            this.FRM_InterfaceDomain.Text = "N/A";
            this.FRM_InterfaceDomain.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(109, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 16);
            this.label8.TabIndex = 15;
            this.label8.Text = "domain:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_InterfaceName
            // 
            this.FRM_InterfaceName.BackColor = System.Drawing.Color.Transparent;
            this.FRM_InterfaceName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_InterfaceName.Location = new System.Drawing.Point(196, 46);
            this.FRM_InterfaceName.Name = "FRM_InterfaceName";
            this.FRM_InterfaceName.Size = new System.Drawing.Size(239, 20);
            this.FRM_InterfaceName.TabIndex = 14;
            this.FRM_InterfaceName.Text = "N/A";
            this.FRM_InterfaceName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Turquoise;
            this.label7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(139, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(296, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "N/A";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(109, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 16);
            this.label9.TabIndex = 11;
            this.label9.Text = "name:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Turquoise;
            this.label10.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(109, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 17);
            this.label10.TabIndex = 10;
            this.label10.Text = "ID:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Turquoise;
            this.label11.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(16, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 17);
            this.label11.TabIndex = 8;
            this.label11.Text = "Interfaces";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FRM_InterfaceIcon
            // 
            this.FRM_InterfaceIcon.Image = global::EasyESS_Scan.Properties.Resources.InterfaceIcon;
            this.FRM_InterfaceIcon.Location = new System.Drawing.Point(16, 40);
            this.FRM_InterfaceIcon.Name = "FRM_InterfaceIcon";
            this.FRM_InterfaceIcon.Size = new System.Drawing.Size(92, 87);
            this.FRM_InterfaceIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_InterfaceIcon.TabIndex = 7;
            this.FRM_InterfaceIcon.TabStop = false;
            // 
            // FRM_ListInterfaces
            // 
            this.FRM_ListInterfaces.FormattingEnabled = true;
            this.FRM_ListInterfaces.ItemHeight = 16;
            this.FRM_ListInterfaces.Location = new System.Drawing.Point(8, 194);
            this.FRM_ListInterfaces.Name = "FRM_ListInterfaces";
            this.FRM_ListInterfaces.Size = new System.Drawing.Size(387, 340);
            this.FRM_ListInterfaces.TabIndex = 0;
            this.FRM_ListInterfaces.SelectedIndexChanged += new System.EventHandler(this.FRM_ListInterfaces_SelectedIndexChanged);
            // 
            // FRM_Overall
            // 
            this.FRM_Overall.Controls.Add(this.FRM_ManualAdd);
            this.FRM_Overall.Controls.Add(this.FRM_AddRemoveIOCGroup);
            this.FRM_Overall.Controls.Add(this.FRM_OpenPVListArrow);
            this.FRM_Overall.Controls.Add(this.FRM_OpenPVListButton);
            this.FRM_Overall.Controls.Add(this.label12);
            this.FRM_Overall.Controls.Add(this.FRM_FilterIOC);
            this.FRM_Overall.Controls.Add(this.FRM_IOCOnlineGroup);
            this.FRM_Overall.Controls.Add(this.FRM_OverallGroup);
            this.FRM_Overall.Controls.Add(this.FRM_RefreshOverall);
            this.FRM_Overall.Controls.Add(this.FRM_Header3);
            this.FRM_Overall.Controls.Add(this.FRM_OverallList);
            this.FRM_Overall.Location = new System.Drawing.Point(4, 25);
            this.FRM_Overall.Name = "FRM_Overall";
            this.FRM_Overall.Padding = new System.Windows.Forms.Padding(3);
            this.FRM_Overall.Size = new System.Drawing.Size(1535, 681);
            this.FRM_Overall.TabIndex = 2;
            this.FRM_Overall.Text = "Deployed IOCs";
            this.FRM_Overall.UseVisualStyleBackColor = true;
            // 
            // FRM_ManualAdd
            // 
            this.FRM_ManualAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_ManualAdd.Location = new System.Drawing.Point(1431, 329);
            this.FRM_ManualAdd.Name = "FRM_ManualAdd";
            this.FRM_ManualAdd.Size = new System.Drawing.Size(96, 23);
            this.FRM_ManualAdd.TabIndex = 32;
            this.FRM_ManualAdd.Text = "Manual Add";
            this.FRM_ManualAdd.UseVisualStyleBackColor = true;
            this.FRM_ManualAdd.Click += new System.EventHandler(this.FRM_ManualAdd_Click);
            // 
            // FRM_AddRemoveIOCGroup
            // 
            this.FRM_AddRemoveIOCGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddRemoveIOCGroup.Controls.Add(this.FRM_AddIOCHostName);
            this.FRM_AddRemoveIOCGroup.Controls.Add(this.labal121);
            this.FRM_AddRemoveIOCGroup.Controls.Add(this.FRM_AddIOC);
            this.FRM_AddRemoveIOCGroup.Controls.Add(this.FRM_AddIOCIP);
            this.FRM_AddRemoveIOCGroup.Controls.Add(this.FRM_AddIOCName);
            this.FRM_AddRemoveIOCGroup.Controls.Add(this.label61);
            this.FRM_AddRemoveIOCGroup.Controls.Add(this.label62);
            this.FRM_AddRemoveIOCGroup.Location = new System.Drawing.Point(960, 416);
            this.FRM_AddRemoveIOCGroup.Name = "FRM_AddRemoveIOCGroup";
            this.FRM_AddRemoveIOCGroup.Size = new System.Drawing.Size(422, 154);
            this.FRM_AddRemoveIOCGroup.TabIndex = 31;
            this.FRM_AddRemoveIOCGroup.TabStop = false;
            this.FRM_AddRemoveIOCGroup.Text = "Add remove IOC";
            this.FRM_AddRemoveIOCGroup.Visible = false;
            // 
            // FRM_AddIOCHostName
            // 
            this.FRM_AddIOCHostName.Location = new System.Drawing.Point(90, 86);
            this.FRM_AddIOCHostName.Name = "FRM_AddIOCHostName";
            this.FRM_AddIOCHostName.Size = new System.Drawing.Size(304, 23);
            this.FRM_AddIOCHostName.TabIndex = 26;
            // 
            // labal121
            // 
            this.labal121.AutoSize = true;
            this.labal121.BackColor = System.Drawing.Color.Transparent;
            this.labal121.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labal121.ForeColor = System.Drawing.Color.Navy;
            this.labal121.Location = new System.Drawing.Point(6, 89);
            this.labal121.Name = "labal121";
            this.labal121.Size = new System.Drawing.Size(82, 16);
            this.labal121.TabIndex = 25;
            this.labal121.Text = "Host Name:";
            this.labal121.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_AddIOC
            // 
            this.FRM_AddIOC.Location = new System.Drawing.Point(90, 115);
            this.FRM_AddIOC.Name = "FRM_AddIOC";
            this.FRM_AddIOC.Size = new System.Drawing.Size(169, 30);
            this.FRM_AddIOC.TabIndex = 24;
            this.FRM_AddIOC.Text = "ADD";
            this.FRM_AddIOC.UseVisualStyleBackColor = true;
            this.FRM_AddIOC.Click += new System.EventHandler(this.FRM_AddIOC_Click);
            // 
            // FRM_AddIOCIP
            // 
            this.FRM_AddIOCIP.Location = new System.Drawing.Point(90, 57);
            this.FRM_AddIOCIP.Name = "FRM_AddIOCIP";
            this.FRM_AddIOCIP.Size = new System.Drawing.Size(304, 23);
            this.FRM_AddIOCIP.TabIndex = 23;
            // 
            // FRM_AddIOCName
            // 
            this.FRM_AddIOCName.Location = new System.Drawing.Point(90, 28);
            this.FRM_AddIOCName.Name = "FRM_AddIOCName";
            this.FRM_AddIOCName.Size = new System.Drawing.Size(304, 23);
            this.FRM_AddIOCName.TabIndex = 22;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label61.ForeColor = System.Drawing.Color.Navy;
            this.label61.Location = new System.Drawing.Point(6, 60);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(25, 16);
            this.label61.TabIndex = 21;
            this.label61.Text = "IP:";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label62.ForeColor = System.Drawing.Color.Navy;
            this.label62.Location = new System.Drawing.Point(6, 31);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(78, 16);
            this.label62.TabIndex = 20;
            this.label62.Text = "IOC Name:";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_OpenPVListArrow
            // 
            this.FRM_OpenPVListArrow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_OpenPVListArrow.Image = global::EasyESS_Scan.Properties.Resources.arrow_rigth;
            this.FRM_OpenPVListArrow.Location = new System.Drawing.Point(925, 363);
            this.FRM_OpenPVListArrow.Name = "FRM_OpenPVListArrow";
            this.FRM_OpenPVListArrow.Size = new System.Drawing.Size(91, 47);
            this.FRM_OpenPVListArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_OpenPVListArrow.TabIndex = 30;
            this.FRM_OpenPVListArrow.TabStop = false;
            this.FRM_OpenPVListArrow.Visible = false;
            // 
            // FRM_OpenPVListButton
            // 
            this.FRM_OpenPVListButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_OpenPVListButton.Location = new System.Drawing.Point(1017, 370);
            this.FRM_OpenPVListButton.Name = "FRM_OpenPVListButton";
            this.FRM_OpenPVListButton.Size = new System.Drawing.Size(126, 32);
            this.FRM_OpenPVListButton.TabIndex = 29;
            this.FRM_OpenPVListButton.Text = "Open PV list";
            this.FRM_OpenPVListButton.UseVisualStyleBackColor = true;
            this.FRM_OpenPVListButton.Visible = false;
            this.FRM_OpenPVListButton.Click += new System.EventHandler(this.FRM_OpenPVListButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(5, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 16);
            this.label12.TabIndex = 28;
            this.label12.Text = "Filter IOC name:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_FilterIOC
            // 
            this.FRM_FilterIOC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_FilterIOC.Location = new System.Drawing.Point(144, 66);
            this.FRM_FilterIOC.Name = "FRM_FilterIOC";
            this.FRM_FilterIOC.Size = new System.Drawing.Size(1261, 23);
            this.FRM_FilterIOC.TabIndex = 27;
            this.FRM_FilterIOC.TextChanged += new System.EventHandler(this.FRM_FilterIOC_TextChanged);
            // 
            // FRM_IOCOnlineGroup
            // 
            this.FRM_IOCOnlineGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_IOCOnlineGroup.Controls.Add(this.FRM_AnalyseWait);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_HashPLC);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label39);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_HashIOC);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label37);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_IOCConnected);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label35);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_PVCount);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_IOCFactoryTag);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label34);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_HashOK);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label6);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_PLCtoIOC);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label14);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_IOCtoPLC);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label22);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_PLCFactory);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label26);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label29);
            this.FRM_IOCOnlineGroup.Controls.Add(this.IOC_Ping);
            this.FRM_IOCOnlineGroup.Controls.Add(this.label32);
            this.FRM_IOCOnlineGroup.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_IOCOnlineGroup.Location = new System.Drawing.Point(491, 329);
            this.FRM_IOCOnlineGroup.Name = "FRM_IOCOnlineGroup";
            this.FRM_IOCOnlineGroup.Size = new System.Drawing.Size(458, 242);
            this.FRM_IOCOnlineGroup.TabIndex = 25;
            this.FRM_IOCOnlineGroup.TabStop = false;
            this.FRM_IOCOnlineGroup.Text = "Selected IOC state ";
            // 
            // FRM_AnalyseWait
            // 
            this.FRM_AnalyseWait.Image = global::EasyESS_Scan.Properties.Resources.wait;
            this.FRM_AnalyseWait.Location = new System.Drawing.Point(251, 119);
            this.FRM_AnalyseWait.Name = "FRM_AnalyseWait";
            this.FRM_AnalyseWait.Size = new System.Drawing.Size(84, 79);
            this.FRM_AnalyseWait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_AnalyseWait.TabIndex = 35;
            this.FRM_AnalyseWait.TabStop = false;
            this.FRM_AnalyseWait.Visible = false;
            // 
            // IOC_HashPLC
            // 
            this.IOC_HashPLC.BackColor = System.Drawing.Color.Transparent;
            this.IOC_HashPLC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_HashPLC.ForeColor = System.Drawing.Color.Green;
            this.IOC_HashPLC.Location = new System.Drawing.Point(150, 219);
            this.IOC_HashPLC.Name = "IOC_HashPLC";
            this.IOC_HashPLC.Size = new System.Drawing.Size(285, 20);
            this.IOC_HashPLC.TabIndex = 32;
            this.IOC_HashPLC.Text = "N/A";
            this.IOC_HashPLC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(66, 219);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(74, 16);
            this.label39.TabIndex = 31;
            this.label39.Text = "PLC hash:";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IOC_HashIOC
            // 
            this.IOC_HashIOC.BackColor = System.Drawing.Color.Transparent;
            this.IOC_HashIOC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_HashIOC.ForeColor = System.Drawing.Color.Green;
            this.IOC_HashIOC.Location = new System.Drawing.Point(150, 198);
            this.IOC_HashIOC.Name = "IOC_HashIOC";
            this.IOC_HashIOC.Size = new System.Drawing.Size(285, 20);
            this.IOC_HashIOC.TabIndex = 30;
            this.IOC_HashIOC.Text = "N/A";
            this.IOC_HashIOC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(66, 198);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(72, 16);
            this.label37.TabIndex = 29;
            this.label37.Text = "IOC hash:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IOC_IOCConnected
            // 
            this.IOC_IOCConnected.BackColor = System.Drawing.Color.Transparent;
            this.IOC_IOCConnected.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_IOCConnected.ForeColor = System.Drawing.Color.Green;
            this.IOC_IOCConnected.Location = new System.Drawing.Point(174, 135);
            this.IOC_IOCConnected.Name = "IOC_IOCConnected";
            this.IOC_IOCConnected.Size = new System.Drawing.Size(261, 20);
            this.IOC_IOCConnected.TabIndex = 28;
            this.IOC_IOCConnected.Text = "N/A";
            this.IOC_IOCConnected.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(42, 135);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(107, 16);
            this.label35.TabIndex = 27;
            this.label35.Text = "IOC connected:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IOC_PVCount
            // 
            this.IOC_PVCount.BackColor = System.Drawing.Color.Transparent;
            this.IOC_PVCount.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_PVCount.ForeColor = System.Drawing.Color.Green;
            this.IOC_PVCount.Location = new System.Drawing.Point(107, 48);
            this.IOC_PVCount.Name = "IOC_PVCount";
            this.IOC_PVCount.Size = new System.Drawing.Size(328, 20);
            this.IOC_PVCount.TabIndex = 16;
            this.IOC_PVCount.Text = "N/A";
            this.IOC_PVCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // IOC_IOCFactoryTag
            // 
            this.IOC_IOCFactoryTag.BackColor = System.Drawing.Color.Transparent;
            this.IOC_IOCFactoryTag.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_IOCFactoryTag.ForeColor = System.Drawing.Color.Green;
            this.IOC_IOCFactoryTag.Location = new System.Drawing.Point(168, 156);
            this.IOC_IOCFactoryTag.Name = "IOC_IOCFactoryTag";
            this.IOC_IOCFactoryTag.Size = new System.Drawing.Size(267, 20);
            this.IOC_IOCFactoryTag.TabIndex = 26;
            this.IOC_IOCFactoryTag.Text = "N/A";
            this.IOC_IOCFactoryTag.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(42, 156);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(109, 16);
            this.label34.TabIndex = 25;
            this.label34.Text = "IOCFactory tag:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IOC_HashOK
            // 
            this.IOC_HashOK.BackColor = System.Drawing.Color.Transparent;
            this.IOC_HashOK.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_HashOK.ForeColor = System.Drawing.Color.Green;
            this.IOC_HashOK.Location = new System.Drawing.Point(125, 178);
            this.IOC_HashOK.Name = "IOC_HashOK";
            this.IOC_HashOK.Size = new System.Drawing.Size(310, 20);
            this.IOC_HashOK.TabIndex = 24;
            this.IOC_HashOK.Text = "N/A";
            this.IOC_HashOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(42, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 23;
            this.label6.Text = "HashOK:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IOC_PLCtoIOC
            // 
            this.IOC_PLCtoIOC.BackColor = System.Drawing.Color.Transparent;
            this.IOC_PLCtoIOC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_PLCtoIOC.ForeColor = System.Drawing.Color.Green;
            this.IOC_PLCtoIOC.Location = new System.Drawing.Point(226, 114);
            this.IOC_PLCtoIOC.Name = "IOC_PLCtoIOC";
            this.IOC_PLCtoIOC.Size = new System.Drawing.Size(209, 20);
            this.IOC_PLCtoIOC.TabIndex = 22;
            this.IOC_PLCtoIOC.Text = "N/A";
            this.IOC_PLCtoIOC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(42, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(150, 16);
            this.label14.TabIndex = 21;
            this.label14.Text = "PLC->IOC connection:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IOC_IOCtoPLC
            // 
            this.IOC_IOCtoPLC.BackColor = System.Drawing.Color.Transparent;
            this.IOC_IOCtoPLC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_IOCtoPLC.ForeColor = System.Drawing.Color.Green;
            this.IOC_IOCtoPLC.Location = new System.Drawing.Point(226, 92);
            this.IOC_IOCtoPLC.Name = "IOC_IOCtoPLC";
            this.IOC_IOCtoPLC.Size = new System.Drawing.Size(209, 20);
            this.IOC_IOCtoPLC.TabIndex = 20;
            this.IOC_IOCtoPLC.Text = "N/A";
            this.IOC_IOCtoPLC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(42, 92);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(150, 16);
            this.label22.TabIndex = 19;
            this.label22.Text = "IOC->PLC connection:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IOC_PLCFactory
            // 
            this.IOC_PLCFactory.BackColor = System.Drawing.Color.Transparent;
            this.IOC_PLCFactory.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_PLCFactory.ForeColor = System.Drawing.Color.Green;
            this.IOC_PLCFactory.Location = new System.Drawing.Point(164, 70);
            this.IOC_PLCFactory.Name = "IOC_PLCFactory";
            this.IOC_PLCFactory.Size = new System.Drawing.Size(271, 20);
            this.IOC_PLCFactory.TabIndex = 18;
            this.IOC_PLCFactory.Text = "N/A";
            this.IOC_PLCFactory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(20, 70);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(116, 16);
            this.label26.TabIndex = 17;
            this.label26.Text = "PLCFactory IOC:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(20, 48);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 16);
            this.label29.TabIndex = 15;
            this.label29.Text = "PV count:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IOC_Ping
            // 
            this.IOC_Ping.BackColor = System.Drawing.Color.Transparent;
            this.IOC_Ping.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IOC_Ping.ForeColor = System.Drawing.Color.Green;
            this.IOC_Ping.Location = new System.Drawing.Point(72, 25);
            this.IOC_Ping.Name = "IOC_Ping";
            this.IOC_Ping.Size = new System.Drawing.Size(363, 20);
            this.IOC_Ping.TabIndex = 14;
            this.IOC_Ping.Text = "N/A";
            this.IOC_Ping.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(20, 25);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(40, 16);
            this.label32.TabIndex = 11;
            this.label32.Text = "Ping:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_OverallGroup
            // 
            this.FRM_OverallGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_OverallGroup.Controls.Add(this.FRM_OverallOwner);
            this.FRM_OverallGroup.Controls.Add(this.label16);
            this.FRM_OverallGroup.Controls.Add(this.FRM_OverallMAC);
            this.FRM_OverallGroup.Controls.Add(this.label20);
            this.FRM_OverallGroup.Controls.Add(this.FRM_OverallIP);
            this.FRM_OverallGroup.Controls.Add(this.label23);
            this.FRM_OverallGroup.Controls.Add(this.FRM_OverallHost);
            this.FRM_OverallGroup.Controls.Add(this.label25);
            this.FRM_OverallGroup.Controls.Add(this.FRM_OverallDomain);
            this.FRM_OverallGroup.Controls.Add(this.label27);
            this.FRM_OverallGroup.Controls.Add(this.FRM_OverallName);
            this.FRM_OverallGroup.Controls.Add(this.label30);
            this.FRM_OverallGroup.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_OverallGroup.Location = new System.Drawing.Point(8, 329);
            this.FRM_OverallGroup.Name = "FRM_OverallGroup";
            this.FRM_OverallGroup.Size = new System.Drawing.Size(463, 242);
            this.FRM_OverallGroup.TabIndex = 18;
            this.FRM_OverallGroup.TabStop = false;
            this.FRM_OverallGroup.Text = "Selected IOC configuration ";
            // 
            // FRM_OverallOwner
            // 
            this.FRM_OverallOwner.BackColor = System.Drawing.Color.Transparent;
            this.FRM_OverallOwner.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_OverallOwner.Location = new System.Drawing.Point(157, 135);
            this.FRM_OverallOwner.Name = "FRM_OverallOwner";
            this.FRM_OverallOwner.Size = new System.Drawing.Size(282, 20);
            this.FRM_OverallOwner.TabIndex = 24;
            this.FRM_OverallOwner.Text = "N/A";
            this.FRM_OverallOwner.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(24, 135);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 16);
            this.label16.TabIndex = 23;
            this.label16.Text = "CSEntry owner:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_OverallMAC
            // 
            this.FRM_OverallMAC.BackColor = System.Drawing.Color.Transparent;
            this.FRM_OverallMAC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_OverallMAC.Location = new System.Drawing.Point(111, 114);
            this.FRM_OverallMAC.Name = "FRM_OverallMAC";
            this.FRM_OverallMAC.Size = new System.Drawing.Size(328, 20);
            this.FRM_OverallMAC.TabIndex = 22;
            this.FRM_OverallMAC.Text = "N/A";
            this.FRM_OverallMAC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(24, 114);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 16);
            this.label20.TabIndex = 21;
            this.label20.Text = "MAC:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_OverallIP
            // 
            this.FRM_OverallIP.BackColor = System.Drawing.Color.Transparent;
            this.FRM_OverallIP.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_OverallIP.Location = new System.Drawing.Point(111, 92);
            this.FRM_OverallIP.Name = "FRM_OverallIP";
            this.FRM_OverallIP.Size = new System.Drawing.Size(328, 20);
            this.FRM_OverallIP.TabIndex = 20;
            this.FRM_OverallIP.Text = "N/A";
            this.FRM_OverallIP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(24, 92);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 16);
            this.label23.TabIndex = 19;
            this.label23.Text = "IP:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_OverallHost
            // 
            this.FRM_OverallHost.BackColor = System.Drawing.Color.Transparent;
            this.FRM_OverallHost.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_OverallHost.Location = new System.Drawing.Point(111, 70);
            this.FRM_OverallHost.Name = "FRM_OverallHost";
            this.FRM_OverallHost.Size = new System.Drawing.Size(328, 20);
            this.FRM_OverallHost.TabIndex = 18;
            this.FRM_OverallHost.Text = "N/A";
            this.FRM_OverallHost.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(24, 70);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 16);
            this.label25.TabIndex = 17;
            this.label25.Text = "Host:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_OverallDomain
            // 
            this.FRM_OverallDomain.BackColor = System.Drawing.Color.Transparent;
            this.FRM_OverallDomain.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_OverallDomain.Location = new System.Drawing.Point(111, 48);
            this.FRM_OverallDomain.Name = "FRM_OverallDomain";
            this.FRM_OverallDomain.Size = new System.Drawing.Size(328, 20);
            this.FRM_OverallDomain.TabIndex = 16;
            this.FRM_OverallDomain.Text = "N/A";
            this.FRM_OverallDomain.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(24, 48);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(60, 16);
            this.label27.TabIndex = 15;
            this.label27.Text = "Domain:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_OverallName
            // 
            this.FRM_OverallName.BackColor = System.Drawing.Color.Transparent;
            this.FRM_OverallName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_OverallName.Location = new System.Drawing.Point(111, 25);
            this.FRM_OverallName.Name = "FRM_OverallName";
            this.FRM_OverallName.Size = new System.Drawing.Size(328, 20);
            this.FRM_OverallName.TabIndex = 14;
            this.FRM_OverallName.Text = "N/A";
            this.FRM_OverallName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(24, 25);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(49, 16);
            this.label30.TabIndex = 11;
            this.label30.Text = "Name:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_RefreshOverall
            // 
            this.FRM_RefreshOverall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_RefreshOverall.Location = new System.Drawing.Point(1411, 64);
            this.FRM_RefreshOverall.Name = "FRM_RefreshOverall";
            this.FRM_RefreshOverall.Size = new System.Drawing.Size(116, 32);
            this.FRM_RefreshOverall.TabIndex = 7;
            this.FRM_RefreshOverall.Text = "Refresh";
            this.FRM_RefreshOverall.UseVisualStyleBackColor = true;
            this.FRM_RefreshOverall.Click += new System.EventHandler(this.FRM_RefreshOverall_Click);
            // 
            // FRM_Header3
            // 
            this.FRM_Header3.Dock = System.Windows.Forms.DockStyle.Top;
            this.FRM_Header3.Image = global::EasyESS_Scan.Properties.Resources.header_iocs;
            this.FRM_Header3.Location = new System.Drawing.Point(3, 3);
            this.FRM_Header3.Name = "FRM_Header3";
            this.FRM_Header3.Size = new System.Drawing.Size(1529, 57);
            this.FRM_Header3.TabIndex = 26;
            this.FRM_Header3.TabStop = false;
            // 
            // FRM_OverallList
            // 
            this.FRM_OverallList.AllowColumnReorder = true;
            this.FRM_OverallList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_OverallList.AutoArrange = false;
            this.FRM_OverallList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader0,
            this.columnHeader8,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.FRM_OverallList.ContextMenuStrip = this.IOCMenu;
            this.FRM_OverallList.FullRowSelect = true;
            this.FRM_OverallList.Location = new System.Drawing.Point(3, 102);
            this.FRM_OverallList.MultiSelect = false;
            this.FRM_OverallList.Name = "FRM_OverallList";
            this.FRM_OverallList.Size = new System.Drawing.Size(1524, 221);
            this.FRM_OverallList.TabIndex = 6;
            this.FRM_OverallList.UseCompatibleStateImageBehavior = false;
            this.FRM_OverallList.View = System.Windows.Forms.View.Details;
            this.FRM_OverallList.SelectedIndexChanged += new System.EventHandler(this.FRM_OverallList_SelectedIndexChanged);
            // 
            // columnHeader0
            // 
            this.columnHeader0.Text = "IOC name";
            this.columnHeader0.Width = 251;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Contained in IPC";
            this.columnHeader8.Width = 143;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Hosted by CSEntry";
            this.columnHeader4.Width = 163;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Host name";
            this.columnHeader5.Width = 204;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "IP address";
            this.columnHeader6.Width = 147;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Description";
            this.columnHeader7.Width = 255;
            // 
            // IOCMenu
            // 
            this.IOCMenu.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.IOCMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_LookupIP,
            this.Menu_GetPVList,
            this.analyseIOCToolStripMenuItem,
            this.manageIOCToolStripMenuItem,
            this.loadExternalPVListToolStripMenuItem});
            this.IOCMenu.Name = "PVMenu";
            this.IOCMenu.Size = new System.Drawing.Size(204, 256);
            this.IOCMenu.Opening += new System.ComponentModel.CancelEventHandler(this.IOCMenu_Opening);
            // 
            // Menu_LookupIP
            // 
            this.Menu_LookupIP.Image = global::EasyESS_Scan.Properties.Resources.ping;
            this.Menu_LookupIP.Name = "Menu_LookupIP";
            this.Menu_LookupIP.Size = new System.Drawing.Size(203, 46);
            this.Menu_LookupIP.Text = "Lookup IP";
            this.Menu_LookupIP.Click += new System.EventHandler(this.Menu_LookupIP_Click);
            // 
            // Menu_GetPVList
            // 
            this.Menu_GetPVList.Image = global::EasyESS_Scan.Properties.Resources.pvlist1;
            this.Menu_GetPVList.Name = "Menu_GetPVList";
            this.Menu_GetPVList.Size = new System.Drawing.Size(203, 46);
            this.Menu_GetPVList.Text = "Get PV list";
            this.Menu_GetPVList.Click += new System.EventHandler(this.Menu_GetPVList_Click);
            // 
            // analyseIOCToolStripMenuItem
            // 
            this.analyseIOCToolStripMenuItem.Image = global::EasyESS_Scan.Properties.Resources.analysis;
            this.analyseIOCToolStripMenuItem.Name = "analyseIOCToolStripMenuItem";
            this.analyseIOCToolStripMenuItem.Size = new System.Drawing.Size(203, 46);
            this.analyseIOCToolStripMenuItem.Text = "Analyse IOC";
            this.analyseIOCToolStripMenuItem.Click += new System.EventHandler(this.analyseIOCToolStripMenuItem_Click);
            // 
            // manageIOCToolStripMenuItem
            // 
            this.manageIOCToolStripMenuItem.Image = global::EasyESS_Scan.Properties.Resources.manage;
            this.manageIOCToolStripMenuItem.Name = "manageIOCToolStripMenuItem";
            this.manageIOCToolStripMenuItem.Size = new System.Drawing.Size(203, 46);
            this.manageIOCToolStripMenuItem.Text = "Manage IOC";
            this.manageIOCToolStripMenuItem.Click += new System.EventHandler(this.manageIOCToolStripMenuItem_Click);
            // 
            // FRM_SMSPage
            // 
            this.FRM_SMSPage.Controls.Add(this.FRM_RulePanel);
            this.FRM_SMSPage.Controls.Add(this.FRM_RuleGroup);
            this.FRM_SMSPage.Controls.Add(this.FRM_PVManagementGroup);
            this.FRM_SMSPage.Controls.Add(this.FRM_LoginGroup);
            this.FRM_SMSPage.Controls.Add(this.FRM_RedirectToWeb);
            this.FRM_SMSPage.Controls.Add(this.label44);
            this.FRM_SMSPage.Controls.Add(this.FRM_TestSMSGroup);
            this.FRM_SMSPage.Controls.Add(this.FRM_PhoneBook);
            this.FRM_SMSPage.Location = new System.Drawing.Point(4, 25);
            this.FRM_SMSPage.Name = "FRM_SMSPage";
            this.FRM_SMSPage.Padding = new System.Windows.Forms.Padding(3);
            this.FRM_SMSPage.Size = new System.Drawing.Size(1535, 681);
            this.FRM_SMSPage.TabIndex = 3;
            this.FRM_SMSPage.Text = "Easy SMS for ESS";
            this.FRM_SMSPage.UseVisualStyleBackColor = true;
            this.FRM_SMSPage.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // FRM_RulePanel
            // 
            this.FRM_RulePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_RulePanel.BackColor = System.Drawing.Color.MidnightBlue;
            this.FRM_RulePanel.Controls.Add(this.PN_Help);
            this.FRM_RulePanel.Controls.Add(this.PN_SaveRule);
            this.FRM_RulePanel.Controls.Add(this.PN_NeedDisconnect);
            this.FRM_RulePanel.Controls.Add(this.PN_SMSText);
            this.FRM_RulePanel.Controls.Add(this.label58);
            this.FRM_RulePanel.Controls.Add(this.PN_PhoneSet);
            this.FRM_RulePanel.Controls.Add(this.label57);
            this.FRM_RulePanel.Controls.Add(this.PN_RuleName);
            this.FRM_RulePanel.Controls.Add(this.label56);
            this.FRM_RulePanel.Controls.Add(this.PN_RuleErrors);
            this.FRM_RulePanel.Controls.Add(this.label55);
            this.FRM_RulePanel.Controls.Add(this.label53);
            this.FRM_RulePanel.Controls.Add(this.PN_EditorPanel);
            this.FRM_RulePanel.Controls.Add(this.PN_statusBar);
            this.FRM_RulePanel.Location = new System.Drawing.Point(11, 18);
            this.FRM_RulePanel.Name = "FRM_RulePanel";
            this.FRM_RulePanel.Size = new System.Drawing.Size(275, 657);
            this.FRM_RulePanel.TabIndex = 41;
            this.FRM_RulePanel.Visible = false;
            // 
            // PN_Help
            // 
            this.PN_Help.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_Help.BackgroundImage = global::EasyESS_Scan.Properties.Resources.help;
            this.PN_Help.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PN_Help.Location = new System.Drawing.Point(196, 567);
            this.PN_Help.Name = "PN_Help";
            this.PN_Help.Size = new System.Drawing.Size(60, 70);
            this.PN_Help.TabIndex = 60;
            this.toolTip1.SetToolTip(this.PN_Help, "Help for Rule Editor");
            this.PN_Help.UseVisualStyleBackColor = true;
            // 
            // PN_SaveRule
            // 
            this.PN_SaveRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_SaveRule.BackgroundImage = global::EasyESS_Scan.Properties.Resources.ok;
            this.PN_SaveRule.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PN_SaveRule.Location = new System.Drawing.Point(196, 510);
            this.PN_SaveRule.Name = "PN_SaveRule";
            this.PN_SaveRule.Size = new System.Drawing.Size(60, 55);
            this.PN_SaveRule.TabIndex = 54;
            this.toolTip1.SetToolTip(this.PN_SaveRule, "Save Rule");
            this.PN_SaveRule.UseVisualStyleBackColor = true;
            this.PN_SaveRule.Click += new System.EventHandler(this.PN_SaveRule_Click);
            // 
            // PN_NeedDisconnect
            // 
            this.PN_NeedDisconnect.AutoSize = true;
            this.PN_NeedDisconnect.ForeColor = System.Drawing.Color.White;
            this.PN_NeedDisconnect.Location = new System.Drawing.Point(237, 549);
            this.PN_NeedDisconnect.Name = "PN_NeedDisconnect";
            this.PN_NeedDisconnect.Size = new System.Drawing.Size(192, 20);
            this.PN_NeedDisconnect.TabIndex = 65;
            this.PN_NeedDisconnect.Text = "Send SMS if disconnected";
            this.PN_NeedDisconnect.UseVisualStyleBackColor = true;
            // 
            // PN_SMSText
            // 
            this.PN_SMSText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_SMSText.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PN_SMSText.ForeColor = System.Drawing.Color.DarkGreen;
            this.PN_SMSText.Location = new System.Drawing.Point(21, 571);
            this.PN_SMSText.Multiline = true;
            this.PN_SMSText.Name = "PN_SMSText";
            this.PN_SMSText.Size = new System.Drawing.Size(165, 64);
            this.PN_SMSText.TabIndex = 64;
            this.PN_SMSText.TextChanged += new System.EventHandler(this.PN_SMSText_TextChanged);
            // 
            // label58
            // 
            this.label58.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("BankGothic Lt BT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label58.ForeColor = System.Drawing.Color.White;
            this.label58.Location = new System.Drawing.Point(18, 552);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(157, 14);
            this.label58.TabIndex = 63;
            this.label58.Text = "SMS text to send";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PN_PhoneSet
            // 
            this.PN_PhoneSet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_PhoneSet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PN_PhoneSet.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PN_PhoneSet.FormattingEnabled = true;
            this.PN_PhoneSet.Location = new System.Drawing.Point(21, 512);
            this.PN_PhoneSet.Name = "PN_PhoneSet";
            this.PN_PhoneSet.Size = new System.Drawing.Size(166, 26);
            this.PN_PhoneSet.TabIndex = 62;
            this.toolTip1.SetToolTip(this.PN_PhoneSet, "Choose stored phonesets");
            // 
            // label57
            // 
            this.label57.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("BankGothic Lt BT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label57.ForeColor = System.Drawing.Color.White;
            this.label57.Location = new System.Drawing.Point(19, 494);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(339, 14);
            this.label57.TabIndex = 61;
            this.label57.Text = "PhoneSet to trigger if @RULE is TRUE";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PN_RuleName
            // 
            this.PN_RuleName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PN_RuleName.Font = new System.Drawing.Font("BankGothic Lt BT", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PN_RuleName.ForeColor = System.Drawing.Color.White;
            this.PN_RuleName.Location = new System.Drawing.Point(210, 19);
            this.PN_RuleName.Name = "PN_RuleName";
            this.PN_RuleName.Size = new System.Drawing.Size(274, 18);
            this.PN_RuleName.TabIndex = 59;
            this.PN_RuleName.Text = "RuleName";
            this.PN_RuleName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label56
            // 
            this.label56.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("BankGothic Lt BT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label56.ForeColor = System.Drawing.Color.White;
            this.label56.Location = new System.Drawing.Point(169, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(89, 14);
            this.label56.TabIndex = 58;
            this.label56.Text = "RuleName";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PN_RuleErrors
            // 
            this.PN_RuleErrors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_RuleErrors.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PN_RuleErrors.ForeColor = System.Drawing.Color.Red;
            this.PN_RuleErrors.Location = new System.Drawing.Point(21, 409);
            this.PN_RuleErrors.Multiline = true;
            this.PN_RuleErrors.Name = "PN_RuleErrors";
            this.PN_RuleErrors.ReadOnly = true;
            this.PN_RuleErrors.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.PN_RuleErrors.Size = new System.Drawing.Size(233, 68);
            this.PN_RuleErrors.TabIndex = 57;
            // 
            // label55
            // 
            this.label55.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("BankGothic Lt BT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label55.ForeColor = System.Drawing.Color.White;
            this.label55.Location = new System.Drawing.Point(18, 388);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(151, 14);
            this.label55.TabIndex = 56;
            this.label55.Text = "Syntax messages";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("BankGothic Lt BT", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(12, 5);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(198, 28);
            this.label53.TabIndex = 55;
            this.label53.Text = "Rule Editor";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PN_EditorPanel
            // 
            this.PN_EditorPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_EditorPanel.Location = new System.Drawing.Point(21, 41);
            this.PN_EditorPanel.Multiline = true;
            this.PN_EditorPanel.Name = "PN_EditorPanel";
            this.PN_EditorPanel.Size = new System.Drawing.Size(235, 317);
            this.PN_EditorPanel.TabIndex = 2;
            this.PN_EditorPanel.Text = resources.GetString("PN_EditorPanel.Text");
            this.PN_EditorPanel.WordWrap = false;
            // 
            // PN_statusBar
            // 
            this.PN_statusBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_statusBar.AutoSize = false;
            this.PN_statusBar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.PN_statusBar.Dock = System.Windows.Forms.DockStyle.None;
            this.PN_statusBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.PN_statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editor});
            this.PN_statusBar.Location = new System.Drawing.Point(21, 360);
            this.PN_statusBar.Name = "PN_statusBar";
            this.PN_statusBar.Size = new System.Drawing.Size(235, 25);
            this.PN_statusBar.TabIndex = 1;
            this.PN_statusBar.Text = "statusStrip1";
            // 
            // editor
            // 
            this.editor.Name = "editor";
            this.editor.Size = new System.Drawing.Size(0, 20);
            // 
            // FRM_RuleGroup
            // 
            this.FRM_RuleGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_RuleGroup.Controls.Add(this.FRM_RemoveRuleButton);
            this.FRM_RuleGroup.Controls.Add(this.FRM_AddRuleButton);
            this.FRM_RuleGroup.Controls.Add(this.FRM_FRM_AddRuleGroup);
            this.FRM_RuleGroup.Controls.Add(this.FRM_RuleList);
            this.FRM_RuleGroup.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_RuleGroup.ForeColor = System.Drawing.Color.Navy;
            this.FRM_RuleGroup.Location = new System.Drawing.Point(1039, 171);
            this.FRM_RuleGroup.Name = "FRM_RuleGroup";
            this.FRM_RuleGroup.Size = new System.Drawing.Size(488, 498);
            this.FRM_RuleGroup.TabIndex = 40;
            this.FRM_RuleGroup.TabStop = false;
            this.FRM_RuleGroup.Text = "Rule Management";
            this.FRM_RuleGroup.Visible = false;
            // 
            // FRM_RemoveRuleButton
            // 
            this.FRM_RemoveRuleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_RemoveRuleButton.BackgroundImage = global::EasyESS_Scan.Properties.Resources.remove;
            this.FRM_RemoveRuleButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_RemoveRuleButton.Location = new System.Drawing.Point(445, 377);
            this.FRM_RemoveRuleButton.Name = "FRM_RemoveRuleButton";
            this.FRM_RemoveRuleButton.Size = new System.Drawing.Size(31, 28);
            this.FRM_RemoveRuleButton.TabIndex = 54;
            this.toolTip1.SetToolTip(this.FRM_RemoveRuleButton, "Remove selected Rule");
            this.FRM_RemoveRuleButton.UseVisualStyleBackColor = true;
            this.FRM_RemoveRuleButton.Click += new System.EventHandler(this.FRM_RemoveRuleButton_Click);
            // 
            // FRM_AddRuleButton
            // 
            this.FRM_AddRuleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddRuleButton.BackgroundImage = global::EasyESS_Scan.Properties.Resources.add;
            this.FRM_AddRuleButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddRuleButton.Location = new System.Drawing.Point(408, 377);
            this.FRM_AddRuleButton.Name = "FRM_AddRuleButton";
            this.FRM_AddRuleButton.Size = new System.Drawing.Size(31, 28);
            this.FRM_AddRuleButton.TabIndex = 53;
            this.toolTip1.SetToolTip(this.FRM_AddRuleButton, "Add new empty Rule");
            this.FRM_AddRuleButton.UseVisualStyleBackColor = true;
            this.FRM_AddRuleButton.Click += new System.EventHandler(this.FRM_AddRuleButton_Click);
            // 
            // FRM_FRM_AddRuleGroup
            // 
            this.FRM_FRM_AddRuleGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_FRM_AddRuleGroup.Controls.Add(this.label54);
            this.FRM_FRM_AddRuleGroup.Controls.Add(this.FRM_AddNewRuleButtonCancel);
            this.FRM_FRM_AddRuleGroup.Controls.Add(this.FRM_RuleName);
            this.FRM_FRM_AddRuleGroup.Controls.Add(this.FRM_AddNewRuleButton);
            this.FRM_FRM_AddRuleGroup.Location = new System.Drawing.Point(13, 398);
            this.FRM_FRM_AddRuleGroup.Name = "FRM_FRM_AddRuleGroup";
            this.FRM_FRM_AddRuleGroup.Size = new System.Drawing.Size(461, 92);
            this.FRM_FRM_AddRuleGroup.TabIndex = 55;
            this.FRM_FRM_AddRuleGroup.TabStop = false;
            this.FRM_FRM_AddRuleGroup.Visible = false;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label54.ForeColor = System.Drawing.Color.Navy;
            this.label54.Location = new System.Drawing.Point(6, 31);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(80, 16);
            this.label54.TabIndex = 46;
            this.label54.Text = "Rule name:";
            // 
            // FRM_AddNewRuleButtonCancel
            // 
            this.FRM_AddNewRuleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddNewRuleButtonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddNewRuleButtonCancel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_AddNewRuleButtonCancel.Location = new System.Drawing.Point(377, 18);
            this.FRM_AddNewRuleButtonCancel.Name = "FRM_AddNewRuleButtonCancel";
            this.FRM_AddNewRuleButtonCancel.Size = new System.Drawing.Size(77, 28);
            this.FRM_AddNewRuleButtonCancel.TabIndex = 49;
            this.FRM_AddNewRuleButtonCancel.Text = "Cancel";
            this.toolTip1.SetToolTip(this.FRM_AddNewRuleButtonCancel, "Cancel Operation");
            this.FRM_AddNewRuleButtonCancel.UseVisualStyleBackColor = true;
            this.FRM_AddNewRuleButtonCancel.Click += new System.EventHandler(this.FRM_AddNewRuleButtonCancel_Click);
            // 
            // FRM_RuleName
            // 
            this.FRM_RuleName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_RuleName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_RuleName.Location = new System.Drawing.Point(7, 52);
            this.FRM_RuleName.Name = "FRM_RuleName";
            this.FRM_RuleName.Size = new System.Drawing.Size(447, 23);
            this.FRM_RuleName.TabIndex = 44;
            this.toolTip1.SetToolTip(this.FRM_RuleName, "Give a full description what this Rule will check");
            // 
            // FRM_AddNewRuleButton
            // 
            this.FRM_AddNewRuleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddNewRuleButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddNewRuleButton.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_AddNewRuleButton.Location = new System.Drawing.Point(294, 18);
            this.FRM_AddNewRuleButton.Name = "FRM_AddNewRuleButton";
            this.FRM_AddNewRuleButton.Size = new System.Drawing.Size(77, 28);
            this.FRM_AddNewRuleButton.TabIndex = 48;
            this.FRM_AddNewRuleButton.Text = "ADD";
            this.toolTip1.SetToolTip(this.FRM_AddNewRuleButton, "Add new empty PhoneSet");
            this.FRM_AddNewRuleButton.UseVisualStyleBackColor = true;
            this.FRM_AddNewRuleButton.Click += new System.EventHandler(this.FRM_AddNewRuleButton_Click);
            // 
            // FRM_RuleList
            // 
            this.FRM_RuleList.AllowColumnReorder = true;
            this.FRM_RuleList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_RuleList.AutoArrange = false;
            this.FRM_RuleList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader12,
            this.columnHeader13});
            this.FRM_RuleList.ContextMenuStrip = this.RuleMenu;
            this.FRM_RuleList.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_RuleList.ForeColor = System.Drawing.Color.Navy;
            this.FRM_RuleList.FullRowSelect = true;
            this.FRM_RuleList.Location = new System.Drawing.Point(13, 29);
            this.FRM_RuleList.MultiSelect = false;
            this.FRM_RuleList.Name = "FRM_RuleList";
            this.FRM_RuleList.Size = new System.Drawing.Size(464, 342);
            this.FRM_RuleList.SmallImageList = this.RuleImageList;
            this.FRM_RuleList.TabIndex = 38;
            this.toolTip1.SetToolTip(this.FRM_RuleList, "Right click to Edit Rule");
            this.FRM_RuleList.UseCompatibleStateImageBehavior = false;
            this.FRM_RuleList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Rule name";
            this.columnHeader12.Width = 338;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Syntax";
            this.columnHeader13.Width = 109;
            // 
            // RuleMenu
            // 
            this.RuleMenu.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.RuleMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.RuleMenu.Name = "PVMenu";
            this.RuleMenu.Size = new System.Drawing.Size(188, 50);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = global::EasyESS_Scan.Properties.Resources.pvlist;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(187, 46);
            this.toolStripMenuItem2.Text = "Open Rule Editor";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // RuleImageList
            // 
            this.RuleImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("RuleImageList.ImageStream")));
            this.RuleImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.RuleImageList.Images.SetKeyName(0, "rule.png");
            // 
            // FRM_PVManagementGroup
            // 
            this.FRM_PVManagementGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_PVManagementGroup.Controls.Add(this.FRM_AddPVGroup);
            this.FRM_PVManagementGroup.Controls.Add(this.FRM_RemovePV);
            this.FRM_PVManagementGroup.Controls.Add(this.FRM_AddPV);
            this.FRM_PVManagementGroup.Controls.Add(this.FRM_PVList);
            this.FRM_PVManagementGroup.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_PVManagementGroup.ForeColor = System.Drawing.Color.Navy;
            this.FRM_PVManagementGroup.Location = new System.Drawing.Point(542, 12);
            this.FRM_PVManagementGroup.Name = "FRM_PVManagementGroup";
            this.FRM_PVManagementGroup.Size = new System.Drawing.Size(491, 657);
            this.FRM_PVManagementGroup.TabIndex = 39;
            this.FRM_PVManagementGroup.TabStop = false;
            this.FRM_PVManagementGroup.Text = "PV Management";
            this.FRM_PVManagementGroup.Visible = false;
            // 
            // FRM_AddPVGroup
            // 
            this.FRM_AddPVGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddPVGroup.Controls.Add(this.FRM_NewPVType);
            this.FRM_AddPVGroup.Controls.Add(this.FRM_NewPVIP);
            this.FRM_AddPVGroup.Controls.Add(this.label60);
            this.FRM_AddPVGroup.Controls.Add(this.label59);
            this.FRM_AddPVGroup.Controls.Add(this.FRM_OpenPVSearchList);
            this.FRM_AddPVGroup.Controls.Add(this.label52);
            this.FRM_AddPVGroup.Controls.Add(this.label51);
            this.FRM_AddPVGroup.Controls.Add(this.FRM_AddNewPVButtonCancel);
            this.FRM_AddPVGroup.Controls.Add(this.FRM_NewPVName);
            this.FRM_AddPVGroup.Controls.Add(this.FRM_AddNewPVButton);
            this.FRM_AddPVGroup.Location = new System.Drawing.Point(6, 519);
            this.FRM_AddPVGroup.Name = "FRM_AddPVGroup";
            this.FRM_AddPVGroup.Size = new System.Drawing.Size(413, 131);
            this.FRM_AddPVGroup.TabIndex = 52;
            this.FRM_AddPVGroup.TabStop = false;
            this.FRM_AddPVGroup.Visible = false;
            // 
            // FRM_NewPVType
            // 
            this.FRM_NewPVType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_NewPVType.AutoSize = true;
            this.FRM_NewPVType.BackColor = System.Drawing.Color.Transparent;
            this.FRM_NewPVType.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_NewPVType.ForeColor = System.Drawing.Color.Silver;
            this.FRM_NewPVType.Location = new System.Drawing.Point(190, 83);
            this.FRM_NewPVType.Name = "FRM_NewPVType";
            this.FRM_NewPVType.Size = new System.Drawing.Size(25, 14);
            this.FRM_NewPVType.TabIndex = 55;
            this.FRM_NewPVType.Text = "N/A";
            // 
            // FRM_NewPVIP
            // 
            this.FRM_NewPVIP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_NewPVIP.AutoSize = true;
            this.FRM_NewPVIP.BackColor = System.Drawing.Color.Transparent;
            this.FRM_NewPVIP.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_NewPVIP.ForeColor = System.Drawing.Color.Silver;
            this.FRM_NewPVIP.Location = new System.Drawing.Point(190, 66);
            this.FRM_NewPVIP.Name = "FRM_NewPVIP";
            this.FRM_NewPVIP.Size = new System.Drawing.Size(25, 14);
            this.FRM_NewPVIP.TabIndex = 54;
            this.FRM_NewPVIP.Text = "N/A";
            // 
            // label60
            // 
            this.label60.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label60.ForeColor = System.Drawing.Color.Silver;
            this.label60.Location = new System.Drawing.Point(155, 83);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(33, 14);
            this.label60.TabIndex = 53;
            this.label60.Text = "Type:";
            // 
            // label59
            // 
            this.label59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label59.ForeColor = System.Drawing.Color.Silver;
            this.label59.Location = new System.Drawing.Point(156, 66);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(18, 14);
            this.label59.TabIndex = 52;
            this.label59.Text = "IP:";
            // 
            // FRM_OpenPVSearchList
            // 
            this.FRM_OpenPVSearchList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_OpenPVSearchList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_OpenPVSearchList.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_OpenPVSearchList.Location = new System.Drawing.Point(7, 89);
            this.FRM_OpenPVSearchList.Name = "FRM_OpenPVSearchList";
            this.FRM_OpenPVSearchList.Size = new System.Drawing.Size(147, 31);
            this.FRM_OpenPVSearchList.TabIndex = 51;
            this.FRM_OpenPVSearchList.Text = "Search";
            this.toolTip1.SetToolTip(this.FRM_OpenPVSearchList, "Add new empty PhoneSet");
            this.FRM_OpenPVSearchList.UseVisualStyleBackColor = true;
            this.FRM_OpenPVSearchList.Click += new System.EventHandler(this.FRM_OpenPVSearchList_Click);
            // 
            // label52
            // 
            this.label52.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label52.ForeColor = System.Drawing.Color.Navy;
            this.label52.Location = new System.Drawing.Point(3, 67);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(128, 16);
            this.label52.TabIndex = 50;
            this.label52.Text = "SearchPV from list:";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label51.ForeColor = System.Drawing.Color.Navy;
            this.label51.Location = new System.Drawing.Point(6, 16);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(69, 16);
            this.label51.TabIndex = 46;
            this.label51.Text = "PV name:";
            // 
            // FRM_AddNewPVButtonCancel
            // 
            this.FRM_AddNewPVButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddNewPVButtonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddNewPVButtonCancel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_AddNewPVButtonCancel.Location = new System.Drawing.Point(329, 97);
            this.FRM_AddNewPVButtonCancel.Name = "FRM_AddNewPVButtonCancel";
            this.FRM_AddNewPVButtonCancel.Size = new System.Drawing.Size(77, 28);
            this.FRM_AddNewPVButtonCancel.TabIndex = 49;
            this.FRM_AddNewPVButtonCancel.Text = "Cancel";
            this.toolTip1.SetToolTip(this.FRM_AddNewPVButtonCancel, "Cancel Operation");
            this.FRM_AddNewPVButtonCancel.UseVisualStyleBackColor = true;
            this.FRM_AddNewPVButtonCancel.Click += new System.EventHandler(this.FRM_AddNewPVButtonCancel_Click);
            // 
            // FRM_NewPVName
            // 
            this.FRM_NewPVName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_NewPVName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_NewPVName.Location = new System.Drawing.Point(7, 37);
            this.FRM_NewPVName.Name = "FRM_NewPVName";
            this.FRM_NewPVName.ReadOnly = true;
            this.FRM_NewPVName.Size = new System.Drawing.Size(399, 23);
            this.FRM_NewPVName.TabIndex = 44;
            this.toolTip1.SetToolTip(this.FRM_NewPVName, "PV name to add");
            // 
            // FRM_AddNewPVButton
            // 
            this.FRM_AddNewPVButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddNewPVButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddNewPVButton.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_AddNewPVButton.Location = new System.Drawing.Point(329, 68);
            this.FRM_AddNewPVButton.Name = "FRM_AddNewPVButton";
            this.FRM_AddNewPVButton.Size = new System.Drawing.Size(77, 28);
            this.FRM_AddNewPVButton.TabIndex = 48;
            this.FRM_AddNewPVButton.Text = "ADD";
            this.toolTip1.SetToolTip(this.FRM_AddNewPVButton, "Add new empty PhoneSet");
            this.FRM_AddNewPVButton.UseVisualStyleBackColor = true;
            this.FRM_AddNewPVButton.Click += new System.EventHandler(this.FRM_AddNewPVButton_Click);
            // 
            // FRM_RemovePV
            // 
            this.FRM_RemovePV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_RemovePV.BackgroundImage = global::EasyESS_Scan.Properties.Resources.remove;
            this.FRM_RemovePV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_RemovePV.Location = new System.Drawing.Point(453, 529);
            this.FRM_RemovePV.Name = "FRM_RemovePV";
            this.FRM_RemovePV.Size = new System.Drawing.Size(31, 28);
            this.FRM_RemovePV.TabIndex = 47;
            this.toolTip1.SetToolTip(this.FRM_RemovePV, "Remove selected PV");
            this.FRM_RemovePV.UseVisualStyleBackColor = true;
            this.FRM_RemovePV.Click += new System.EventHandler(this.FRM_RemovePV_Click);
            // 
            // FRM_AddPV
            // 
            this.FRM_AddPV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddPV.BackgroundImage = global::EasyESS_Scan.Properties.Resources.add;
            this.FRM_AddPV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddPV.Location = new System.Drawing.Point(420, 530);
            this.FRM_AddPV.Name = "FRM_AddPV";
            this.FRM_AddPV.Size = new System.Drawing.Size(31, 28);
            this.FRM_AddPV.TabIndex = 45;
            this.toolTip1.SetToolTip(this.FRM_AddPV, "Add new PV ");
            this.FRM_AddPV.UseVisualStyleBackColor = true;
            this.FRM_AddPV.Click += new System.EventHandler(this.FRM_AddPV_Click);
            // 
            // FRM_PVList
            // 
            this.FRM_PVList.AllowColumnReorder = true;
            this.FRM_PVList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_PVList.AutoArrange = false;
            this.FRM_PVList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader14});
            this.FRM_PVList.ContextMenuStrip = this.PVMenu;
            this.FRM_PVList.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_PVList.FullRowSelect = true;
            this.FRM_PVList.Location = new System.Drawing.Point(6, 26);
            this.FRM_PVList.MultiSelect = false;
            this.FRM_PVList.Name = "FRM_PVList";
            this.FRM_PVList.Size = new System.Drawing.Size(479, 492);
            this.FRM_PVList.TabIndex = 38;
            this.FRM_PVList.UseCompatibleStateImageBehavior = false;
            this.FRM_PVList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "#";
            this.columnHeader10.Width = 47;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "PV Name";
            this.columnHeader11.Width = 295;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Type";
            this.columnHeader14.Width = 123;
            // 
            // PVMenu
            // 
            this.PVMenu.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.PVMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyPVNameToClipboardToolStripMenuItem,
            this.getPVValueToolStripMenuItem});
            this.PVMenu.Name = "PVMenu";
            this.PVMenu.Size = new System.Drawing.Size(244, 96);
            // 
            // copyPVNameToClipboardToolStripMenuItem
            // 
            this.copyPVNameToClipboardToolStripMenuItem.Image = global::EasyESS_Scan.Properties.Resources.copyclip;
            this.copyPVNameToClipboardToolStripMenuItem.Name = "copyPVNameToClipboardToolStripMenuItem";
            this.copyPVNameToClipboardToolStripMenuItem.Size = new System.Drawing.Size(243, 46);
            this.copyPVNameToClipboardToolStripMenuItem.Text = "Copy PV name to clipboard";
            this.copyPVNameToClipboardToolStripMenuItem.Click += new System.EventHandler(this.copyPVNameToClipboardToolStripMenuItem_Click);
            // 
            // getPVValueToolStripMenuItem
            // 
            this.getPVValueToolStripMenuItem.Image = global::EasyESS_Scan.Properties.Resources.analysis;
            this.getPVValueToolStripMenuItem.Name = "getPVValueToolStripMenuItem";
            this.getPVValueToolStripMenuItem.Size = new System.Drawing.Size(243, 46);
            this.getPVValueToolStripMenuItem.Text = "Get PV value";
            // 
            // FRM_LoginGroup
            // 
            this.FRM_LoginGroup.Controls.Add(this.pictureBox3);
            this.FRM_LoginGroup.Controls.Add(this.FRM_FullUserName);
            this.FRM_LoginGroup.Controls.Add(this.FRM_LogOutButton);
            this.FRM_LoginGroup.Controls.Add(this.FRM_LoginButton);
            this.FRM_LoginGroup.Controls.Add(this.FRM_LoginPassword);
            this.FRM_LoginGroup.Controls.Add(this.label46);
            this.FRM_LoginGroup.Controls.Add(this.FRM_LoginUsername);
            this.FRM_LoginGroup.Controls.Add(this.label45);
            this.FRM_LoginGroup.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_LoginGroup.ForeColor = System.Drawing.Color.Navy;
            this.FRM_LoginGroup.Location = new System.Drawing.Point(23, 62);
            this.FRM_LoginGroup.Name = "FRM_LoginGroup";
            this.FRM_LoginGroup.Size = new System.Drawing.Size(419, 169);
            this.FRM_LoginGroup.TabIndex = 35;
            this.FRM_LoginGroup.TabStop = false;
            this.FRM_LoginGroup.Text = "Login";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::EasyESS_Scan.Properties.Resources.login;
            this.pictureBox3.Location = new System.Drawing.Point(6, 31);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(78, 73);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 39;
            this.pictureBox3.TabStop = false;
            // 
            // FRM_FullUserName
            // 
            this.FRM_FullUserName.BackColor = System.Drawing.Color.Transparent;
            this.FRM_FullUserName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_FullUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.FRM_FullUserName.Location = new System.Drawing.Point(6, 137);
            this.FRM_FullUserName.Name = "FRM_FullUserName";
            this.FRM_FullUserName.Size = new System.Drawing.Size(397, 23);
            this.FRM_FullUserName.TabIndex = 36;
            this.FRM_FullUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_LogOutButton
            // 
            this.FRM_LogOutButton.Enabled = false;
            this.FRM_LogOutButton.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_LogOutButton.ForeColor = System.Drawing.Color.Black;
            this.FRM_LogOutButton.Location = new System.Drawing.Point(318, 97);
            this.FRM_LogOutButton.Name = "FRM_LogOutButton";
            this.FRM_LogOutButton.Size = new System.Drawing.Size(85, 32);
            this.FRM_LogOutButton.TabIndex = 39;
            this.FRM_LogOutButton.Text = "Logout";
            this.FRM_LogOutButton.UseVisualStyleBackColor = true;
            this.FRM_LogOutButton.Click += new System.EventHandler(this.FRM_LogOutButton_Click);
            // 
            // FRM_LoginButton
            // 
            this.FRM_LoginButton.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_LoginButton.ForeColor = System.Drawing.Color.Black;
            this.FRM_LoginButton.Location = new System.Drawing.Point(184, 97);
            this.FRM_LoginButton.Name = "FRM_LoginButton";
            this.FRM_LoginButton.Size = new System.Drawing.Size(128, 32);
            this.FRM_LoginButton.TabIndex = 38;
            this.FRM_LoginButton.Text = "Login";
            this.toolTip1.SetToolTip(this.FRM_LoginButton, "LogIn");
            this.FRM_LoginButton.UseVisualStyleBackColor = true;
            this.FRM_LoginButton.Click += new System.EventHandler(this.FRM_LoginButton_Click);
            // 
            // FRM_LoginPassword
            // 
            this.FRM_LoginPassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_LoginPassword.ForeColor = System.Drawing.Color.Green;
            this.FRM_LoginPassword.Location = new System.Drawing.Point(184, 63);
            this.FRM_LoginPassword.Name = "FRM_LoginPassword";
            this.FRM_LoginPassword.Size = new System.Drawing.Size(219, 26);
            this.FRM_LoginPassword.TabIndex = 37;
            this.FRM_LoginPassword.Text = "Huibon8.";
            this.FRM_LoginPassword.UseSystemPasswordChar = true;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(87, 64);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(73, 16);
            this.label46.TabIndex = 36;
            this.label46.Text = "Password:";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_LoginUsername
            // 
            this.FRM_LoginUsername.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_LoginUsername.ForeColor = System.Drawing.Color.Green;
            this.FRM_LoginUsername.Location = new System.Drawing.Point(184, 30);
            this.FRM_LoginUsername.Name = "FRM_LoginUsername";
            this.FRM_LoginUsername.Size = new System.Drawing.Size(219, 26);
            this.FRM_LoginUsername.TabIndex = 35;
            this.FRM_LoginUsername.Text = "miklosboros";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(87, 31);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(76, 16);
            this.label45.TabIndex = 34;
            this.label45.Text = "Username:";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_RedirectToWeb
            // 
            this.FRM_RedirectToWeb.AutoSize = true;
            this.FRM_RedirectToWeb.Location = new System.Drawing.Point(329, 26);
            this.FRM_RedirectToWeb.Name = "FRM_RedirectToWeb";
            this.FRM_RedirectToWeb.Size = new System.Drawing.Size(112, 16);
            this.FRM_RedirectToWeb.TabIndex = 33;
            this.FRM_RedirectToWeb.TabStop = true;
            this.FRM_RedirectToWeb.Tag = "http://sms-gw-01.tn.esss.lu.se/";
            this.FRM_RedirectToWeb.Text = "EasySMSforESS";
            this.FRM_RedirectToWeb.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.FRM_RedirectToWeb_LinkClicked);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(19, 26);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(255, 16);
            this.label44.TabIndex = 32;
            this.label44.Text = "Use the WEB interface to create users:";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_TestSMSGroup
            // 
            this.FRM_TestSMSGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_TestSMSGroup.BackColor = System.Drawing.Color.Transparent;
            this.FRM_TestSMSGroup.Controls.Add(this.FRM_TestSMSSend);
            this.FRM_TestSMSGroup.Controls.Add(this.label43);
            this.FRM_TestSMSGroup.Controls.Add(this.FRM_TestSMSMessage);
            this.FRM_TestSMSGroup.Controls.Add(this.label42);
            this.FRM_TestSMSGroup.Controls.Add(this.FRM_TestSMSPhoneNumber);
            this.FRM_TestSMSGroup.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic);
            this.FRM_TestSMSGroup.ForeColor = System.Drawing.Color.Navy;
            this.FRM_TestSMSGroup.Location = new System.Drawing.Point(1039, 12);
            this.FRM_TestSMSGroup.Name = "FRM_TestSMSGroup";
            this.FRM_TestSMSGroup.Size = new System.Drawing.Size(488, 153);
            this.FRM_TestSMSGroup.TabIndex = 31;
            this.FRM_TestSMSGroup.TabStop = false;
            this.FRM_TestSMSGroup.Text = "Test SMS service";
            // 
            // FRM_TestSMSSend
            // 
            this.FRM_TestSMSSend.Font = new System.Drawing.Font("Arial", 10F);
            this.FRM_TestSMSSend.ForeColor = System.Drawing.Color.Black;
            this.FRM_TestSMSSend.Location = new System.Drawing.Point(326, 50);
            this.FRM_TestSMSSend.Name = "FRM_TestSMSSend";
            this.FRM_TestSMSSend.Size = new System.Drawing.Size(128, 37);
            this.FRM_TestSMSSend.TabIndex = 34;
            this.FRM_TestSMSSend.Text = "SEND";
            this.FRM_TestSMSSend.UseVisualStyleBackColor = true;
            this.FRM_TestSMSSend.Click += new System.EventHandler(this.FRM_TestSMSSend_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(12, 86);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(91, 16);
            this.label43.TabIndex = 33;
            this.label43.Text = "Message text";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_TestSMSMessage
            // 
            this.FRM_TestSMSMessage.Font = new System.Drawing.Font("Arial", 10F);
            this.FRM_TestSMSMessage.Location = new System.Drawing.Point(16, 109);
            this.FRM_TestSMSMessage.Name = "FRM_TestSMSMessage";
            this.FRM_TestSMSMessage.Size = new System.Drawing.Size(438, 23);
            this.FRM_TestSMSMessage.TabIndex = 32;
            this.toolTip1.SetToolTip(this.FRM_TestSMSMessage, "Test message");
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(12, 26);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(230, 16);
            this.label42.TabIndex = 31;
            this.label42.Text = "Phone number like: +46722716953";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_TestSMSPhoneNumber
            // 
            this.FRM_TestSMSPhoneNumber.Font = new System.Drawing.Font("Arial", 10F);
            this.FRM_TestSMSPhoneNumber.Location = new System.Drawing.Point(16, 55);
            this.FRM_TestSMSPhoneNumber.Name = "FRM_TestSMSPhoneNumber";
            this.FRM_TestSMSPhoneNumber.Size = new System.Drawing.Size(287, 23);
            this.FRM_TestSMSPhoneNumber.TabIndex = 0;
            this.toolTip1.SetToolTip(this.FRM_TestSMSPhoneNumber, "Enter a phone number here to send instant message");
            // 
            // FRM_PhoneBook
            // 
            this.FRM_PhoneBook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_PhoneBook.Controls.Add(this.FRM_NewPhoneNumberGroup);
            this.FRM_PhoneBook.Controls.Add(this.FRM_AddPhoneSetGroup);
            this.FRM_PhoneBook.Controls.Add(this.FRM_RemovePhoneNumber);
            this.FRM_PhoneBook.Controls.Add(this.FRM_AddPhoneNumber);
            this.FRM_PhoneBook.Controls.Add(this.FRM_RemovePhoneSetButton);
            this.FRM_PhoneBook.Controls.Add(this.FRM_AddPhoneSetButton);
            this.FRM_PhoneBook.Controls.Add(this.pictureBox4);
            this.FRM_PhoneBook.Controls.Add(this.label47);
            this.FRM_PhoneBook.Controls.Add(this.FRM_PhoneSetList);
            this.FRM_PhoneBook.Controls.Add(this.label41);
            this.FRM_PhoneBook.Controls.Add(this.FRM_PhoneNumberList);
            this.FRM_PhoneBook.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_PhoneBook.ForeColor = System.Drawing.Color.Navy;
            this.FRM_PhoneBook.Location = new System.Drawing.Point(23, 237);
            this.FRM_PhoneBook.Name = "FRM_PhoneBook";
            this.FRM_PhoneBook.Size = new System.Drawing.Size(513, 432);
            this.FRM_PhoneBook.TabIndex = 38;
            this.FRM_PhoneBook.TabStop = false;
            this.FRM_PhoneBook.Text = "PhoneBook Management";
            this.FRM_PhoneBook.Visible = false;
            // 
            // FRM_NewPhoneNumberGroup
            // 
            this.FRM_NewPhoneNumberGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_NewPhoneNumberGroup.Controls.Add(this.label50);
            this.FRM_NewPhoneNumberGroup.Controls.Add(this.FRM_NewPhoneNumber);
            this.FRM_NewPhoneNumberGroup.Controls.Add(this.FRM_AddNewPhoneNumberButtonCancel);
            this.FRM_NewPhoneNumberGroup.Controls.Add(this.FRM_AddNewPhoneNumberButton);
            this.FRM_NewPhoneNumberGroup.Controls.Add(this.label49);
            this.FRM_NewPhoneNumberGroup.Controls.Add(this.FRM_NewPhoneOwner);
            this.FRM_NewPhoneNumberGroup.Location = new System.Drawing.Point(6, 336);
            this.FRM_NewPhoneNumberGroup.Name = "FRM_NewPhoneNumberGroup";
            this.FRM_NewPhoneNumberGroup.Size = new System.Drawing.Size(397, 86);
            this.FRM_NewPhoneNumberGroup.TabIndex = 46;
            this.FRM_NewPhoneNumberGroup.TabStop = false;
            this.FRM_NewPhoneNumberGroup.Visible = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(6, 57);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(102, 16);
            this.label50.TabIndex = 45;
            this.label50.Text = "PhoneNumber:";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_NewPhoneNumber
            // 
            this.FRM_NewPhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_NewPhoneNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_NewPhoneNumber.Location = new System.Drawing.Point(128, 53);
            this.FRM_NewPhoneNumber.Name = "FRM_NewPhoneNumber";
            this.FRM_NewPhoneNumber.Size = new System.Drawing.Size(180, 23);
            this.FRM_NewPhoneNumber.TabIndex = 44;
            this.toolTip1.SetToolTip(this.FRM_NewPhoneNumber, "Enter the PhoneNumber starting with +");
            // 
            // FRM_AddNewPhoneNumberButtonCancel
            // 
            this.FRM_AddNewPhoneNumberButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddNewPhoneNumberButtonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddNewPhoneNumberButtonCancel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_AddNewPhoneNumberButtonCancel.Location = new System.Drawing.Point(314, 52);
            this.FRM_AddNewPhoneNumberButtonCancel.Name = "FRM_AddNewPhoneNumberButtonCancel";
            this.FRM_AddNewPhoneNumberButtonCancel.Size = new System.Drawing.Size(77, 28);
            this.FRM_AddNewPhoneNumberButtonCancel.TabIndex = 43;
            this.FRM_AddNewPhoneNumberButtonCancel.Text = "Cancel";
            this.toolTip1.SetToolTip(this.FRM_AddNewPhoneNumberButtonCancel, "Cancel operation");
            this.FRM_AddNewPhoneNumberButtonCancel.UseVisualStyleBackColor = true;
            this.FRM_AddNewPhoneNumberButtonCancel.Click += new System.EventHandler(this.FRM_AddNewPhoneNumberButtonCancel_Click);
            // 
            // FRM_AddNewPhoneNumberButton
            // 
            this.FRM_AddNewPhoneNumberButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddNewPhoneNumberButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddNewPhoneNumberButton.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_AddNewPhoneNumberButton.Location = new System.Drawing.Point(314, 18);
            this.FRM_AddNewPhoneNumberButton.Name = "FRM_AddNewPhoneNumberButton";
            this.FRM_AddNewPhoneNumberButton.Size = new System.Drawing.Size(77, 28);
            this.FRM_AddNewPhoneNumberButton.TabIndex = 42;
            this.FRM_AddNewPhoneNumberButton.Text = "ADD";
            this.toolTip1.SetToolTip(this.FRM_AddNewPhoneNumberButton, "Add new PhoneNumber to PhoneSet");
            this.FRM_AddNewPhoneNumberButton.UseVisualStyleBackColor = true;
            this.FRM_AddNewPhoneNumberButton.Click += new System.EventHandler(this.FRM_AddNewPhoneNumberButton_Click);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label49.ForeColor = System.Drawing.Color.Navy;
            this.label49.Location = new System.Drawing.Point(6, 18);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(94, 16);
            this.label49.TabIndex = 41;
            this.label49.Text = "PhoneOwner:";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_NewPhoneOwner
            // 
            this.FRM_NewPhoneOwner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_NewPhoneOwner.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_NewPhoneOwner.Location = new System.Drawing.Point(128, 14);
            this.FRM_NewPhoneOwner.Name = "FRM_NewPhoneOwner";
            this.FRM_NewPhoneOwner.Size = new System.Drawing.Size(180, 23);
            this.FRM_NewPhoneOwner.TabIndex = 0;
            this.toolTip1.SetToolTip(this.FRM_NewPhoneOwner, "Enter the Phone Owner Name");
            // 
            // FRM_AddPhoneSetGroup
            // 
            this.FRM_AddPhoneSetGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddPhoneSetGroup.Controls.Add(this.FRM_AddPhoneSetButtonCancel);
            this.FRM_AddPhoneSetGroup.Controls.Add(this.FRM_AddNewPhoneSetButton);
            this.FRM_AddPhoneSetGroup.Controls.Add(this.label48);
            this.FRM_AddPhoneSetGroup.Controls.Add(this.FRM_NewPhoneSetName);
            this.FRM_AddPhoneSetGroup.Location = new System.Drawing.Point(9, 77);
            this.FRM_AddPhoneSetGroup.Name = "FRM_AddPhoneSetGroup";
            this.FRM_AddPhoneSetGroup.Size = new System.Drawing.Size(488, 47);
            this.FRM_AddPhoneSetGroup.TabIndex = 45;
            this.FRM_AddPhoneSetGroup.TabStop = false;
            this.FRM_AddPhoneSetGroup.Visible = false;
            // 
            // FRM_AddPhoneSetButtonCancel
            // 
            this.FRM_AddPhoneSetButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddPhoneSetButtonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddPhoneSetButtonCancel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_AddPhoneSetButtonCancel.Location = new System.Drawing.Point(403, 13);
            this.FRM_AddPhoneSetButtonCancel.Name = "FRM_AddPhoneSetButtonCancel";
            this.FRM_AddPhoneSetButtonCancel.Size = new System.Drawing.Size(77, 28);
            this.FRM_AddPhoneSetButtonCancel.TabIndex = 43;
            this.FRM_AddPhoneSetButtonCancel.Text = "Cancel";
            this.toolTip1.SetToolTip(this.FRM_AddPhoneSetButtonCancel, "Cancel Operation");
            this.FRM_AddPhoneSetButtonCancel.UseVisualStyleBackColor = true;
            this.FRM_AddPhoneSetButtonCancel.Click += new System.EventHandler(this.FRM_AddPhoneSetButtonCancel_Click);
            // 
            // FRM_AddNewPhoneSetButton
            // 
            this.FRM_AddNewPhoneSetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddNewPhoneSetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddNewPhoneSetButton.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_AddNewPhoneSetButton.Location = new System.Drawing.Point(342, 14);
            this.FRM_AddNewPhoneSetButton.Name = "FRM_AddNewPhoneSetButton";
            this.FRM_AddNewPhoneSetButton.Size = new System.Drawing.Size(55, 28);
            this.FRM_AddNewPhoneSetButton.TabIndex = 42;
            this.FRM_AddNewPhoneSetButton.Text = "ADD";
            this.toolTip1.SetToolTip(this.FRM_AddNewPhoneSetButton, "Add new empty PhoneSet");
            this.FRM_AddNewPhoneSetButton.UseVisualStyleBackColor = true;
            this.FRM_AddNewPhoneSetButton.Click += new System.EventHandler(this.FRM_AddNewPhoneSetButton_Click);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(6, 18);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(104, 16);
            this.label48.TabIndex = 41;
            this.label48.Text = "New PhoneSet:";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_NewPhoneSetName
            // 
            this.FRM_NewPhoneSetName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_NewPhoneSetName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_NewPhoneSetName.Location = new System.Drawing.Point(128, 14);
            this.FRM_NewPhoneSetName.Name = "FRM_NewPhoneSetName";
            this.FRM_NewPhoneSetName.Size = new System.Drawing.Size(208, 23);
            this.FRM_NewPhoneSetName.TabIndex = 0;
            this.toolTip1.SetToolTip(this.FRM_NewPhoneSetName, "Enter the PhoneSet name");
            this.FRM_NewPhoneSetName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // FRM_RemovePhoneNumber
            // 
            this.FRM_RemovePhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_RemovePhoneNumber.BackgroundImage = global::EasyESS_Scan.Properties.Resources.remove;
            this.FRM_RemovePhoneNumber.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_RemovePhoneNumber.Location = new System.Drawing.Point(466, 347);
            this.FRM_RemovePhoneNumber.Name = "FRM_RemovePhoneNumber";
            this.FRM_RemovePhoneNumber.Size = new System.Drawing.Size(31, 28);
            this.FRM_RemovePhoneNumber.TabIndex = 44;
            this.toolTip1.SetToolTip(this.FRM_RemovePhoneNumber, "Remove selected PhoneNumber from PhoneSet");
            this.FRM_RemovePhoneNumber.UseVisualStyleBackColor = true;
            this.FRM_RemovePhoneNumber.Click += new System.EventHandler(this.FRM_RemovePhoneNumber_Click);
            // 
            // FRM_AddPhoneNumber
            // 
            this.FRM_AddPhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddPhoneNumber.BackgroundImage = global::EasyESS_Scan.Properties.Resources.add;
            this.FRM_AddPhoneNumber.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddPhoneNumber.Location = new System.Drawing.Point(429, 347);
            this.FRM_AddPhoneNumber.Name = "FRM_AddPhoneNumber";
            this.FRM_AddPhoneNumber.Size = new System.Drawing.Size(31, 28);
            this.FRM_AddPhoneNumber.TabIndex = 43;
            this.toolTip1.SetToolTip(this.FRM_AddPhoneNumber, "Add new Numer to PhoneSet");
            this.FRM_AddPhoneNumber.UseVisualStyleBackColor = true;
            this.FRM_AddPhoneNumber.Click += new System.EventHandler(this.FRM_AddPhoneNumber_Click);
            // 
            // FRM_RemovePhoneSetButton
            // 
            this.FRM_RemovePhoneSetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_RemovePhoneSetButton.BackgroundImage = global::EasyESS_Scan.Properties.Resources.remove;
            this.FRM_RemovePhoneSetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_RemovePhoneSetButton.Location = new System.Drawing.Point(466, 48);
            this.FRM_RemovePhoneSetButton.Name = "FRM_RemovePhoneSetButton";
            this.FRM_RemovePhoneSetButton.Size = new System.Drawing.Size(31, 28);
            this.FRM_RemovePhoneSetButton.TabIndex = 42;
            this.toolTip1.SetToolTip(this.FRM_RemovePhoneSetButton, "Remove selected PhoneSet");
            this.FRM_RemovePhoneSetButton.UseVisualStyleBackColor = true;
            this.FRM_RemovePhoneSetButton.Click += new System.EventHandler(this.FRM_RemovePhoneSetButton_Click);
            // 
            // FRM_AddPhoneSetButton
            // 
            this.FRM_AddPhoneSetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_AddPhoneSetButton.BackgroundImage = global::EasyESS_Scan.Properties.Resources.add;
            this.FRM_AddPhoneSetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FRM_AddPhoneSetButton.Location = new System.Drawing.Point(434, 48);
            this.FRM_AddPhoneSetButton.Name = "FRM_AddPhoneSetButton";
            this.FRM_AddPhoneSetButton.Size = new System.Drawing.Size(31, 28);
            this.FRM_AddPhoneSetButton.TabIndex = 41;
            this.toolTip1.SetToolTip(this.FRM_AddPhoneSetButton, "Add new empty PhoneSet");
            this.FRM_AddPhoneSetButton.UseVisualStyleBackColor = true;
            this.FRM_AddPhoneSetButton.Click += new System.EventHandler(this.FRM_AddPhoneSetButton_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::EasyESS_Scan.Properties.Resources.phonebook;
            this.pictureBox4.Location = new System.Drawing.Point(6, 26);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(54, 45);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 40;
            this.pictureBox4.TabStop = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(66, 52);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(70, 16);
            this.label47.TabIndex = 40;
            this.label47.Text = "PhoneSet";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_PhoneSetList
            // 
            this.FRM_PhoneSetList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_PhoneSetList.FormattingEnabled = true;
            this.FRM_PhoneSetList.Location = new System.Drawing.Point(152, 48);
            this.FRM_PhoneSetList.Name = "FRM_PhoneSetList";
            this.FRM_PhoneSetList.Size = new System.Drawing.Size(276, 24);
            this.FRM_PhoneSetList.TabIndex = 39;
            this.toolTip1.SetToolTip(this.FRM_PhoneSetList, "Choose stored phonesets");
            this.FRM_PhoneSetList.SelectedIndexChanged += new System.EventHandler(this.FRM_PhoneSetList_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(224, 23);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(211, 14);
            this.label41.TabIndex = 38;
            this.label41.Text = "* These phonesets are only visible for you";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_PhoneNumberList
            // 
            this.FRM_PhoneNumberList.AllowColumnReorder = true;
            this.FRM_PhoneNumberList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_PhoneNumberList.AutoArrange = false;
            this.FRM_PhoneNumberList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader9});
            this.FRM_PhoneNumberList.ContextMenuStrip = this.PhoneMenu;
            this.FRM_PhoneNumberList.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_PhoneNumberList.FullRowSelect = true;
            this.FRM_PhoneNumberList.Location = new System.Drawing.Point(6, 130);
            this.FRM_PhoneNumberList.MultiSelect = false;
            this.FRM_PhoneNumberList.Name = "FRM_PhoneNumberList";
            this.FRM_PhoneNumberList.Size = new System.Drawing.Size(491, 206);
            this.FRM_PhoneNumberList.SmallImageList = this.PhoneNumberImageList;
            this.FRM_PhoneNumberList.TabIndex = 37;
            this.FRM_PhoneNumberList.UseCompatibleStateImageBehavior = false;
            this.FRM_PhoneNumberList.View = System.Windows.Forms.View.Details;
            this.FRM_PhoneNumberList.SelectedIndexChanged += new System.EventHandler(this.FRM_PhoneNumberList_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Phone Owner";
            this.columnHeader3.Width = 264;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Phone Number";
            this.columnHeader9.Width = 200;
            // 
            // PhoneMenu
            // 
            this.PhoneMenu.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.PhoneMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.PhoneMenu.Name = "PVMenu";
            this.PhoneMenu.Size = new System.Drawing.Size(276, 50);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = global::EasyESS_Scan.Properties.Resources.copyclip;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(275, 46);
            this.toolStripMenuItem1.Text = "Copy phone number to clipboard";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // PhoneNumberImageList
            // 
            this.PhoneNumberImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("PhoneNumberImageList.ImageStream")));
            this.PhoneNumberImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.PhoneNumberImageList.Images.SetKeyName(0, "user.jpg");
            // 
            // LoadTimer
            // 
            this.LoadTimer.Tick += new System.EventHandler(this.LoadTimer_Tick);
            // 
            // loadExternalPVListToolStripMenuItem
            // 
            this.loadExternalPVListToolStripMenuItem.Image = global::EasyESS_Scan.Properties.Resources.arrow_rigth;
            this.loadExternalPVListToolStripMenuItem.Name = "loadExternalPVListToolStripMenuItem";
            this.loadExternalPVListToolStripMenuItem.Size = new System.Drawing.Size(203, 46);
            this.loadExternalPVListToolStripMenuItem.Text = "Load external PV list";
            this.loadExternalPVListToolStripMenuItem.Click += new System.EventHandler(this.loadExternalPVListToolStripMenuItem_Click);
            // 
            // FRM_MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1543, 860);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.FRM_LogList);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1284, 688);
            this.Name = "FRM_MainForm";
            this.Text = "EasyESS Scan";
            this.Load += new System.EventHandler(this.FRM_MainForm_Load);
            this.Shown += new System.EventHandler(this.FRM_MainForm_Shown);
            this.tabControl1.ResumeLayout(false);
            this.FRM_QueryPage.ResumeLayout(false);
            this.FRM_QueryPage.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_CCDB_WAIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_CSEntry_WAIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_CCDB_OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_CSEntry_OK)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_LoadLocal_OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Header1)).EndInit();
            this.FRM_FindIOCPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Header2)).EndInit();
            this.FRM_SearchCCDBGroup.ResumeLayout(false);
            this.FRM_SearchCCDBGroup.PerformLayout();
            this.FRM_SearchNameGroup.ResumeLayout(false);
            this.FRM_SearchNameGroup.PerformLayout();
            this.FRM_UserGroup.ResumeLayout(false);
            this.FRM_InterfaceGroup.ResumeLayout(false);
            this.FRM_InterfaceGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_InterfaceIcon)).EndInit();
            this.FRM_Overall.ResumeLayout(false);
            this.FRM_Overall.PerformLayout();
            this.FRM_AddRemoveIOCGroup.ResumeLayout(false);
            this.FRM_AddRemoveIOCGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_OpenPVListArrow)).EndInit();
            this.FRM_IOCOnlineGroup.ResumeLayout(false);
            this.FRM_IOCOnlineGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_AnalyseWait)).EndInit();
            this.FRM_OverallGroup.ResumeLayout(false);
            this.FRM_OverallGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Header3)).EndInit();
            this.IOCMenu.ResumeLayout(false);
            this.FRM_SMSPage.ResumeLayout(false);
            this.FRM_SMSPage.PerformLayout();
            this.FRM_RulePanel.ResumeLayout(false);
            this.FRM_RulePanel.PerformLayout();
            this.PN_statusBar.ResumeLayout(false);
            this.PN_statusBar.PerformLayout();
            this.FRM_RuleGroup.ResumeLayout(false);
            this.FRM_FRM_AddRuleGroup.ResumeLayout(false);
            this.FRM_FRM_AddRuleGroup.PerformLayout();
            this.RuleMenu.ResumeLayout(false);
            this.FRM_PVManagementGroup.ResumeLayout(false);
            this.FRM_AddPVGroup.ResumeLayout(false);
            this.FRM_AddPVGroup.PerformLayout();
            this.PVMenu.ResumeLayout(false);
            this.FRM_LoginGroup.ResumeLayout(false);
            this.FRM_LoginGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.FRM_TestSMSGroup.ResumeLayout(false);
            this.FRM_TestSMSGroup.PerformLayout();
            this.FRM_PhoneBook.ResumeLayout(false);
            this.FRM_PhoneBook.PerformLayout();
            this.FRM_NewPhoneNumberGroup.ResumeLayout(false);
            this.FRM_NewPhoneNumberGroup.PerformLayout();
            this.FRM_AddPhoneSetGroup.ResumeLayout(false);
            this.FRM_AddPhoneSetGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.PhoneMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button FRM_GetCSEntry;
        public System.Windows.Forms.ListView FRM_LogList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage FRM_QueryPage;
        private System.Windows.Forms.TabPage FRM_FindIOCPage;
        private System.Windows.Forms.Label FRM_UserEmail;
        private System.Windows.Forms.Label FRM_UserName;
        private System.Windows.Forms.Label FRM_UserID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox FRM_UserCombo;
        private System.Windows.Forms.GroupBox FRM_InterfaceGroup;
        private System.Windows.Forms.Label FRM_InterfaceName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox FRM_InterfaceIcon;
        private System.Windows.Forms.GroupBox FRM_UserGroup;
        private System.Windows.Forms.Label FRM_InterfaceUpdate;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label FRM_InterfaceOwner;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label FRM_InterfaceMAC;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label FRM_InterfaceIP;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label FRM_InterfaceHost;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label FRM_InterfaceDomain;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox FRM_SearchInterface;
        private System.Windows.Forms.ListBox FRM_ListInterfaces;
        private System.Windows.Forms.GroupBox FRM_SearchNameGroup;
        private System.Windows.Forms.Button FRM_GetCCDB;
        private System.Windows.Forms.GroupBox FRM_SearchCCDBGroup;
        private System.Windows.Forms.TextBox FRM_SearchCCDB;
        private System.Windows.Forms.ListBox FRM_ListIPCs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox FRM_IOCs;
        private System.Windows.Forms.TabPage FRM_Overall;
        public System.Windows.Forms.ListView FRM_OverallList;
        private System.Windows.Forms.ColumnHeader columnHeader0;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button FRM_RefreshOverall;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.GroupBox FRM_OverallGroup;
        private System.Windows.Forms.Label FRM_OverallOwner;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label FRM_OverallMAC;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label FRM_OverallIP;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label FRM_OverallHost;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label FRM_OverallDomain;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label FRM_OverallName;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ContextMenuStrip IOCMenu;
        private System.Windows.Forms.ToolStripMenuItem Menu_LookupIP;
        private System.Windows.Forms.ToolStripMenuItem Menu_GetPVList;
        private System.Windows.Forms.GroupBox FRM_IOCOnlineGroup;
        private System.Windows.Forms.Label IOC_IOCFactoryTag;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label IOC_HashOK;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label IOC_PLCtoIOC;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label IOC_IOCtoPLC;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label IOC_PLCFactory;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label IOC_PVCount;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label IOC_Ping;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ToolStripMenuItem analyseIOCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageIOCToolStripMenuItem;
        private System.Windows.Forms.PictureBox FRM_Header1;
        private System.Windows.Forms.PictureBox FRM_Header2;
        private System.Windows.Forms.PictureBox FRM_Header3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox FRM_FilterIOC;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox FRM_CSEntry_OK;
        private System.Windows.Forms.PictureBox FRM_CCDB_OK;
        private System.Windows.Forms.PictureBox FRM_CSEntry_WAIT;
        private System.Windows.Forms.PictureBox FRM_CCDB_WAIT;
        private System.Windows.Forms.Label FRM_OnlineSave;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button FRM_LoadOffline;
        private System.Windows.Forms.PictureBox FRM_LoadLocal_OK;
        private System.Windows.Forms.PictureBox FRM_OpenPVListArrow;
        private System.Windows.Forms.Button FRM_OpenPVListButton;
        private System.Windows.Forms.Label IOC_IOCConnected;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label IOC_HashPLC;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label IOC_HashIOC;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.PictureBox FRM_AnalyseWait;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button FRM_PVFinder;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage FRM_SMSPage;
        private System.Windows.Forms.GroupBox FRM_TestSMSGroup;
        private System.Windows.Forms.Button FRM_TestSMSSend;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox FRM_TestSMSMessage;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox FRM_TestSMSPhoneNumber;
        private System.Windows.Forms.LinkLabel FRM_RedirectToWeb;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.GroupBox FRM_LoginGroup;
        private System.Windows.Forms.TextBox FRM_LoginPassword;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox FRM_LoginUsername;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Button FRM_LoginButton;
        private System.Windows.Forms.Button FRM_LogOutButton;
        private System.Windows.Forms.Label FRM_FullUserName;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox FRM_PhoneBook;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox FRM_PhoneSetList;
        private System.Windows.Forms.Label label41;
        public System.Windows.Forms.ListView FRM_PhoneNumberList;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button FRM_AddPhoneSetButton;
        private System.Windows.Forms.Button FRM_RemovePhoneSetButton;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button FRM_RemovePhoneNumber;
        private System.Windows.Forms.Button FRM_AddPhoneNumber;
        private System.Windows.Forms.GroupBox FRM_AddPhoneSetGroup;
        private System.Windows.Forms.Button FRM_AddNewPhoneSetButton;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox FRM_NewPhoneSetName;
        private System.Windows.Forms.Button FRM_AddPhoneSetButtonCancel;
        private System.Windows.Forms.GroupBox FRM_NewPhoneNumberGroup;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox FRM_NewPhoneNumber;
        private System.Windows.Forms.Button FRM_AddNewPhoneNumberButtonCancel;
        private System.Windows.Forms.Button FRM_AddNewPhoneNumberButton;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox FRM_NewPhoneOwner;
        private System.Windows.Forms.GroupBox FRM_PVManagementGroup;
        public System.Windows.Forms.ListView FRM_PVList;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.Button FRM_OpenPVSearchList;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button FRM_RemovePV;
        private System.Windows.Forms.Button FRM_AddPV;
        private System.Windows.Forms.Button FRM_AddNewPVButtonCancel;
        private System.Windows.Forms.Button FRM_AddNewPVButton;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox FRM_NewPVName;
        private System.Windows.Forms.GroupBox FRM_AddPVGroup;
        private System.Windows.Forms.ContextMenuStrip PVMenu;
        private System.Windows.Forms.ToolStripMenuItem copyPVNameToClipboardToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip PhoneMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.GroupBox FRM_RuleGroup;
        public System.Windows.Forms.ListView FRM_RuleList;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.Button FRM_RemoveRuleButton;
        private System.Windows.Forms.Button FRM_AddRuleButton;
        private System.Windows.Forms.GroupBox FRM_FRM_AddRuleGroup;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Button FRM_AddNewRuleButtonCancel;
        private System.Windows.Forms.TextBox FRM_RuleName;
        private System.Windows.Forms.Button FRM_AddNewRuleButton;
        private System.Windows.Forms.ContextMenuStrip RuleMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.Panel FRM_RulePanel;
        private System.Windows.Forms.ToolStripStatusLabel editor;
        public System.Windows.Forms.StatusStrip PN_statusBar;
        private System.Windows.Forms.TextBox PN_EditorPanel;
        private System.Windows.Forms.TextBox PN_RuleErrors;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Button PN_SaveRule;
        private System.Windows.Forms.Label PN_RuleName;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Button PN_Help;
        private System.Windows.Forms.ComboBox PN_PhoneSet;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label FRM_DPI;
        private System.Windows.Forms.TextBox PN_SMSText;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ToolStripMenuItem getPVValueToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ImageList RuleImageList;
        private System.Windows.Forms.ImageList PhoneNumberImageList;
        private System.Windows.Forms.Label FRM_NewPVType;
        private System.Windows.Forms.Label FRM_NewPVIP;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.GroupBox FRM_AddRemoveIOCGroup;
        private System.Windows.Forms.TextBox FRM_AddIOCIP;
        private System.Windows.Forms.TextBox FRM_AddIOCName;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Button FRM_AddIOC;
        private System.Windows.Forms.TextBox FRM_AddIOCHostName;
        private System.Windows.Forms.Label labal121;
        private System.Windows.Forms.Button FRM_ManualAdd;
        private System.Windows.Forms.Label FRM_MainGateway;
        private System.Windows.Forms.Timer LoadTimer;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label FRM_ProxyPort;
        private System.Windows.Forms.Label FRM_ProxyURL;
        private System.Windows.Forms.CheckBox PN_NeedDisconnect;
        private System.Windows.Forms.ToolStripMenuItem loadExternalPVListToolStripMenuItem;
    }
}


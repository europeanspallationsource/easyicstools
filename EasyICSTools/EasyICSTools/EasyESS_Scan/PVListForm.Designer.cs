﻿namespace EasyESS_Scan
{
    partial class PVListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FRM_PVListGroup = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FRM_Found = new System.Windows.Forms.Label();
            this.FRM_Wait = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.FRM_SearchPV = new System.Windows.Forms.TextBox();
            this.FRM_PVList = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PVMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ReadPVValue = new System.Windows.Forms.ToolStripMenuItem();
            this.copyPVNameToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FRM_LoadOffline = new System.Windows.Forms.Button();
            this.FilterTimer = new System.Windows.Forms.Timer(this.components);
            this.FRM_ReadPVs = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.FRM_IOC_IP = new System.Windows.Forms.Label();
            this.FRM_PVListIOCName = new System.Windows.Forms.Label();
            this.FRM_Header4 = new System.Windows.Forms.PictureBox();
            this.FRM_ChoosePV = new System.Windows.Forms.Button();
            this.FRM_SelectedPVName = new System.Windows.Forms.Label();
            this.FRM_CopyClipboard = new System.Windows.Forms.Button();
            this.FRM_PVListGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Wait)).BeginInit();
            this.PVMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Header4)).BeginInit();
            this.SuspendLayout();
            // 
            // FRM_PVListGroup
            // 
            this.FRM_PVListGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_PVListGroup.Controls.Add(this.label2);
            this.FRM_PVListGroup.Controls.Add(this.label1);
            this.FRM_PVListGroup.Controls.Add(this.FRM_Found);
            this.FRM_PVListGroup.Controls.Add(this.FRM_Wait);
            this.FRM_PVListGroup.Controls.Add(this.label5);
            this.FRM_PVListGroup.Controls.Add(this.FRM_SearchPV);
            this.FRM_PVListGroup.Controls.Add(this.FRM_PVList);
            this.FRM_PVListGroup.Location = new System.Drawing.Point(10, 75);
            this.FRM_PVListGroup.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_PVListGroup.Name = "FRM_PVListGroup";
            this.FRM_PVListGroup.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_PVListGroup.Size = new System.Drawing.Size(1226, 505);
            this.FRM_PVListGroup.TabIndex = 22;
            this.FRM_PVListGroup.TabStop = false;
            this.FRM_PVListGroup.Text = "PVList";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(1011, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "* - any characters (zero or more)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(1011, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "? - any character  (one and only one)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_Found
            // 
            this.FRM_Found.AutoSize = true;
            this.FRM_Found.BackColor = System.Drawing.Color.Transparent;
            this.FRM_Found.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_Found.ForeColor = System.Drawing.Color.Navy;
            this.FRM_Found.Location = new System.Drawing.Point(134, 51);
            this.FRM_Found.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FRM_Found.Name = "FRM_Found";
            this.FRM_Found.Size = new System.Drawing.Size(25, 14);
            this.FRM_Found.TabIndex = 37;
            this.FRM_Found.Text = "N/A";
            this.FRM_Found.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_Wait
            // 
            this.FRM_Wait.Image = global::EasyESS_Scan.Properties.Resources.wait;
            this.FRM_Wait.Location = new System.Drawing.Point(550, 221);
            this.FRM_Wait.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_Wait.Name = "FRM_Wait";
            this.FRM_Wait.Size = new System.Drawing.Size(128, 128);
            this.FRM_Wait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_Wait.TabIndex = 36;
            this.FRM_Wait.TabStop = false;
            this.FRM_Wait.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(7, 28);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 19);
            this.label5.TabIndex = 12;
            this.label5.Text = "Filter PV name:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_SearchPV
            // 
            this.FRM_SearchPV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_SearchPV.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.FRM_SearchPV.Location = new System.Drawing.Point(138, 25);
            this.FRM_SearchPV.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_SearchPV.Name = "FRM_SearchPV";
            this.FRM_SearchPV.Size = new System.Drawing.Size(868, 26);
            this.FRM_SearchPV.TabIndex = 7;
            this.FRM_SearchPV.TextChanged += new System.EventHandler(this.FRM_SearchPV_TextChanged);
            // 
            // FRM_PVList
            // 
            this.FRM_PVList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_PVList.AutoArrange = false;
            this.FRM_PVList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader9,
            this.columnHeader1,
            this.columnHeader14,
            this.columnHeader16,
            this.columnHeader2,
            this.columnHeader4});
            this.FRM_PVList.ContextMenuStrip = this.PVMenu;
            this.FRM_PVList.FullRowSelect = true;
            this.FRM_PVList.Location = new System.Drawing.Point(0, 72);
            this.FRM_PVList.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_PVList.Name = "FRM_PVList";
            this.FRM_PVList.Size = new System.Drawing.Size(1220, 428);
            this.FRM_PVList.TabIndex = 6;
            this.FRM_PVList.UseCompatibleStateImageBehavior = false;
            this.FRM_PVList.View = System.Windows.Forms.View.Details;
            this.FRM_PVList.SelectedIndexChanged += new System.EventHandler(this.FRM_PVList_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "PVName";
            this.columnHeader3.Width = 382;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Value";
            this.columnHeader9.Width = 119;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Type";
            this.columnHeader1.Width = 80;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Unit";
            this.columnHeader14.Width = 73;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Description";
            this.columnHeader16.Width = 248;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "IOC IP";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "IOC Name";
            this.columnHeader4.Width = 211;
            // 
            // PVMenu
            // 
            this.PVMenu.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.PVMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReadPVValue,
            this.copyPVNameToClipboardToolStripMenuItem});
            this.PVMenu.Name = "PVMenu";
            this.PVMenu.Size = new System.Drawing.Size(244, 96);
            this.PVMenu.Opening += new System.ComponentModel.CancelEventHandler(this.PVMenu_Opening);
            // 
            // ReadPVValue
            // 
            this.ReadPVValue.Image = global::EasyESS_Scan.Properties.Resources.readpvs;
            this.ReadPVValue.Name = "ReadPVValue";
            this.ReadPVValue.Size = new System.Drawing.Size(243, 46);
            this.ReadPVValue.Text = "Read PV value";
            this.ReadPVValue.Click += new System.EventHandler(this.ReadPVValue_Click);
            // 
            // copyPVNameToClipboardToolStripMenuItem
            // 
            this.copyPVNameToClipboardToolStripMenuItem.Image = global::EasyESS_Scan.Properties.Resources.copyclip;
            this.copyPVNameToClipboardToolStripMenuItem.Name = "copyPVNameToClipboardToolStripMenuItem";
            this.copyPVNameToClipboardToolStripMenuItem.Size = new System.Drawing.Size(243, 46);
            this.copyPVNameToClipboardToolStripMenuItem.Text = "Copy PV name to clipboard";
            this.copyPVNameToClipboardToolStripMenuItem.Click += new System.EventHandler(this.copyPVNameToClipboardToolStripMenuItem_Click);
            // 
            // FRM_LoadOffline
            // 
            this.FRM_LoadOffline.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_LoadOffline.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_LoadOffline.Location = new System.Drawing.Point(1090, 616);
            this.FRM_LoadOffline.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_LoadOffline.Name = "FRM_LoadOffline";
            this.FRM_LoadOffline.Size = new System.Drawing.Size(141, 41);
            this.FRM_LoadOffline.TabIndex = 40;
            this.FRM_LoadOffline.Text = "Close";
            this.FRM_LoadOffline.UseVisualStyleBackColor = true;
            this.FRM_LoadOffline.Click += new System.EventHandler(this.FRM_LoadOffline_Click);
            // 
            // FilterTimer
            // 
            this.FilterTimer.Interval = 200;
            this.FilterTimer.Tick += new System.EventHandler(this.FilterTimer_Tick);
            // 
            // FRM_ReadPVs
            // 
            this.FRM_ReadPVs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_ReadPVs.BackColor = System.Drawing.SystemColors.Control;
            this.FRM_ReadPVs.Enabled = false;
            this.FRM_ReadPVs.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_ReadPVs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.FRM_ReadPVs.Location = new System.Drawing.Point(10, 616);
            this.FRM_ReadPVs.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_ReadPVs.Name = "FRM_ReadPVs";
            this.FRM_ReadPVs.Size = new System.Drawing.Size(194, 41);
            this.FRM_ReadPVs.TabIndex = 42;
            this.FRM_ReadPVs.Text = "Read all PV value";
            this.toolTip1.SetToolTip(this.FRM_ReadPVs, "Enabled if less than 25 PV are shown");
            this.FRM_ReadPVs.UseVisualStyleBackColor = false;
            this.FRM_ReadPVs.Click += new System.EventHandler(this.FRM_ReadPVs_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // FRM_IOC_IP
            // 
            this.FRM_IOC_IP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_IOC_IP.BackColor = System.Drawing.Color.Transparent;
            this.FRM_IOC_IP.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.FRM_IOC_IP.ForeColor = System.Drawing.Color.White;
            this.FRM_IOC_IP.Image = global::EasyESS_Scan.Properties.Resources.header_empty;
            this.FRM_IOC_IP.Location = new System.Drawing.Point(938, 5);
            this.FRM_IOC_IP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FRM_IOC_IP.Name = "FRM_IOC_IP";
            this.FRM_IOC_IP.Size = new System.Drawing.Size(135, 45);
            this.FRM_IOC_IP.TabIndex = 41;
            this.FRM_IOC_IP.Text = "N/A";
            this.FRM_IOC_IP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_PVListIOCName
            // 
            this.FRM_PVListIOCName.BackColor = System.Drawing.Color.Transparent;
            this.FRM_PVListIOCName.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_PVListIOCName.ForeColor = System.Drawing.Color.White;
            this.FRM_PVListIOCName.Image = global::EasyESS_Scan.Properties.Resources.header_empty;
            this.FRM_PVListIOCName.Location = new System.Drawing.Point(118, 5);
            this.FRM_PVListIOCName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FRM_PVListIOCName.Name = "FRM_PVListIOCName";
            this.FRM_PVListIOCName.Size = new System.Drawing.Size(822, 45);
            this.FRM_PVListIOCName.TabIndex = 23;
            this.FRM_PVListIOCName.Text = "N/A";
            this.FRM_PVListIOCName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FRM_PVListIOCName.Click += new System.EventHandler(this.FRM_PVListIOCName_Click);
            // 
            // FRM_Header4
            // 
            this.FRM_Header4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_Header4.Image = global::EasyESS_Scan.Properties.Resources.header_pvlist;
            this.FRM_Header4.Location = new System.Drawing.Point(0, 0);
            this.FRM_Header4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_Header4.Name = "FRM_Header4";
            this.FRM_Header4.Size = new System.Drawing.Size(1235, 53);
            this.FRM_Header4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_Header4.TabIndex = 24;
            this.FRM_Header4.TabStop = false;
            // 
            // FRM_ChoosePV
            // 
            this.FRM_ChoosePV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_ChoosePV.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_ChoosePV.Location = new System.Drawing.Point(503, 616);
            this.FRM_ChoosePV.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_ChoosePV.Name = "FRM_ChoosePV";
            this.FRM_ChoosePV.Size = new System.Drawing.Size(185, 41);
            this.FRM_ChoosePV.TabIndex = 43;
            this.FRM_ChoosePV.Text = "Choose these PV(s)";
            this.FRM_ChoosePV.UseVisualStyleBackColor = true;
            this.FRM_ChoosePV.Visible = false;
            this.FRM_ChoosePV.Click += new System.EventHandler(this.FRM_ChoosePV_Click);
            // 
            // FRM_SelectedPVName
            // 
            this.FRM_SelectedPVName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FRM_SelectedPVName.BackColor = System.Drawing.Color.Transparent;
            this.FRM_SelectedPVName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_SelectedPVName.ForeColor = System.Drawing.Color.Green;
            this.FRM_SelectedPVName.Location = new System.Drawing.Point(402, 585);
            this.FRM_SelectedPVName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FRM_SelectedPVName.Name = "FRM_SelectedPVName";
            this.FRM_SelectedPVName.Size = new System.Drawing.Size(384, 27);
            this.FRM_SelectedPVName.TabIndex = 44;
            this.FRM_SelectedPVName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FRM_CopyClipboard
            // 
            this.FRM_CopyClipboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_CopyClipboard.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_CopyClipboard.Location = new System.Drawing.Point(911, 616);
            this.FRM_CopyClipboard.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_CopyClipboard.Name = "FRM_CopyClipboard";
            this.FRM_CopyClipboard.Size = new System.Drawing.Size(162, 41);
            this.FRM_CopyClipboard.TabIndex = 45;
            this.FRM_CopyClipboard.Text = "Copy to ClipBoard";
            this.toolTip1.SetToolTip(this.FRM_CopyClipboard, "Copy all PVs from the list to the clipboard");
            this.FRM_CopyClipboard.UseVisualStyleBackColor = true;
            this.FRM_CopyClipboard.Click += new System.EventHandler(this.FRM_CopyClipboard_Click);
            // 
            // PVListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1247, 671);
            this.Controls.Add(this.FRM_CopyClipboard);
            this.Controls.Add(this.FRM_SelectedPVName);
            this.Controls.Add(this.FRM_ChoosePV);
            this.Controls.Add(this.FRM_ReadPVs);
            this.Controls.Add(this.FRM_IOC_IP);
            this.Controls.Add(this.FRM_LoadOffline);
            this.Controls.Add(this.FRM_PVListIOCName);
            this.Controls.Add(this.FRM_Header4);
            this.Controls.Add(this.FRM_PVListGroup);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "PVListForm";
            this.Text = "PV list";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PVListForm_FormClosing);
            this.Load += new System.EventHandler(this.PVListForm_Load);
            this.Shown += new System.EventHandler(this.PVListForm_Shown);
            this.FRM_PVListGroup.ResumeLayout(false);
            this.FRM_PVListGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Wait)).EndInit();
            this.PVMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Header4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox FRM_PVListGroup;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.ListView FRM_PVList;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.PictureBox FRM_Header4;
        public System.Windows.Forms.TextBox FRM_SearchPV;
        private System.Windows.Forms.ContextMenuStrip PVMenu;
        private System.Windows.Forms.ToolStripMenuItem ReadPVValue;
        public System.Windows.Forms.Label FRM_PVListIOCName;
        private System.Windows.Forms.Button FRM_LoadOffline;
        public System.Windows.Forms.Label FRM_IOC_IP;
        private System.Windows.Forms.PictureBox FRM_Wait;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label FRM_Found;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ToolStripMenuItem copyPVNameToClipboardToolStripMenuItem;
        public System.Windows.Forms.Button FRM_ChoosePV;
        public System.Windows.Forms.Button FRM_ReadPVs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Timer FilterTimer;
        public System.Windows.Forms.Label FRM_SelectedPVName;
        private System.Windows.Forms.Button FRM_CopyClipboard;
    }
}
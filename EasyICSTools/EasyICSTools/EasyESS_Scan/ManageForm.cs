﻿using EpicsSharp.ChannelAccess.Client;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyESS_Scan
{
    public partial class ManageForm : Form
    {
        private CAClient client = new CAClient();

        private string gateway = null;

        public string Gateway
        {
            get
            {
                return gateway;
            }

            set
            {
                if (value == null) return;
                client.Configuration.SearchAddress = value;
            }
        }

        public ManageForm()
        {
            InitializeComponent();
        }

        private void FRM_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public string GetPVVal(string PVName)
        {
            string retval = "--PV not available--";

            try
            {
                Channel<string> channel = client.CreateChannel<string>(PVName);
                client.Configuration.WaitTimeout = 3000;
                retval = channel.Get();
                channel.Dispose();
            }
            catch (Exception e)
            {
            }
            return retval;

        }

        public bool PingIOC(string IPAddr)
        {
            bool retval = false;
            if (IPAddr != "N/A")
            {
                Ping p = new Ping();
                PingReply rep = p.Send(IPAddr, 3000);

                if (rep.Status == IPStatus.Success)
                {
                    retval = true;
                }
    
            }
            return retval;
        }

        public bool RefreshStatus()
        {
            FRM_WAIT.Visible = true;
            FRM_WAIT.Refresh();
            Application.DoEvents();
            bool retval = false;
            string user = "root";
            string pass = "";
            string SSHOutput = string.Empty;
            FRM_STARTIOC.Enabled = false;
            FRM_STOPIOC.Enabled = false;
            if (PingIOC(FRM_IOCIP.Text))
            {

                SshClient client = new SshClient(FRM_IOCIP.Text, user, pass);
                try
                {
                    client.Connect();
                    SSHOutput = client.RunCommand("ps axw | grep procServ").Result;

                    FRM_Message.Text = "Refresh successful.";
                    if (SSHOutput.Contains(FRM_IOCName.Text))
                    {
                        FRM_IOCStatus.Text = "RUN";
                        FRM_STARTIOC.Enabled = false;
                        FRM_STOPIOC.Enabled = true;
                    }
                    else
                    {
                        FRM_IOCStatus.Text = "STOPPED";
                        FRM_STARTIOC.Enabled = true;
                        FRM_STOPIOC.Enabled = false;
                    }

                }
                catch (Exception exp)
                {
                    FRM_Message.Text = "Refresh error!";
                }
                finally
                {
                    client.Disconnect();
                    client.Dispose();
                }
            } else
            {
                FRM_Message.Text = "IOC ping error.";

            }
            FRM_WAIT.Visible = false;
            FRM_WAIT.Refresh();
            Application.DoEvents();
            return retval;
        }

        private void FRM_RefreshStatus_Click(object sender, EventArgs e)
        {
            RefreshStatus();
        }

        private void ManageForm_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            FRM_IOCStatus.Text = "N/A";
            FRM_Message.Text = "Init.";
            RefreshStatus();
        }

        private void FRM_STARTIOC_Click(object sender, EventArgs e)
        {
            string user = "root";
            string pass = "";
            string SSHOutput = string.Empty;
            FRM_STARTIOC.Enabled = false;
            FRM_STOPIOC.Enabled = false;
            if (FRM_IOCName.Text == "CrS-ACCP:Cryo-IOC-001")
            {
                pass = "accp12";
            }
            DialogResult dialogResult = MessageBox.Show("Do you really want to start the IOC:" + FRM_IOCName.Text + "?", "Starting IOC confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {

                FRM_WAIT.Visible = true;
                FRM_WAIT.Refresh();
                Application.DoEvents();
                if (PingIOC(FRM_IOCIP.Text))
                {
                    SshClient client = new SshClient(FRM_IOCIP.Text, user, pass);
                    try
                    {
                        client.Connect();
                        SSHOutput = client.RunCommand("systemctl start ioc@" + FRM_IOCName.Text).Result;
                        FRM_Message.Text = "Command successful.";
                    }
                    catch (Exception exp)
                    {
                        FRM_Message.Text = "Command error!";
                    }
                    finally
                    {
                        client.Disconnect();
                        client.Dispose();
                    }

                }
                else
                {
                    FRM_Message.Text = "IOC ping error.";

                }
                FRM_WAIT.Visible = false;
                FRM_WAIT.Refresh();
                Application.DoEvents();
                RefreshStatus();
            }
        }

        private void FRM_STOPIOC_Click(object sender, EventArgs e)
        {
            string user = "root";
            string pass = "";
            string SSHOutput = string.Empty;
            FRM_STARTIOC.Enabled = false;
            FRM_STOPIOC.Enabled = false;
            if (FRM_IOCName.Text == "CrS-ACCP:Cryo-IOC-001")
            {
                pass = "accp12";
            }
            DialogResult dialogResult = MessageBox.Show("Do you really want to stop the IOC:" + FRM_IOCName.Text + "?", "Stopping IOC confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {

                FRM_WAIT.Visible = true;
                FRM_WAIT.Refresh();
                Application.DoEvents();
                if (PingIOC(FRM_IOCIP.Text))
                {
                    dialogResult = MessageBox.Show("Stopping the IOC may cause machine break and data loss. Do you want to continue? IOC:" + FRM_IOCName.Text , "Stopping IOC confirmation", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        SshClient client = new SshClient(FRM_IOCIP.Text, user, pass);
                        try
                        {
                            client.Connect();
                            SSHOutput = client.RunCommand("systemctl stop ioc@" + FRM_IOCName.Text).Result;
                            FRM_Message.Text = "Command successful.";
                        }
                        catch (Exception exp)
                        {
                            FRM_Message.Text = "Command error!";
                        }
                        finally
                        {
                            client.Disconnect();
                            client.Dispose();
                        }
                    }
                }
                else
                {
                    FRM_Message.Text = "IOC ping error.";

                }
                FRM_WAIT.Visible = false;
                FRM_WAIT.Refresh();
                Application.DoEvents();
                RefreshStatus();
            }

        }

        private void FRM_RemoveLock_Click(object sender, EventArgs e)
        {
            string user = "root";
            string pass = "";
            string SSHOutput = string.Empty;
            FRM_STARTIOC.Enabled = false;
            FRM_STOPIOC.Enabled = false;
            if (FRM_IOCName.Text == "CrS-ACCP:Cryo-IOC-001")
            {
                pass = "accp12";
            }

                FRM_WAIT.Visible = true;
                FRM_WAIT.Refresh();
                Application.DoEvents();
                if (PingIOC(FRM_IOCIP.Text))
                {
                        SshClient client = new SshClient(FRM_IOCIP.Text, user, pass);
                        try
                        {
                            client.Connect();
                            SSHOutput = client.RunCommand(@"rm -f /var/run/procServ/" + FRM_IOCName.Text+@"/require*").Result;
                            FRM_Message.Text = "Command successful.";
                        }
                        catch (Exception exp)
                        {
                            FRM_Message.Text = "Command error!";
                        }
                        finally
                        {
                            client.Disconnect();
                            client.Dispose();
                        }
                }
                else
                {
                    FRM_Message.Text = "IOC ping error.";

                }
                FRM_WAIT.Visible = false;
                FRM_WAIT.Refresh();
                Application.DoEvents();
                RefreshStatus();

        }
    }
}

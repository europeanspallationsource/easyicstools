﻿namespace EasyESS_Scan
{
    partial class ManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FRM_IOCStatus = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.FRM_IOCName = new System.Windows.Forms.Label();
            this.FRM_STARTIOC = new System.Windows.Forms.Button();
            this.FRM_STOPIOC = new System.Windows.Forms.Button();
            this.FRM_IOCIP = new System.Windows.Forms.Label();
            this.FRM_Message = new System.Windows.Forms.Label();
            this.FRM_Close = new System.Windows.Forms.Button();
            this.FRM_RefreshStatus = new System.Windows.Forms.Button();
            this.FRM_WAIT = new System.Windows.Forms.PictureBox();
            this.FRM_RemoveLock = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_WAIT)).BeginInit();
            this.SuspendLayout();
            // 
            // FRM_IOCStatus
            // 
            this.FRM_IOCStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.FRM_IOCStatus.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_IOCStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.FRM_IOCStatus.Location = new System.Drawing.Point(172, 111);
            this.FRM_IOCStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FRM_IOCStatus.Name = "FRM_IOCStatus";
            this.FRM_IOCStatus.Size = new System.Drawing.Size(134, 34);
            this.FRM_IOCStatus.TabIndex = 25;
            this.FRM_IOCStatus.Text = "RUN";
            this.FRM_IOCStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::EasyESS_Scan.Properties.Resources.IOC_CONTROL;
            this.pictureBox2.Location = new System.Drawing.Point(2, 173);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(283, 38);
            this.pictureBox2.TabIndex = 27;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::EasyESS_Scan.Properties.Resources.IOC_STATUS;
            this.pictureBox1.Location = new System.Drawing.Point(2, 65);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(320, 38);
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // FRM_IOCName
            // 
            this.FRM_IOCName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_IOCName.BackColor = System.Drawing.Color.Transparent;
            this.FRM_IOCName.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_IOCName.ForeColor = System.Drawing.Color.White;
            this.FRM_IOCName.Image = global::EasyESS_Scan.Properties.Resources.header_empty;
            this.FRM_IOCName.Location = new System.Drawing.Point(-2, -2);
            this.FRM_IOCName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FRM_IOCName.Name = "FRM_IOCName";
            this.FRM_IOCName.Size = new System.Drawing.Size(358, 41);
            this.FRM_IOCName.TabIndex = 24;
            this.FRM_IOCName.Text = "N/A";
            this.FRM_IOCName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_STARTIOC
            // 
            this.FRM_STARTIOC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.FRM_STARTIOC.Enabled = false;
            this.FRM_STARTIOC.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_STARTIOC.Location = new System.Drawing.Point(42, 224);
            this.FRM_STARTIOC.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FRM_STARTIOC.Name = "FRM_STARTIOC";
            this.FRM_STARTIOC.Size = new System.Drawing.Size(105, 34);
            this.FRM_STARTIOC.TabIndex = 41;
            this.FRM_STARTIOC.Text = "START";
            this.FRM_STARTIOC.UseVisualStyleBackColor = false;
            this.FRM_STARTIOC.Click += new System.EventHandler(this.FRM_STARTIOC_Click);
            // 
            // FRM_STOPIOC
            // 
            this.FRM_STOPIOC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.FRM_STOPIOC.Enabled = false;
            this.FRM_STOPIOC.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_STOPIOC.Location = new System.Drawing.Point(42, 273);
            this.FRM_STOPIOC.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FRM_STOPIOC.Name = "FRM_STOPIOC";
            this.FRM_STOPIOC.Size = new System.Drawing.Size(105, 34);
            this.FRM_STOPIOC.TabIndex = 42;
            this.FRM_STOPIOC.Text = "STOP";
            this.FRM_STOPIOC.UseVisualStyleBackColor = false;
            this.FRM_STOPIOC.Click += new System.EventHandler(this.FRM_STOPIOC_Click);
            // 
            // FRM_IOCIP
            // 
            this.FRM_IOCIP.AutoSize = true;
            this.FRM_IOCIP.BackColor = System.Drawing.Color.Transparent;
            this.FRM_IOCIP.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_IOCIP.ForeColor = System.Drawing.Color.Navy;
            this.FRM_IOCIP.Location = new System.Drawing.Point(1, 46);
            this.FRM_IOCIP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FRM_IOCIP.Name = "FRM_IOCIP";
            this.FRM_IOCIP.Size = new System.Drawing.Size(31, 16);
            this.FRM_IOCIP.TabIndex = 43;
            this.FRM_IOCIP.Text = "N/A";
            this.FRM_IOCIP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_Message
            // 
            this.FRM_Message.BackColor = System.Drawing.Color.Transparent;
            this.FRM_Message.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_Message.ForeColor = System.Drawing.Color.Navy;
            this.FRM_Message.Location = new System.Drawing.Point(41, 150);
            this.FRM_Message.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FRM_Message.Name = "FRM_Message";
            this.FRM_Message.Size = new System.Drawing.Size(136, 16);
            this.FRM_Message.TabIndex = 44;
            this.FRM_Message.Text = "N/A";
            this.FRM_Message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_Close
            // 
            this.FRM_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRM_Close.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_Close.Location = new System.Drawing.Point(274, 5);
            this.FRM_Close.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FRM_Close.Name = "FRM_Close";
            this.FRM_Close.Size = new System.Drawing.Size(74, 24);
            this.FRM_Close.TabIndex = 45;
            this.FRM_Close.Text = "Close";
            this.FRM_Close.UseVisualStyleBackColor = true;
            this.FRM_Close.Click += new System.EventHandler(this.FRM_Close_Click);
            // 
            // FRM_RefreshStatus
            // 
            this.FRM_RefreshStatus.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_RefreshStatus.Location = new System.Drawing.Point(42, 112);
            this.FRM_RefreshStatus.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FRM_RefreshStatus.Name = "FRM_RefreshStatus";
            this.FRM_RefreshStatus.Size = new System.Drawing.Size(105, 34);
            this.FRM_RefreshStatus.TabIndex = 46;
            this.FRM_RefreshStatus.Text = "Refresh";
            this.FRM_RefreshStatus.UseVisualStyleBackColor = true;
            this.FRM_RefreshStatus.Click += new System.EventHandler(this.FRM_RefreshStatus_Click);
            // 
            // FRM_WAIT
            // 
            this.FRM_WAIT.Image = global::EasyESS_Scan.Properties.Resources.wait;
            this.FRM_WAIT.Location = new System.Drawing.Point(310, 108);
            this.FRM_WAIT.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FRM_WAIT.Name = "FRM_WAIT";
            this.FRM_WAIT.Size = new System.Drawing.Size(38, 41);
            this.FRM_WAIT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_WAIT.TabIndex = 47;
            this.FRM_WAIT.TabStop = false;
            this.FRM_WAIT.Visible = false;
            // 
            // FRM_RemoveLock
            // 
            this.FRM_RemoveLock.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_RemoveLock.Location = new System.Drawing.Point(176, 224);
            this.FRM_RemoveLock.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FRM_RemoveLock.Name = "FRM_RemoveLock";
            this.FRM_RemoveLock.Size = new System.Drawing.Size(105, 34);
            this.FRM_RemoveLock.TabIndex = 48;
            this.FRM_RemoveLock.Text = "Remove Lock";
            this.FRM_RemoveLock.UseVisualStyleBackColor = true;
            this.FRM_RemoveLock.Click += new System.EventHandler(this.FRM_RemoveLock_Click);
            // 
            // ManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(352, 318);
            this.Controls.Add(this.FRM_RemoveLock);
            this.Controls.Add(this.FRM_WAIT);
            this.Controls.Add(this.FRM_RefreshStatus);
            this.Controls.Add(this.FRM_Close);
            this.Controls.Add(this.FRM_Message);
            this.Controls.Add(this.FRM_IOCIP);
            this.Controls.Add(this.FRM_STOPIOC);
            this.Controls.Add(this.FRM_STARTIOC);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.FRM_IOCStatus);
            this.Controls.Add(this.FRM_IOCName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ManageForm";
            this.Text = "Manage IOC";
            this.Shown += new System.EventHandler(this.ManageForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_WAIT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label FRM_IOCName;
        private System.Windows.Forms.Label FRM_IOCStatus;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button FRM_STARTIOC;
        private System.Windows.Forms.Button FRM_STOPIOC;
        private System.Windows.Forms.Label FRM_Message;
        public System.Windows.Forms.Label FRM_IOCIP;
        private System.Windows.Forms.Button FRM_Close;
        private System.Windows.Forms.Button FRM_RefreshStatus;
        private System.Windows.Forms.PictureBox FRM_WAIT;
        private System.Windows.Forms.Button FRM_RemoveLock;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.NetworkInformation;
using Renci.SshNet;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Ini;
using EpicsSharp.ChannelAccess.Client;
using System.Data.SqlClient;
using System.Configuration;
using HashLibrary;
using UrielGuy.SyntaxHighlightingTextBox;
using Flee.PublicTypes;
using Flee.CalcEngine.PublicTypes;

namespace EasyESS_Scan
{
    //import in the declaration for GenerateConsoleCtrlEvent

    public partial class FRM_MainForm : Form
    {

        public class Overall
        {
            public string IOCName { get; set; }
            public string Contained { get; set; }
            public string Hosted { get; set; }
            public string HostName { get; set; }
            public string Domain { get; set; }
            public string IP { get; set; }
            public string MAC { get; set; }
            public string Accessable { get; set; }
            public string Description { get; set; }
            public string Owner { get; set; }
            public string Ping { get; set; }
            public string PLCFactory { get; set; }
            public string IOCPLC { get; set; }
            public string PLCIOC { get; set; }
            public string IOCConnected { get; set; }
            public string HashOK { get; set; }
            public string HashIOC { get; set; }
            public string HashPLC { get; set; }
            public string IOCFactoryTag { get; set; }
            public List<PV> PVs { get; set; }
            public Overall()
            {
                IOCName = string.Empty;
                Contained = string.Empty;
                Hosted = string.Empty;
                HostName = string.Empty;
                Domain = string.Empty;
                IP = string.Empty;
                MAC = string.Empty;
                Accessable = string.Empty;
                Description = string.Empty;
                Owner = string.Empty;
                Ping = string.Empty;
                PLCFactory = string.Empty;
                IOCPLC = string.Empty;
                IOCConnected = string.Empty;
                PLCIOC = string.Empty;
                HashOK = string.Empty;
                HashIOC = string.Empty;
                HashPLC = string.Empty;
                IOCFactoryTag = string.Empty;
                PVs = new List<PV>();
            }
        }

        public class PV
        {
            public string name { get; set; }
            public string desc { get; set; }
            public string value { get; set; }
            public string engu { get; set; }
            public string type { get; set; }
            public string IP { get; set; }
            public string IOCName { get; set; }
            public PV()
            {
                name = string.Empty;
                desc = string.Empty;
                engu = string.Empty;
                value = string.Empty;
                type = string.Empty;
                IP = string.Empty;
                IOCName = string.Empty;
            }
        }

        public class CCDBIOC
        {
            public string name { get; set; }
            public string desc { get; set; }
            public string parent { get; set; }
            public string EPICSModule { get; set; }
            public string EPICSSnippet { get; set; }
            public CCDBIOC()
            {
                name = string.Empty;
                desc = string.Empty;
                parent = string.Empty;
                EPICSModule = string.Empty;
                EPICSSnippet = string.Empty;
            }
        }

        public class CCDBIPC
        {
            public string name { get; set; }
            public string desc { get; set; }
            public string parent { get; set; }
            public List<string> IOCs { get; set; }
            public string HostName { get; set; }
            public CCDBIPC()
            {
                name = string.Empty;
                desc = string.Empty;
                parent = string.Empty;
                HostName = string.Empty;
                IOCs = new List<string>();
            }
        }

        public class CSEntryUser
        {
            public string ID { get; set; }
            public string username { get; set; }
            public string displayname { get; set; }
            public string email { get; set; }
            public CSEntryUser()
            {
                ID = string.Empty;
                username = string.Empty;
                displayname = string.Empty;
                email = string.Empty;
            }
        }

        public class RuleBits
        {
            public int BitNumber { get; set; }
            public bool Created { get; set; }
            public bool Used { get; set; }
            public string Expression { get; set; }
            public bool Value { get; set; }
            public RuleBits()
            {
                BitNumber = 0;
                Created = false;
                Used = false;
                Expression = string.Empty;
                Value = false;
            }
        }

        List<RuleBits> RuleBIT = new List<RuleBits>();
        RuleBits RuleMain = new RuleBits();

        public class CSEntryInterface
        {
            public string ID { get; set; }
            public string Name { get; set; }
            public string user { get; set; }
            public string IP { get; set; }
            public string MAC { get; set; }
            public string host { get; set; }
            public string network { get; set; }
            public string domain { get; set; }
            public string lastupdate { get; set; }
            public CSEntryInterface()
            {
                ID = string.Empty;
                Name = string.Empty;
                user = string.Empty;
                IP = string.Empty;
                MAC = string.Empty;
                host = string.Empty;
                network = string.Empty;
                domain = string.Empty;
                lastupdate = string.Empty;
            }
        }
        PVListForm PVListFormWindow = new PVListForm();
        ManageForm ManageFormWindow = new ManageForm();

        private CAClient client = new CAClient();

        private string gateway = null;

        public string Gateway
        {
            get
            {
                return gateway;
            }

            set
            {
              if (value == null) return;
                client.Configuration.SearchAddress = value;
            }
        }

        public IniFile inifile;

        

        string Token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Mjg3OTkzNjEsIm5iZiI6MTUyODc5OTM2MSwianRpIjoiMGFkNDU2MTgtODllYS00NjNjLTg2YTQtOTM4ZTFlMjQ1YzA1IiwiaWRlbnRpdHkiOjE5LCJmcmVzaCI6ZmFsc2UsInR5cGUiOiJhY2Nlc3MifQ.xHgkXSQsX1N_Gcv5xtF1AybmMv4UeZJQNovc_eppqig";

        List<CSEntryUser> CSEntryUsers = new List<CSEntryUser>();
        List<CSEntryInterface> CSEntryInterfaces = new List<CSEntryInterface>();
        List<CCDBIOC> CCDBIOCs = new List<CCDBIOC>();
        List<CCDBIPC> CCDBIPCs = new List<CCDBIPC>();
        List<Overall> OverallList = new List<Overall>();

        public int SelectedIOC = -1;

        public bool SMSLoggedIn = false;

        public string FullUserName = string.Empty;

        SyntaxHighlightingTextBox shtb = new SyntaxHighlightingTextBox();

        public FRM_MainForm()
        {
            InitializeComponent();
            Application.EnableVisualStyles();
            this.AutoScaleMode = AutoScaleMode.Font;
            this.AutoScaleDimensions = new SizeF(6F, 13F); 

            if (File.Exists(Application.StartupPath + "\\LocalSettings.ini"))
            {
                inifile = new IniFile(Application.StartupPath + "\\LocalSettings.ini");
            }
            else
            {
                var myFile = File.Create(Application.StartupPath + "\\LocalSettings.ini");
                myFile.Close();
                inifile = new IniFile(Application.StartupPath + "\\LocalSettings.ini");
                inifile.IniWriteValue("LocalData", "LastOnlineUpdate", "never");
                inifile.IniWriteValue("LocalData", "MainGateway", "10.0.16.85");
                inifile.IniWriteValue("LocalData", "ProxyIP", "10.0.16.5");
                inifile.IniWriteValue("LocalData", "ProxyPort", "8888");

            }
            string[] args = Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].ToString()=="--nosms")
                {
                    tabControl1.TabPages.Remove(FRM_SMSPage);
                }
            }
        }

        public void Log(string message, Color messagecolor)
        {
            ListViewItem _Item = new ListViewItem(DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture));
            _Item.ForeColor = Color.Black;
            _Item.UseItemStyleForSubItems = false;

            ListViewItem.ListViewSubItem _SubItem = new ListViewItem.ListViewSubItem(_Item, message);
            _SubItem.ForeColor = messagecolor;
            _Item.SubItems.Add(_SubItem);


            FRM_LogList.Items.Add(_Item);
            FRM_LogList.Items[FRM_LogList.Items.Count - 1].EnsureVisible();

            Application.DoEvents();
        }

        public bool CheckCSEntryAvailability()
        {
            bool retval = false;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://csentry.esss.lu.se");
            request.AllowAutoRedirect = false; // find out if this site is up and don't follow a redirector
            request.Method = "HEAD";
            request.Proxy = new WebProxy(FRM_ProxyURL.Text, Convert.ToInt32(FRM_ProxyPort.Text));
            request.Timeout = 4000;
            try
            {
                WebResponse response = request.GetResponse();
                if (response.Headers.ToString().Contains("https://csentry.esss.lu.se"))
                {
                    retval = true;
                }
            }
            catch (WebException wex)
            {

                //set flag if there was a timeout or some other issues
            }

            return retval;
        }

        public bool CheckCCDBAvailability()
        {
            bool retval = false;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://ccdb.esss.lu.se");
            request.AllowAutoRedirect = false; // find out if this site is up and don't follow a redirector
            request.Proxy = new WebProxy(FRM_ProxyURL.Text, Convert.ToInt32(FRM_ProxyPort.Text));
            request.Method = "HEAD";
            request.Timeout = 4000;

            try
            {
                WebResponse response = request.GetResponse();
                if (response.Headers.ToString().Contains("Connection"))
                {
                    retval = true;
                }
            }
            catch (WebException wex)
            {

                //set flag if there was a timeout or some other issues
            }

            return retval;
        }

        public bool GetCSEntryUsers()
        {
            List<string> CSEntryHeaderReturn = new List<string>();
            List<string> CSEntryRawUsers = new List<string>();
            bool reetval = false;
            int XTotalCount = 0;
            int XActCount = 0;
            int XActPage = 1;
            string URI = string.Empty;
            string HtmlResult = string.Empty;
            string HtmlLinks = string.Empty;
            JArray jUsers = new JArray();

            CSEntryUsers = new List<CSEntryUser>();

            try
            {
                using (WebClient wc = new WebClient())
                {
                    URI = "https://csentry.esss.lu.se/api/v1/user/users?page=" + XActPage.ToString();
                    wc.Headers.Add("Content-Type", "application/json");
                    wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + Token;
                    wc.Proxy = new WebProxy(FRM_ProxyURL.Text, Convert.ToInt32(FRM_ProxyPort.Text));
                    HtmlResult = wc.DownloadString(URI);
                    HtmlLinks = wc.ResponseHeaders.ToString();

                    CSEntryHeaderReturn = HtmlLinks.Split('\n').ToList();

                    for (int i = 0; i < CSEntryHeaderReturn.Count; i++)
                    {
                        if ((CSEntryHeaderReturn[i].ToUpper()).Contains("X-TOTAL-COUNT:"))
                        {
                            XTotalCount = Convert.ToInt32(CSEntryHeaderReturn[i].Remove(0, 14));
                        }
                    }


                    jUsers = JArray.Parse(HtmlResult);
                    foreach (var item in jUsers.Children())
                    {
                        CSEntryUser UserItem = new CSEntryUser();
                        var itemProperties = item.Children<JProperty>();
                        var myElement = itemProperties.FirstOrDefault(x => x.Name == "display_name");
                        UserItem.displayname = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "username");
                        UserItem.username = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "id");
                        UserItem.ID = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "email");
                        UserItem.email = myElement.Value.ToString();
                        CSEntryUsers.Add(UserItem);
                        XActCount++;
                    }
                    while (XActCount < XTotalCount)
                    {
                        XActPage++;
                        URI = "https://csentry.esss.lu.se/api/v1/user/users?page=" + XActPage.ToString();
                        wc.Headers.Add("Content-Type", "application/json");
                        wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + Token;
                        HtmlResult = wc.DownloadString(URI);
                        jUsers = JArray.Parse(HtmlResult);
                        foreach (var item in jUsers.Children())
                        {
                            CSEntryUser UserItem = new CSEntryUser();
                            var itemProperties = item.Children<JProperty>();
                            var myElement = itemProperties.FirstOrDefault(x => x.Name == "display_name");
                            UserItem.displayname = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "username");
                            UserItem.username = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "id");
                            UserItem.ID = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "email");
                            UserItem.email = myElement.Value.ToString();
                            CSEntryUsers.Add(UserItem);
                            XActCount++;
                        }
                    }


                }
            }
            catch (Exception e)
            {
                Log(@"Something went wrong: " + e.Message, Color.Red);

            }
            if (XTotalCount > 0)
            {
                if (XTotalCount == XActCount)
                {
                    Log(@"Reading usernames successful!", Color.DarkGreen);

                }
                else
                {

                    Log(@"Reading usernames failed! Not every neame was read.", Color.Red);
                }

            }
            else
            {
                Log(@"Reading usernames failed!", Color.Red);

            }

            return reetval;
        }

        public bool GetCSEntryInterfaces()
        {
            List<string> CSEntryHeaderReturn = new List<string>();
            List<string> CSEntryRawUsers = new List<string>();
            bool reetval = false;
            int XTotalCount = 0;
            int XActCount = 0;
            int XActPage = 1;
            string URI = string.Empty;
            string HtmlResult = string.Empty;
            string HtmlLinks = string.Empty;
            JArray jInterfaces = new JArray();


            CSEntryInterfaces = new List<CSEntryInterface>();

            try
            {
                using (WebClient wc = new WebClient())
                {
                    URI = "https://csentry.esss.lu.se/api/v1/network/interfaces?page=" + XActPage.ToString();
                    wc.Headers.Add("Content-Type", "application/json");
                    wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + Token;
                    wc.Proxy = new WebProxy(FRM_ProxyURL.Text, Convert.ToInt32(FRM_ProxyPort.Text));
                    HtmlResult = wc.DownloadString(URI);
                    HtmlLinks = wc.ResponseHeaders.ToString();

                    CSEntryHeaderReturn = HtmlLinks.Split('\n').ToList();

                    for (int i = 0; i < CSEntryHeaderReturn.Count; i++)
                    {
                        if ((CSEntryHeaderReturn[i].ToUpper()).Contains("X-TOTAL-COUNT:"))
                        {
                            XTotalCount = Convert.ToInt32(CSEntryHeaderReturn[i].Remove(0, 14));
                        }
                    }


                    jInterfaces = JArray.Parse(HtmlResult);
                    foreach (var item in jInterfaces.Children())
                    {
                        CSEntryInterface InterfaceItem = new CSEntryInterface();
                        var itemProperties = item.Children<JProperty>();
                        var myElement = itemProperties.FirstOrDefault(x => x.Name == "user");
                        InterfaceItem.user = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "network");
                        InterfaceItem.network = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "mac");
                        InterfaceItem.MAC = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "updated_at");
                        InterfaceItem.lastupdate = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "ip");
                        InterfaceItem.IP = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "id");
                        InterfaceItem.ID = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "host");
                        InterfaceItem.host = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "domain");
                        InterfaceItem.domain = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "name");
                        InterfaceItem.Name = myElement.Value.ToString();
                        CSEntryInterfaces.Add(InterfaceItem);
                        XActCount++;
                    }
                    while (XActCount < XTotalCount)
                    {
                        XActPage++;
                        URI = "https://csentry.esss.lu.se/api/v1/network/interfaces?page=" + XActPage.ToString();
                        wc.Headers.Add("Content-Type", "application/json");
                        wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + Token;
                        HtmlResult = wc.DownloadString(URI);
                        jInterfaces = JArray.Parse(HtmlResult);
                        foreach (var item in jInterfaces.Children())
                        {
                            CSEntryInterface InterfaceItem = new CSEntryInterface();
                            var itemProperties = item.Children<JProperty>();
                            var myElement = itemProperties.FirstOrDefault(x => x.Name == "user");
                            InterfaceItem.user = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "network");
                            InterfaceItem.network = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "mac");
                            InterfaceItem.MAC = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "updated_at");
                            InterfaceItem.lastupdate = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "ip");
                            InterfaceItem.IP = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "id");
                            InterfaceItem.ID = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "host");
                            InterfaceItem.host = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "domain");
                            InterfaceItem.domain = myElement.Value.ToString();
                            myElement = itemProperties.FirstOrDefault(x => x.Name == "name");
                            InterfaceItem.Name = myElement.Value.ToString();
                            CSEntryInterfaces.Add(InterfaceItem);
                            XActCount++;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Log(@"Something went wrong: " + e.Message, Color.Red);

            }
            if (XTotalCount > 0)
            {
                if (XTotalCount == XActCount)
                {
                    Log(@"Reading interfaces successful!", Color.DarkGreen);

                }
                else
                {

                    Log(@"Reading interfaces failed! Not every interface was read.", Color.Red);
                }

            }
            else
            {
                Log(@"Reading interfaces failed!", Color.Red);

            }

            return reetval;
        }

        public static List<string> GetFieldNames(dynamic input)
        {
            List<string> fieldNames = new List<string>();

            try
            {
                // Deserialize the input json string to an object
                input = Newtonsoft.Json.JsonConvert.DeserializeObject(input);

                // Json Object could either contain an array or an object or just values
                // For the field names, navigate to the root or the first element
                input = input.Root ?? input.First ?? input;

                if (input != null)
                {
                    // Get to the first element in the array
                    bool isArray = true;
                    while (isArray)
                    {
                        input = input.First ?? input;

                        if (input.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                        input.GetType() == typeof(Newtonsoft.Json.Linq.JValue) ||
                        input == null)
                            isArray = false;
                    }

                    // check if the object is of type JObject. 
                    // If yes, read the properties of that JObject
                    if (input.GetType() == typeof(Newtonsoft.Json.Linq.JObject))
                    {
                        // Create JObject from object
                        Newtonsoft.Json.Linq.JObject inputJson =
                            Newtonsoft.Json.Linq.JObject.FromObject(input);

                        // Read Properties
                        var properties = inputJson.Properties();

                        // Loop through all the properties of that JObject
                        foreach (var property in properties)
                        {
                            // Check if there are any sub-fields (nested)
                            // i.e. the value of any field is another JObject or another JArray
                            if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                            property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                            {
                                // If yes, enter the recursive loop to extract sub-field names
                                var subFields = GetFieldNames(property.Value.ToString());

                                if (subFields != null && subFields.Count() > 0)
                                {
                                    // join sub-field names with field name 
                                    //(e.g. Field1.SubField1, Field1.SubField2, etc.)
                                    fieldNames.AddRange(
                                        subFields
                                        .Select(n =>
                                        string.IsNullOrEmpty(n) ? property.Name :
                                     string.Format("{0}.{1}", property.Name, n)));
                                }
                            }
                            else
                            {
                                // If there are no sub-fields, the property name is the field name
                                fieldNames.Add(property.Name+":"+ property.Value);
                            }
                        }
                    }
                    else
                        if (input.GetType() == typeof(Newtonsoft.Json.Linq.JValue))
                    {
                        // for direct values, there is no field name
                        fieldNames.Add(string.Empty);
                    }
                }
            }
            catch
            {
                throw;
            }

            return fieldNames;
        }

        public bool GetCCDBIOCs()
        {
            List<string> CCDBRawIOCs = new List<string>();
            bool reetval = false;
            string URI = string.Empty;
            string HtmlResult = string.Empty;
            string HtmlLinks = string.Empty;
            JArray jIOCs = new JArray();


            CCDBIOCs = new List<CCDBIOC>();

            try
            {
                using (WebClient wc = new WebClient())
                {
                    URI = "https://ccdb.esss.lu.se/rest/slots?deviceType=IOC";
                    wc.Headers.Add("Content-Type", "application/json");
                    wc.Proxy = new WebProxy(FRM_ProxyURL.Text, Convert.ToInt32(FRM_ProxyPort.Text));

                    HtmlResult = wc.DownloadString(URI);

                    HtmlResult = HtmlResult.Remove(0, 21);
                    HtmlResult = HtmlResult.Remove(HtmlResult.Length - 1, 1);
                    jIOCs = JArray.Parse(HtmlResult);
                    foreach (var item in jIOCs.Children())
                    {
                        CCDBIOC IOCItem = new CCDBIOC();
                        var itemProperties = item.Children<JProperty>();
                        var myElement = itemProperties.FirstOrDefault(x => x.Name == "name");
                        IOCItem.name = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "description");
                        IOCItem.desc = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "parents");
                        IOCItem.parent = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name  == "properties");
                        string JProperties = myElement.Value.ToString();
                        JArray jProp = JArray.Parse(JProperties);
                        foreach (var item2 in jProp.Children())
                        {
                            var itemProperties2 = item2.Children<JProperty>();
                            var myElement2 = itemProperties2.FirstOrDefault(x => x.Name == "name");
                            if (myElement2.Value.ToString() == "EPICSModule")
                            {
                                var myElement3 = itemProperties2.FirstOrDefault(x => x.Name == "value");
                                IOCItem.EPICSModule = myElement3.Value.ToString();
                            }
                            if (myElement2.Value.ToString() == "EPICSSnippet")
                            {
                                var myElement3 = itemProperties2.FirstOrDefault(x => x.Name == "value");
                                IOCItem.EPICSSnippet = myElement3.Value.ToString();
                            }
                        }
                        CCDBIOCs.Add(IOCItem);
                    }


                }
            }
            catch (Exception e)
            {
                Log(@"Something went wrong: " + e.Message, Color.Red);
            }

            return reetval;
        }

        public bool GetCCDBIPCs()
        {
            bool reetval = false;
            string URI = string.Empty;
            string HtmlResult = string.Empty;
            string HtmlLinks = string.Empty;
            JArray jIPCs = new JArray();


            CCDBIPCs = new List<CCDBIPC>();

            try
            {
                using (WebClient wc = new WebClient())
                {
                    URI = "https://ccdb.esss.lu.se/rest/slots?deviceType=IPC";
                    wc.Headers.Add("Content-Type", "application/json");
                    wc.Proxy = new WebProxy(FRM_ProxyURL.Text, Convert.ToInt32(FRM_ProxyPort.Text));

                    HtmlResult = wc.DownloadString(URI);
                    HtmlResult = HtmlResult.Remove(0, 21);
                    HtmlResult = HtmlResult.Remove(HtmlResult.Length - 1, 1);
                    jIPCs = JArray.Parse(HtmlResult);
                    foreach (var item in jIPCs.Children())
                    {
                        CCDBIPC IPCItem = new CCDBIPC();
                        var itemProperties = item.Children<JProperty>();
                        var myElement = itemProperties.FirstOrDefault(x => x.Name == "name");
                        IPCItem.name = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "description");
                        IPCItem.desc = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "parents");
                        IPCItem.parent = myElement.Value.ToString();
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "properties");
                        string JProperties = myElement.Value.ToString();
                        JArray jProp = JArray.Parse(JProperties);
                        foreach (var item2 in jProp.Children())
                        {
                            var itemProperties2 = item2.Children<JProperty>();
                            var myElement2 = itemProperties2.FirstOrDefault(x => x.Name == "name");
                            if (myElement2.Value.ToString() == "Hostname")
                            {
                                var myElement3 = itemProperties2.FirstOrDefault(x => x.Name == "value");
                                IPCItem.HostName = myElement3.Value.ToString();
                            }
                        }
                        myElement = itemProperties.FirstOrDefault(x => x.Name == "children");
                        JProperties = myElement.Value.ToString();
                        jProp = JArray.Parse(JProperties);
                        foreach (var item2 in jProp.Children())
                        {
                            IPCItem.IOCs.Add(item2.ToString()); 
                        }


                        CCDBIPCs.Add(IPCItem);
                    }


                }
            }
            catch (Exception e)
            {
                Log(@"Something went wrong: " + e.Message, Color.Red);
            }

            return reetval;
        }

        public void ReadCSEntry()
        {
            Log(@"Checking https://csentry.esss.lu.se. This may take a several seconds. Please wait...", Color.DarkGreen);

            if (CheckCSEntryAvailability())
            {
                Log(@"Lookup success!", Color.DarkGreen);
                Log(@"Loading user database...", Color.DarkGray);
                GetCSEntryUsers();
                Log(@"Loading interface database...", Color.DarkGray);
                GetCSEntryInterfaces();
                Log(@"Finished.", Color.DarkGreen);
            }
            else
            {
                Log(@"CSEntry is not available rigth now!", Color.DarkRed);
                MessageBox.Show("CSEntry is not accessible at the moment!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void ReadCCDB()
        {
            Log(@"Checking https://ccdb.esss.lu.se. This may take a several seconds. Please wait...", Color.DarkGreen);

            if (CheckCCDBAvailability())
            {
                Log(@"Lookup success!", Color.DarkGreen);
                Log(@"Loading IOC database...", Color.DarkGray);
                Log(@"  IOCs...", Color.DarkGray);
                GetCCDBIOCs();
                Log(@"  IPCs...", Color.DarkGray);
                GetCCDBIPCs();
                Log(@"Finished.", Color.DarkGreen);
            }
            else
            {
                Log(@"CCDB is not available rigth now!", Color.DarkRed);
                MessageBox.Show("CCDB is not accessible at the moment!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void WriteStream(string cmd, StreamWriter writer, ShellStream stream)
        {
            writer.WriteLine(cmd);
            while (stream.Length == 0)
            {
                Thread.Sleep(500);
            }
        }

        public void RefreshPVs(int IOC_ID)
        {
            if (IOC_ID == -1)
            {
                PVListFormWindow.PVs = new List<PVListForm.PV>();
                for (int OV = 0; OV < OverallList.Count; OV++)
                {
                    for (int i = 0; i < OverallList[OV].PVs.Count; i++)
                    {
                        PVListFormWindow.PVItem = new PVListForm.PV();

                        PVListFormWindow.PVItem.value = OverallList[OV].PVs[i].value;
                        PVListFormWindow.PVItem.type = OverallList[OV].PVs[i].type;
                        PVListFormWindow.PVItem.name = OverallList[OV].PVs[i].name;
                        PVListFormWindow.PVItem.engu = OverallList[OV].PVs[i].engu;
                        PVListFormWindow.PVItem.desc = OverallList[OV].PVs[i].desc;
                        PVListFormWindow.PVItem.IP = OverallList[OV].PVs[i].IP;
                        PVListFormWindow.PVItem.IOCName = OverallList[OV].PVs[i].IOCName;
                        PVListFormWindow.PVs.Add(PVListFormWindow.PVItem);
                    }
                }

            }
            else
            {
                if (IOC_ID < OverallList.Count)
                {
                    PVListFormWindow.PVs = new List<PVListForm.PV>();
                    for (int i = 0; i < OverallList[IOC_ID].PVs.Count; i++)
                    {
                        PVListFormWindow.PVItem = new PVListForm.PV();

                        PVListFormWindow.PVItem.value = OverallList[IOC_ID].PVs[i].value;
                        PVListFormWindow.PVItem.type = OverallList[IOC_ID].PVs[i].type;
                        PVListFormWindow.PVItem.name = OverallList[IOC_ID].PVs[i].name;
                        PVListFormWindow.PVItem.engu = OverallList[IOC_ID].PVs[i].engu;
                        PVListFormWindow.PVItem.desc = OverallList[IOC_ID].PVs[i].desc;
                        PVListFormWindow.PVItem.IP = OverallList[IOC_ID].PVs[i].IP;
                        PVListFormWindow.PVItem.IOCName = OverallList[IOC_ID].PVs[i].IOCName;
                        PVListFormWindow.PVs.Add(PVListFormWindow.PVItem);
                    }

                }
            }
        }

        public void CreateOverallList()
        {
            FRM_FilterIOC.Text = string.Empty;
            SelectedIOC = -1;
            OverallList = new List<Overall>();
            for (int IOC = 0; IOC < CCDBIOCs.Count; IOC++)
            {
                Overall OverallItem = new Overall();
                OverallItem.IOCName = CCDBIOCs[IOC].name;

                bool IPCContained = false;
                bool Hosted = false;

                OverallItem.Description = CCDBIOCs[IOC].desc;

                for (int IPC = 0; IPC < CCDBIPCs.Count; IPC++)
                {
                    for (int Child = 0; Child < CCDBIPCs[IPC].IOCs.Count; Child++)
                    {
                        if (CCDBIPCs[IPC].IOCs[Child].Contains(CCDBIOCs[IOC].name))
                        {
                            if (CCDBIPCs[IPC].HostName != "null")
                            {

                                OverallItem.HostName = CCDBIPCs[IPC].HostName;
                                IPCContained = true;

                            }
                        }
                    }
                }
                if (IPCContained)
                {
                    OverallItem.Contained = "YES";
                }
                else
                {
                    OverallItem.Contained = "NO";
                }

                for (int INTF = 0; INTF < CSEntryInterfaces.Count; INTF++)
                {
                    if (CSEntryInterfaces[INTF].host.Contains(OverallItem.HostName))
                    {
                        OverallItem.Domain = CSEntryInterfaces[INTF].domain;
                        OverallItem.MAC = CSEntryInterfaces[INTF].MAC;
                        OverallItem.Owner = CSEntryInterfaces[INTF].user;

                        OverallItem.IP = CSEntryInterfaces[INTF].IP;
                        Hosted = true;
                    }
                }

                if (Hosted)
                {
                    OverallItem.Hosted = "YES";
                }
                else
                {
                    OverallItem.Hosted = "NO";
                }

                OverallList.Add(OverallItem);
            }
            SaveObjects();
        }

        public void SaveObjects()
        {
            try
            {
                using (StreamWriter file = File.CreateText(Application.StartupPath + "\\OverallList.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, OverallList);
                }
                using (StreamWriter file = File.CreateText(Application.StartupPath + "\\CSEntryUsers.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, CSEntryUsers);
                }
                using (StreamWriter file = File.CreateText(Application.StartupPath + "\\CSEntryInterfaces.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, CSEntryInterfaces);
                }
                using (StreamWriter file = File.CreateText(Application.StartupPath + "\\CCDBIOCs.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, CCDBIOCs);
                }
                using (StreamWriter file = File.CreateText(Application.StartupPath + "\\CCDBIPCs.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, CCDBIPCs);
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

        }

        public void LoadObjects()
        {
            try
            {
                using (StreamReader file = File.OpenText(Application.StartupPath + "\\OverallList.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    OverallList = (List<Overall>)serializer.Deserialize(file, typeof(IEnumerable<Overall>));
                }
                using (StreamReader file = File.OpenText(Application.StartupPath + "\\CSEntryUsers.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    CSEntryUsers = (List<CSEntryUser>)serializer.Deserialize(file, typeof(IEnumerable<CSEntryUser>));
                }
                using (StreamReader file = File.OpenText(Application.StartupPath + "\\CSEntryInterfaces.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    CSEntryInterfaces = (List<CSEntryInterface>)serializer.Deserialize(file, typeof(IEnumerable<CSEntryInterface>));
                }
                using (StreamReader file = File.OpenText(Application.StartupPath + "\\CCDBIOCs.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    CCDBIOCs = (List<CCDBIOC>)serializer.Deserialize(file, typeof(IEnumerable<CCDBIOC>));
                }
                using (StreamReader file = File.OpenText(Application.StartupPath + "\\CCDBIPCs.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    CCDBIPCs = (List<CCDBIPC>)serializer.Deserialize(file, typeof(IEnumerable<CCDBIPC>));
                }
                for (int i=0;i<OverallList.Count;i++)
                {
                    OverallList[i].Ping = string.Empty;
                    OverallList[i].PLCFactory = string.Empty;
                    OverallList[i].IOCPLC = string.Empty;
                    OverallList[i].PLCIOC = string.Empty;
                    OverallList[i].HashOK = string.Empty;
                    OverallList[i].HashPLC = string.Empty;
                    OverallList[i].HashIOC = string.Empty;
                    OverallList[i].IOCConnected = string.Empty;
                    OverallList[i].IOCFactoryTag = string.Empty;
                }

            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

        }

        public static string SendCommand(string cmd, ShellStream sh)
        {
            StreamReader reader = null;
            try
            {
                reader = new StreamReader(sh);
                StreamWriter writer = new StreamWriter(sh);
                writer.AutoFlush = true;
                writer.WriteLine(cmd);
                while (sh.Length == 0)
                {
                    Thread.Sleep(500);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception: " + ex.ToString());
            }
            return reader.ReadToEnd();
        }

        private StringBuilder ReadStream(StreamReader reader)
        {
            StringBuilder result = new StringBuilder();

            string line;
            while ((line = reader.ReadLine()) != null)
            {
                result.AppendLine(line);
            }
            return result;
        }

        public void RefreshOverallList(string filter)
        {
            if ((CCDBIPCs.Count > 0) && (CCDBIOCs.Count > 0) && (CSEntryInterfaces.Count > 0))
            {
                FRM_OverallList.Items.Clear();
                ListViewItem.ListViewSubItem _SubItem;

                for (int i = 0; i < OverallList.Count; i++)
                {
                    if ((filter == string.Empty) || (OverallList[i].IOCName.ToUpper().Contains(filter.ToUpper())))
                    {
                        ListViewItem _Item = new ListViewItem(OverallList[i].IOCName);
                        _Item.ForeColor = Color.Navy;
                        _Item.UseItemStyleForSubItems = false;

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, OverallList[i].Contained);
                        if (OverallList[i].Contained == "YES")
                        {
                            _SubItem.ForeColor = Color.Green;
                        }
                        else
                        {
                            _SubItem.ForeColor = Color.Orange;
                        }
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, OverallList[i].Hosted);
                        if (OverallList[i].Hosted == "YES")
                        {
                            _SubItem.ForeColor = Color.Green;
                        }
                        else
                        {
                            _SubItem.ForeColor = Color.Orange;
                        }
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, OverallList[i].HostName);
                        _SubItem.ForeColor = Color.Navy;
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, OverallList[i].IP);
                        _SubItem.ForeColor = Color.Navy;
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, OverallList[i].Accessable);
                        if (OverallList[i].Accessable == "YES")
                        {
                            _SubItem.ForeColor = Color.Green;
                        }
                        else
                        {
                            _SubItem.ForeColor = Color.Orange;
                        }
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, OverallList[i].Description);
                        _SubItem.ForeColor = Color.Navy;
                        _Item.SubItems.Add(_SubItem);

                        if ((OverallList[i].Hosted == "YES") && (OverallList[i].Accessable == "YES"))
                        {
                            _Item.BackColor = Color.LightGreen;
                        }
                        else
                        {
                            _Item.BackColor = Color.White;
                        }


                        FRM_OverallList.Items.Add(_Item);
                    }
                }
            }
            else
            {
                Log(@"Load ESS database first!", Color.Orange);
            }

        }

        public void RefreshIOCDetails()
        {
           
            int RealID = -1;
            if (FRM_OverallList.SelectedItems.Count > 0)
            {
                for (int i = 0; i < OverallList.Count; i++)
                {
                    if (OverallList[i].IOCName == FRM_OverallList.Items[FRM_OverallList.SelectedItems[0].Index].Text)
                    {
                        RealID = i;
                    }
                }
                if ((RealID >= 0) && (RealID < OverallList.Count))
                {
                    FRM_OverallName.Text = OverallList[RealID].IOCName;
                    FRM_OverallIP.Text = OverallList[RealID].IP;
                    FRM_OverallHost.Text = OverallList[RealID].HostName;
                    FRM_OverallDomain.Text = OverallList[RealID].Domain;
                    FRM_OverallMAC.Text = OverallList[RealID].MAC;
                    FRM_OverallOwner.Text = OverallList[RealID].Owner;
                    IOC_Ping.Text = OverallList[RealID].Ping;
                    IOC_PVCount.Text = OverallList[RealID].PVs.Count.ToString();
                    IOC_PLCFactory.Text = OverallList[RealID].PLCFactory;
                    IOC_IOCtoPLC.Text = OverallList[RealID].IOCPLC;
                    IOC_PLCtoIOC.Text = OverallList[RealID].PLCIOC;
                    IOC_HashOK.Text =  OverallList[RealID].HashOK;
                    IOC_HashIOC.Text = OverallList[RealID].HashIOC;
                    IOC_HashPLC.Text = OverallList[RealID].HashPLC;
                    IOC_IOCConnected.Text = OverallList[RealID].IOCConnected;
                    IOC_IOCFactoryTag.Text = OverallList[RealID].IOCFactoryTag;
                    Application.DoEvents();
                }
            }
            SelectedIOC = RealID;
            if (SelectedIOC >= 0)
            {
                if (OverallList[SelectedIOC].PVs.Count > 0)
                {
                    FRM_OpenPVListArrow.Visible = true;
                    FRM_OpenPVListButton.Visible = true;
                }
                else
                {
                    FRM_OpenPVListArrow.Visible = false;
                    FRM_OpenPVListButton.Visible = false;
                }
            }

        }

        public bool GetPVList(int IOC_ID)
        {
            bool retval = false;
            if (IOC_ID < OverallList.Count)
            {
                string outp = string.Empty;
                string User = "root";
                string Passwd = string.Empty;
                string IOCPort = "2000";
                if (OverallList[IOC_ID].IOCName == "CrS-ACCP:Cryo-IOC-001")
                {
                    Passwd = "accp12";
                    IOCPort = "3000";
                }
                if (OverallList[IOC_ID].IOCName == "CrS-TMCP:Cryo-IOC-01")
                {
                    Passwd = "tmcp12";
                    IOCPort = "3000";
                    User = "dev";
                }
                using (var vclient = new SshClient(OverallList[IOC_ID].IP, User, Passwd))
                {
                    vclient.Connect();
                    using (ShellStream shell = vclient.CreateShellStream("EasyESS", 80, 24, 800, 600, 1024))
                    {
                        Log("Opening IOC "+ OverallList[IOC_ID].IOCName+ "...", Color.CadetBlue);
                        outp += SendCommand("telnet localhost "+IOCPort+"", shell) + "\n";
                        Thread.Sleep(1000);
                        Log("Pulling PV list...", Color.CadetBlue);
                        outp += SendCommand("dbl", shell) + "\n";
                        Thread.Sleep(1000);
                        Log("PV list successfully loaded.", Color.CadetBlue);
                        outp += SendCommand("echo EASYESS_SCAN_STOP", shell) + "\n";
                        Thread.Sleep(1000);

                        Log("Closing session...", Color.CadetBlue);
                        StreamWriter writer = new StreamWriter(shell);
                        writer.AutoFlush = true;
                        writer.Write('\u001D');
                        while (shell.Length == 0)
                        {
                            Thread.Sleep(500);
                        }
                        Thread.Sleep(500);

                        outp += SendCommand("quit", shell) + "\n";
                        Thread.Sleep(500);

                        shell.Close();
                    }
                    vclient.Disconnect();
                }

                List<string> PVList = outp.Split('\n').ToList();
                bool listbegin = false;
                OverallList[IOC_ID].PVs = new List<PV>();
                for (int i = 0; i < PVList.Count; i++)
                {
                    if ((listbegin) && !(PVList[i].Contains("EASYESS_SCAN_STOP")) && !(PVList[i].Contains(">")))
                    {
                        PV PVItem = new PV();
                        PVItem.name = Regex.Replace(PVList[i], @"\t|\n|\r", "");
                        PVItem.IOCName = OverallList[IOC_ID].IOCName;
                        PVItem.IP = OverallList[IOC_ID].IP;
                        OverallList[IOC_ID].PVs.Add(PVItem);

                    }
                    if (PVList[i].StartsWith("dbl"))
                    {
                        listbegin = true;
                    }
                    if (PVList[i].StartsWith("EASYESS_SCAN_STOP"))
                    {
                        listbegin = false;
                    }

                }
                if (OverallList[IOC_ID].PVs.Count > 0)
                {
                    Log("PV list pull done.", Color.CadetBlue);
                    RefreshPVs(IOC_ID);
                    RefreshIOCDetails();
                    SaveObjects();
                    retval = true;
                }
            }
            return retval;
        }

        public bool PingIOC()
        {
            bool retval = false;

            if ((SelectedIOC >= 0) && (SelectedIOC < OverallList.Count))
            {
                Ping p = new Ping();
                PingReply rep = p.Send(OverallList[SelectedIOC].IP, 3000);

                if (rep.Status == IPStatus.Success)
                {
                    OverallList[SelectedIOC].Ping = "PING OK";
                    retval = true;
                }
                else
                {
                    OverallList[SelectedIOC].Ping = "Offline";

                }
                RefreshIOCDetails();

            }
            return retval;
        }

        public void AnalyseIOC()
        {

            FRM_AnalyseWait.Visible = true;
            FRM_AnalyseWait.Refresh();
            string ModbusConnectedR = string.Empty;
            string S7ConnectedR = string.Empty;
            string ConnectedR = string.Empty;
            string PLCHashCorrectR = string.Empty;
            string AliveR = string.Empty;
            string iCommsHashToPLC = string.Empty;
            string CommsHashFromPLCR = string.Empty;
            string ModVersionR = string.Empty;

            int PLCFactoryOK = 0;
            if (OverallList[SelectedIOC].PVs.Count>0)
            {
                for (int i=0;i< OverallList[SelectedIOC].PVs.Count;i++)
                {
                    if (OverallList[SelectedIOC].PVs[i].name.Contains(":ModbusConnectedR"))
                    {
                        ModbusConnectedR = OverallList[SelectedIOC].PVs[i].name;
                        PLCFactoryOK++;
                    }
                    if (OverallList[SelectedIOC].PVs[i].name.Contains(":S7ConnectedR"))
                    {
                        S7ConnectedR = OverallList[SelectedIOC].PVs[i].name;
                        PLCFactoryOK++;
                    }
                    if (OverallList[SelectedIOC].PVs[i].name.Contains(":ConnectedR"))
                    {
                        ConnectedR = OverallList[SelectedIOC].PVs[i].name;
                        PLCFactoryOK++;
                    }
                    if (OverallList[SelectedIOC].PVs[i].name.Contains(":PLCHashCorrectR"))
                    {
                        PLCHashCorrectR = OverallList[SelectedIOC].PVs[i].name;
                        PLCFactoryOK++;
                    }
                    if (OverallList[SelectedIOC].PVs[i].name.Contains(":AliveR"))
                    {
                        AliveR = OverallList[SelectedIOC].PVs[i].name;
                        PLCFactoryOK++;
                    }
                    if (OverallList[SelectedIOC].PVs[i].name.Contains(":iCommsHashToPLC"))
                    {
                        iCommsHashToPLC = OverallList[SelectedIOC].PVs[i].name;
                        PLCFactoryOK++;
                    }
                    if (OverallList[SelectedIOC].PVs[i].name.Contains(":CommsHashFromPLCR"))
                    {
                        CommsHashFromPLCR = OverallList[SelectedIOC].PVs[i].name;
                        PLCFactoryOK++;
                    }
                    if (OverallList[SelectedIOC].PVs[i].name.Contains(":ModVersionR"))
                    {
                        ModVersionR = OverallList[SelectedIOC].PVs[i].name;
                        PLCFactoryOK++;
                    }
                }
                bool CheckPLCF = false;
                if (PLCFactoryOK == 8)
                {
                    OverallList[SelectedIOC].PLCFactory = "YES";
                    CheckPLCF = true;
                }
                else
                {
                    if ((PLCFactoryOK > 3) && (PLCFactoryOK < 8))
                    {
                        OverallList[SelectedIOC].PLCFactory = "yes, but outdated";
                        CheckPLCF = true;
                    } else
                    {
                        if (PLCFactoryOK > 8)
                        {
                            OverallList[SelectedIOC].PLCFactory = "error, multiple PVs";
                        }  else
                        {
                            OverallList[SelectedIOC].PLCFactory = "NO";

                        }

                    }

                }
                if (CheckPLCF)
                {
//                    Gateway = OverallList[SelectedIOC].IP;
                    Gateway = FRM_MainGateway.Text;
                    OverallList[SelectedIOC].IOCPLC = GetPVVal(ModbusConnectedR);
                    if (OverallList[SelectedIOC].IOCPLC != "--PV not available--")
                    {
                        OverallList[SelectedIOC].PLCIOC = GetPVVal(S7ConnectedR);
                        OverallList[SelectedIOC].IOCConnected = GetPVVal(ConnectedR);
                        OverallList[SelectedIOC].HashOK = GetPVVal(PLCHashCorrectR);
                        OverallList[SelectedIOC].HashIOC = GetPVVal(iCommsHashToPLC);
                        OverallList[SelectedIOC].HashPLC = GetPVVal(CommsHashFromPLCR);
                        OverallList[SelectedIOC].IOCFactoryTag = GetPVVal(ModVersionR);
                    } else
                    {
                        OverallList[SelectedIOC].PLCIOC = "--PV not available--";
                        OverallList[SelectedIOC].IOCConnected = "--PV not available--";
                        OverallList[SelectedIOC].HashOK = "--PV not available--";
                        OverallList[SelectedIOC].HashIOC = "--PV not available--";
                        OverallList[SelectedIOC].HashPLC = "--PV not available--";
                        OverallList[SelectedIOC].IOCFactoryTag = "--PV not available--";
                    }
                    RefreshIOCDetails();
                }
            }

            FRM_AnalyseWait.Visible = false;
            FRM_AnalyseWait.Refresh();
        }

        public string GetPVVal( string PVName)
        {
            string retval = "--PV not available--";
            
            try
            {
                Channel<string> channel = client.CreateChannel<string>(PVName);
                client.Configuration.WaitTimeout = 3000;
                retval = channel.Get();
                channel.Dispose();
            }
            catch (Exception e)
            {
                Log(e.Message + "PV:"+PVName, Color.Red);

            }
            return retval;

        }

        public float GetPVValReal(string PVName, string Field)
        {
            float retval = (float)0.0;
            try
            {
                Channel<float> channel2 = client.CreateChannel<float>(PVName + "." + Field);
                client.Configuration.WaitTimeout = 1000;
                retval = channel2.Get<float>();
                channel2.Dispose();
            }
            catch (Exception e)
            {

            }
            return retval;

        }

        #region Form events

        public void RefreshQuery()
        {
            //Users
            FRM_UserCombo.Items.Clear();
            if (CSEntryUsers.Count > 0)
            {
                for (int i = 0; i < CSEntryUsers.Count; i++)
                {
                    FRM_UserCombo.Items.Add(CSEntryUsers[i].displayname);
                }
                FRM_UserCombo.SelectedIndex = 0;
            }

            //Interfaces
            FRM_ListInterfaces.Items.Clear();
            if (CSEntryInterfaces.Count > 0)
            {
                for (int i = 0; i < CSEntryInterfaces.Count; i++)
                {
                    FRM_ListInterfaces.Items.Add(CSEntryInterfaces[i].Name);
                }
            }

            //IPCs
            FRM_ListIPCs.Items.Clear();
            if (CCDBIPCs.Count > 0)
            {
                for (int i = 0; i < CCDBIPCs.Count; i++)
                {
                    FRM_ListIPCs.Items.Add(CCDBIPCs[i].name);
                }
            }
        }

        private void FRM_GetCSEntry_Click(object sender, EventArgs e)
        {
            FRM_LoadLocal_OK.Visible = false;
            FRM_CSEntry_OK.Visible = false;
            FRM_CSEntry_WAIT.Visible = true;
            FRM_CSEntry_OK.Refresh();
            FRM_CSEntry_WAIT.Refresh();
            Application.DoEvents();

            ReadCSEntry();

            RefreshQuery();

            if ((CCDBIPCs.Count > 0) && (CCDBIOCs.Count > 0) && (CSEntryInterfaces.Count > 0))
            {
                CreateOverallList();
                RefreshOverallList(string.Empty);
                FRM_OnlineSave.Text = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                inifile.IniWriteValue("LocalData", "LastOnlineUpdate", FRM_OnlineSave.Text);
            }
            if ((CSEntryUsers.Count > 0) && (CSEntryInterfaces.Count > 0))
            {
                FRM_CSEntry_OK.Visible = true;
                FRM_CSEntry_OK.Refresh();
                FRM_CSEntry_WAIT.Refresh();
                Application.DoEvents();
            }

            FRM_CSEntry_WAIT.Visible = false;
            FRM_CSEntry_OK.Refresh();
            FRM_CSEntry_WAIT.Refresh();


        }

        private void FRM_GetCCDB_Click(object sender, EventArgs e)
        {
            FRM_LoadLocal_OK.Visible = false;
            FRM_CCDB_OK.Visible = false;
            FRM_CCDB_WAIT.Visible = true;
            FRM_CCDB_OK.Refresh();
            FRM_CCDB_WAIT.Refresh();

            ReadCCDB();

            RefreshQuery();

            if ((CCDBIPCs.Count > 0) && (CCDBIOCs.Count > 0) && (CSEntryInterfaces.Count > 0))
            {
                CreateOverallList();
                RefreshOverallList(string.Empty);
                FRM_OnlineSave.Text = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                inifile.IniWriteValue("LocalData", "LastOnlineUpdate", FRM_OnlineSave.Text);
            }
            if ((CCDBIPCs.Count > 0) && (CCDBIOCs.Count > 0) )
            {
                FRM_CCDB_OK.Visible = true;
                FRM_CCDB_OK.Refresh();
                FRM_CCDB_WAIT.Refresh();
            }
            FRM_CCDB_WAIT.Visible = false;
            FRM_CCDB_OK.Refresh();
            FRM_CCDB_WAIT.Refresh();

        }

        private void FRM_Clear_Click(object sender, EventArgs e)
        {
            FRM_LogList.Items.Clear();
        }

        private void FRM_UserCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CSEntryUsers.Count > 0)
            {
                FRM_UserEmail.Text = CSEntryUsers[FRM_UserCombo.SelectedIndex].email;
                FRM_UserID.Text = CSEntryUsers[FRM_UserCombo.SelectedIndex].ID;
                FRM_UserName.Text = CSEntryUsers[FRM_UserCombo.SelectedIndex].username;
                if (CSEntryInterfaces.Count > 0)
                {
                    FRM_ListInterfaces.Items.Clear();
                    for (int i = 0; i < CSEntryInterfaces.Count; i++)
                    {
                        if (CSEntryInterfaces[i].user.Equals(FRM_UserName.Text))
                        {
                            FRM_ListInterfaces.Items.Add(CSEntryInterfaces[i].Name);
                        }
                    }
                }
            }
        }

        private void FRM_SearchInterface_TextChanged(object sender, EventArgs e)
        {
            if (CSEntryInterfaces.Count > 0)
            {
                FRM_ListInterfaces.Items.Clear();
                for (int i = 0; i < CSEntryInterfaces.Count; i++)
                {
                    if (CSEntryInterfaces[i].Name.Contains(FRM_SearchInterface.Text))
                    {
                        FRM_ListInterfaces.Items.Add(CSEntryInterfaces[i].Name);
                    }
                }
                if (FRM_ListInterfaces.Items.Count > 0)
                {
                    FRM_ListInterfaces.SelectedIndex = 0;
                }
            }

        }

        private void FRM_SearchCCDB_TextChanged(object sender, EventArgs e)
        {
            if (CCDBIPCs.Count>0)
            {
                FRM_ListIPCs.Items.Clear();
                for (int i = 0; i < CCDBIPCs.Count; i++)
                {
                    if (CCDBIPCs[i].name.Contains(FRM_SearchCCDB.Text))
                    {
                        FRM_ListIPCs.Items.Add(CCDBIPCs[i].name );
                    }
                }
                if (FRM_ListIPCs.Items.Count > 0)
                {
                    FRM_ListIPCs.SelectedIndex = 0;
                }
            }
        }

        private void FRM_ListInterfaces_SelectedIndexChanged(object sender, EventArgs e)
        {
            int SelectedIndex = 0;
            if (CSEntryInterfaces.Count > 0)
            {
                for (int i = 0; i < CSEntryInterfaces.Count; i++)
                {
                    if (CSEntryInterfaces[i].Name.Equals(FRM_ListInterfaces.Items[FRM_ListInterfaces.SelectedIndex].ToString()))
                    {
                        FRM_InterfaceName.Text = CSEntryInterfaces[i].Name;
                        FRM_InterfaceDomain.Text = CSEntryInterfaces[i].domain;
                        FRM_InterfaceHost.Text = CSEntryInterfaces[i].host;
                        FRM_InterfaceDomain.Text = CSEntryInterfaces[i].domain;
                        FRM_InterfaceIP.Text = CSEntryInterfaces[i].IP;
                        FRM_InterfaceMAC.Text = CSEntryInterfaces[i].MAC;
                        FRM_InterfaceOwner.Text = CSEntryInterfaces[i].user;
                        FRM_InterfaceUpdate.Text = CSEntryInterfaces[i].lastupdate;
                        SelectedIndex = i;
                    }
                }
            }
            if (CCDBIPCs.Count>0)
            {
                FRM_IOCs.Items.Clear();
                for (int i = 0; i < CCDBIPCs.Count; i++)
                {
                    if ((CCDBIPCs[i].HostName.Contains(CSEntryInterfaces[SelectedIndex].host)))
                    {
                        FRM_IOCs.Items.Add(CCDBIPCs[i].name);
                    }
                }

            }
            if (FRM_IOCs.Items.Count>0)
            {
                FRM_IOCs.SelectedIndex = 0;
            }

        }

        private void FRM_FindIOCPage_Click(object sender, EventArgs e)
        {

        }

        #endregion

        private void FRM_ListIPCs_SelectedIndexChanged(object sender, EventArgs e)
        {
            int SelectedIndex = 0;
            if (CCDBIPCs.Count > 0)
            {
                for (int i = 0; i < CCDBIPCs.Count; i++)
                {
                    if ((CCDBIPCs[i].name) .Equals(FRM_ListIPCs.Items[FRM_ListIPCs.SelectedIndex]))
                    {
                        SelectedIndex = i;
                    }
                }
            }

            if (CSEntryInterfaces.Count > 0)
            {
                FRM_ListInterfaces.Items.Clear();
                for (int i = 0; i < CSEntryInterfaces.Count; i++)
                {
                    if (CSEntryInterfaces[i].host.Contains(CCDBIPCs[SelectedIndex].HostName))
                    {
                        FRM_ListInterfaces.Items.Add(CSEntryInterfaces[i].Name);
                    }
                }
                if (FRM_ListInterfaces.Items.Count > 0)
                {
                    FRM_ListInterfaces.SelectedIndex = 0;
                }
            }

        }

        private void FRM_RefreshOverall_Click(object sender, EventArgs e)
        {
            RefreshOverallList(string.Empty);
        }

        private void FRM_LookupIPs_Click(object sender, EventArgs e)
        {


        }

        private void FRM_OverallList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshIOCDetails();
        }

        public void EditorParameters()
        {
            shtb.Location = new Point(0, 0);
            shtb.Dock = DockStyle.Fill;

            shtb.Seperators.Add(' ');
            shtb.Seperators.Add('\r');
            shtb.Seperators.Add('\n');
            shtb.Seperators.Add(',');
            shtb.Seperators.Add('.');
            shtb.Seperators.Add('-');
            shtb.Seperators.Add(';');
            shtb.Seperators.Add('+');
            shtb.Seperators.Add(':');
            Controls.Add(shtb);
            shtb.WordWrap = false;
            shtb.ReadOnly = false;
            shtb.ScrollBars = RichTextBoxScrollBars.Both;// & RichTextBoxScrollBars.ForcedVertical;

            shtb.FilterAutoComplete = false;
            shtb.HighlightDescriptors.Add(new HighlightDescriptor("=", Color.ForestGreen, null, DescriptorType.Word, DescriptorRecognition.WholeWord, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor("<", Color.Tomato, null, DescriptorType.Word, DescriptorRecognition.WholeWord, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor(">", Color.Tomato, null, DescriptorType.Word, DescriptorRecognition.WholeWord, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor("<=", Color.Tomato, null, DescriptorType.Word, DescriptorRecognition.WholeWord, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor(">=", Color.Tomato, null, DescriptorType.Word, DescriptorRecognition.WholeWord, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor("==", Color.Tomato, null, DescriptorType.Word, DescriptorRecognition.WholeWord, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor("@RULE", Color.Tomato, null, DescriptorType.Word, DescriptorRecognition.StartsWith, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor("@BIT", Color.Blue, null, DescriptorType.Word, DescriptorRecognition.StartsWith, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor("@PV", Color.Peru, null, DescriptorType.Word, DescriptorRecognition.StartsWith, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor("//", Color.Green, null, DescriptorType.ToEOL, DescriptorRecognition.Contains, true));
            shtb.HighlightDescriptors.Add(new HighlightDescriptor("(*", "*)", Color.Green, null, DescriptorType.ToCloseToken, DescriptorRecognition.StartsWith, false));
        }

        private void shtb_CursorPositionChanged(object sender, System.EventArgs e)
        {
            int line = shtb.CurrentLine;
            int col = shtb.CurrentColumn;
            int pos = shtb.CurrentPosition;

            PN_statusBar.Items[0].Text = "Line " + line + ", Column " + col + ", Position " + pos;
            CheckRuleSyntax(false);

        }

        private void shtb_KeyPress(object sender, KeyPressEventArgs e)
        {

            CheckRuleSyntax(false);
        }

        private void shtb_SelectionChanged(object sender, System.EventArgs e)
        {
            int start = shtb.SelectionStart;
            int end = shtb.SelectionEnd;
            int length = shtb.SelectionLength;

            PN_statusBar.Items[0].Text = "Start " + start + ", End " + end + ", Length " + length;
        }

        private void FRM_MainForm_Load(object sender, EventArgs e)
        {

            this.AutoScaleMode = AutoScaleMode.Font;
            //Az editor paraméteri
            EditorParameters();

            //Rögzítés a panelhoz
            PN_EditorPanel.Controls.Add(shtb);
            shtb.Parent = PN_EditorPanel;

            //Eventek beállítása a státusz kiíráshoz
            shtb.CursorPositionChanged += new System.EventHandler(shtb_CursorPositionChanged);
            shtb.SelectionChanged += new System.EventHandler(shtb_SelectionChanged);
            shtb.KeyPress += new KeyPressEventHandler(shtb_KeyPress);
        }

        private void FRM_QueryPage_Click(object sender, EventArgs e)
        {

        }

        private void Menu_LookupIP_Click(object sender, EventArgs e)
        {
            PingIOC();
        }

        private void FRM_FilterIOC_TextChanged(object sender, EventArgs e)
        {
            if (OverallList.Count>0)
            {
                RefreshOverallList(FRM_FilterIOC.Text);
            }
        }

        private void FRM_MainForm_Shown(object sender, EventArgs e)
        {
            FRM_OnlineSave.Text = inifile.IniReadValue("LocalData", "LastOnlineUpdate" );
            FRM_MainGateway.Text = inifile.IniReadValue("LocalData", "MainGateway");
            FRM_ProxyURL.Text = inifile.IniReadValue("LocalData", "ProxyIP");
            FRM_ProxyPort.Text = inifile.IniReadValue("LocalData", "ProxyPort");
            float dpiX, dpiY;
            Graphics graphics = this.CreateGraphics();
            dpiX = graphics.DpiX;
            dpiY = graphics.DpiY;
            FRM_DPI.Text = "DPIx:" + dpiX.ToString() + "; DPIy:" + dpiY.ToString();
            LoadTimer.Enabled = true;
        }

        private void FRM_LoadOffline_Click(object sender, EventArgs e)
        {
            LoadObjects();

            RefreshQuery();

            if ((CCDBIPCs.Count > 0) && (CCDBIOCs.Count > 0) && (CSEntryInterfaces.Count > 0))
            {
                RefreshOverallList(string.Empty);
                FRM_LoadLocal_OK.Visible = true;
            }
        }

        private void Menu_GetPVList_Click(object sender, EventArgs e)
        {
            if (PingIOC())
            {
                GetPVList(SelectedIOC);
                RefreshIOCDetails();
            }
        }

        private void FRM_OpenPVListButton_Click(object sender, EventArgs e)
        {
            if ((SelectedIOC >= 0) && (SelectedIOC < OverallList.Count))
            {
                RefreshPVs(SelectedIOC);
                PVListFormWindow.FRM_PVListIOCName.Text = "IOC: " + FRM_OverallName.Text;
                PVListFormWindow.FRM_IOC_IP.Text = OverallList[SelectedIOC].IP;
                PVListFormWindow.ShowDialog();
            }
        }

        private void analyseIOCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedIOC >= 0)
            {
                if (OverallList[SelectedIOC].PVs.Count > 0)
                {
                    if (PingIOC())
                    {
                        AnalyseIOC();
                        RefreshIOCDetails();
                    }
                }
                else
                {
                    if (PingIOC())
                    {
                        if (GetPVList(SelectedIOC))
                        {
                            AnalyseIOC();
                            RefreshIOCDetails();
                        }
                    }
                }
            }
        }

        private void IOCMenu_Opening(object sender, CancelEventArgs e)
        {

        }

        private void manageIOCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((SelectedIOC >= 0) && (OverallList.Count > 0))
            {
                ManageFormWindow.FRM_IOCIP.Text = OverallList[SelectedIOC].IP;
                ManageFormWindow.FRM_IOCName.Text = OverallList[SelectedIOC].IOCName.Replace(':','_');
                ManageFormWindow.ShowDialog();
            }
        }

        private void FRM_PVFinder_Click(object sender, EventArgs e)
        {
            if ( OverallList.Count>1)
            {

                RefreshPVs(-1);
                PVListFormWindow.FRM_SelectedPVName.Text = "";
                PVListFormWindow.FilterTimer.Interval = 20;
                PVListFormWindow.FRM_PVList.Items.Clear();
                PVListFormWindow.FRM_PVList.MultiSelect = false;
                PVListFormWindow.FRM_PVListIOCName.Text = "PV Finder by EASY ESS SCAN";
                PVListFormWindow.FRM_IOC_IP.Text = FRM_MainGateway.Text;
                PVListFormWindow.ShowDialog();
            }

        }

        private bool SendSMS(string PhoneNumber, string MessageTitle, string MessageBody)
        {
            bool retval = false;
            if (PhoneNumber.Length > 6)
            {
                if (PhoneNumber.StartsWith("+"))
                {
                    if ((MessageTitle != string.Empty) && (MessageBody != string.Empty))
                    {
                        if (MessageTitle.Length <= 11)
                        {
                            try
                            {
                                byte[] AuthBytes = Encoding.ASCII.GetBytes("passwordreset:45HBBp9M");
                                string sAuth = Convert.ToBase64String(AuthBytes);
                                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://sms-pro.net:44343/services/ERICESS/sendsms");
                                request.Headers.Add("Authorization", "Basic " + sAuth);
                                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None;
                                request.Proxy = new WebProxy(FRM_ProxyURL.Text, Convert.ToInt32(FRM_ProxyPort.Text));
                                request.PreAuthenticate = true;

                                request.Method = "POST";
                                request.ContentType = "text/xml";

                                byte[] bytes;
                                bytes = System.Text.Encoding.ASCII.GetBytes(@"<?xml version=""1.0"" encoding=""ISO-8859-1""?><mobilectrl_sms><header><customer_id>ERICESS</customer_id><password>yQvR3s68</password><from_alphanumeric>" + MessageTitle + @"</from_alphanumeric></header><payload><sms account = ""71700""><message><![CDATA[" + MessageBody + @"]]></message><to_msisdn>" + PhoneNumber + @"</to_msisdn></sms></payload></mobilectrl_sms>");

                                Stream requestStream = request.GetRequestStream();
                                requestStream.Write(bytes, 0, bytes.Length);
                                requestStream.Close();
                                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                                string responseStr = string.Empty;
                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    Stream responseStream = response.GetResponseStream();
                                    responseStr = new StreamReader(responseStream).ReadToEnd();
                                }
                                if (responseStr.Contains("SMS request is being processed"))
                                {
                                    retval = true;
                                    Log("SMS sent on number " + PhoneNumber, Color.DarkGreen);
                                }
                                else
                                {
                                    Log("SMS sending error on " + PhoneNumber + " response from Telenor: " + responseStr, Color.DarkGreen);
                                    MessageBox.Show(responseStr);
                                }
                            } catch (Exception ex)
                            {

                                Log("SMS send error:" + ex.Message , Color.Red);
                            }
                        }
                        else
                        {
                            Log("SMS title too long:" + MessageTitle + " (max 11 char)!", Color.Red);
                        }

                    }
                    else
                    {
                        Log("SMS title and body has to non empty strings!", Color.Red);
                    }
                } else
                {
                    Log("SMS phone number has to start with +", Color.Red);
                }
            }
            else
            {
                Log("SMS phone number too short!", Color.Red);
            }

            return retval;
        }

        private void FRM_TestSMSSend_Click(object sender, EventArgs e)
        {

            DialogResult dialogResult = MessageBox.Show("Every SMS costs money for ESS, please consider before sending!", "SMS warning", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SendSMS(FRM_TestSMSPhoneNumber.Text, "ICSSMSTOOL", FRM_TestSMSMessage.Text);    
            }
            else if (dialogResult == DialogResult.No)
            {
                FRM_TestSMSMessage.Text = string.Empty;
            }


        }

        private void FRM_RedirectToWeb_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(FRM_RedirectToWeb.Tag.ToString());
        }

        public bool IsAlphaNumeric(string text)
        {
            return Regex.IsMatch(text, "^[a-zA-Z0-9]+$");
        }



        private void FRM_LogOutButton_Click(object sender, EventArgs e)
        {
            SMSLoggedIn = false;
            FRM_FullUserName.Text = string.Empty;
            FullUserName = string.Empty;
            FRM_LoginGroup.Enabled = true;
            FRM_LoginUsername.Text = string.Empty;
            FRM_LoginPassword.Text = string.Empty;
            FRM_LoginButton.Enabled = true;
            FRM_LogOutButton.Enabled = false;
            FRM_PhoneBook.Visible = false;
            FRM_PVManagementGroup.Visible = false;
            FRM_LoginUsername.Enabled = true;
            FRM_LoginPassword.Enabled = true;
            FRM_RuleGroup.Visible = false;


        }

        private void ReloadPhoneBook()
        {
            SqlConnection conn = null;
            try
            {

                string sql = "select distinct phonesetname from phoneset where username = '" + FRM_LoginUsername.Text.Trim() + "';";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                FRM_PhoneSetList.Items.Clear();
                FRM_PhoneSetList.Text = "";
                FRM_AddPhoneNumber.Enabled = false;
                FRM_RemovePhoneNumber.Enabled = false;
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        FRM_PhoneSetList.Items.Add(SqlData.GetString(0));
                    }
                }
                if (FRM_PhoneSetList.Items.Count > 0)
                {
                    FRM_PhoneSetList.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                Log("ReloadPhoneBook error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }

        private void ReloadPhoneNumbers()
        {
            SqlConnection conn = null;
            ListViewItem.ListViewSubItem _SubItem;
            try
            {

                string sql = "select phoneowner, phonenumber from phonebook where username = '" + FRM_LoginUsername.Text.Trim() + "' and phonesetname = '" + FRM_PhoneSetList.Text.Trim() + "';";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                FRM_PhoneNumberList.Items.Clear();
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        ListViewItem _Item = new ListViewItem(SqlData.GetString(0));
                        _Item.ForeColor = Color.Navy;
                        _Item.UseItemStyleForSubItems = false;

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, SqlData.GetString(1));
                        _SubItem.ForeColor = Color.Navy;
                        _Item.SubItems.Add(_SubItem);

                        _Item.ImageIndex = 0;

                        FRM_PhoneNumberList.Items.Add(_Item);

                    }
                }
            }
            catch (Exception ex)
            {
                Log("ReloadPhoneNumbers error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }


        private void FRM_LoginButton_Click(object sender, EventArgs e)
        {
            if (!SMSLoggedIn)
            {
                FRM_FullUserName.Text = string.Empty;
                if (this.IsAlphaNumeric(FRM_LoginUsername.Text) && FRM_LoginUsername.Text.Length <= 50 && FRM_LoginPassword.Text.Length <= 50)
                {
                    SqlConnection conn = null;

                    try
                    {
                        string sql = "select count(*) from users where username = @username and password = @password";

                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);

                        SqlParameter user = new SqlParameter();
                        user.ParameterName = "@username";
                        user.Value = FRM_LoginUsername.Text.Trim();
                        cmd.Parameters.Add(user);

                        SqlParameter pass = new SqlParameter();
                        pass.ParameterName = "@password";
                        pass.Value = Hasher.HashString(FRM_LoginPassword.Text.Trim());
                        cmd.Parameters.Add(pass);

                        conn.Open();

                        int count = (int)cmd.ExecuteScalar();

                        if (count > 0)
                        {
                            sql = "select fullname from users where username = '"+FRM_LoginUsername.Text.Trim()+"';";
                            cmd = new SqlCommand(sql, conn);
                            SqlDataReader SqlData = cmd.ExecuteReader();
                            if (SqlData.HasRows)
                            {
                                while (SqlData.Read())
                                {
                                    Log("Welcome: "+ SqlData.GetString(0) + "!", Color.DarkGreen);
                                    FullUserName = SqlData.GetString(0);
                                }
                            }
                            else
                            {
                                Log("Logged in!", Color.DarkGreen);
                            }
                            SqlData.Close();
                            FRM_LoginUsername.Enabled = false;
                            FRM_LoginPassword.Enabled = false;
                            FRM_LogOutButton.Enabled = true;
                            FRM_LoginButton.Enabled = false;
                            FRM_FullUserName.Text = "Welcome: " + FullUserName;

                            FRM_PhoneBook.Visible = true;
                            FRM_PVManagementGroup.Visible = true;
                            FRM_RuleGroup.Visible = true;
                            ReloadPhoneBook();
                            ReloadPVList();
                            ReloadRuleList();
                        }
                        else
                        {
                            Log("Login faliure!", Color.Red);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log("Login error: " + ex.Message, Color.Red);
                        // Log your error
                    }
                    finally
                    {
                        if (conn != null) conn.Close();
                    }
                }
                else
                {
                    Log("Password or username have wrong format!", Color.Red);
                }
            }

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void FRM_AddPhoneSetButton_Click(object sender, EventArgs e)
        {
            FRM_PhoneSetList.Enabled = false;
            FRM_AddPhoneSetGroup.Visible = true;
            FRM_AddPhoneSetButton.Enabled = false;
            FRM_RemovePhoneSetButton.Enabled = false;
            FRM_AddPhoneNumber.Enabled = false;
            FRM_RemovePhoneNumber.Enabled = false;
        }

        private void FRM_RemovePhoneSetButton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                if (FRM_PhoneSetList.SelectedIndex >= 0)
                {
                    string sql = "delete from phoneset where username = '" + FRM_LoginUsername.Text.Trim() + "' and phonesetname = '" + FRM_PhoneSetList.Text.Trim() + "';";
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    conn.Open();
                    int count = (int)cmd.ExecuteNonQuery();
                    if (count > 0)
                    {
                        if (conn != null) conn.Close();
                        sql = "delete from phonebook where username = '" + FRM_LoginUsername.Text.Trim() + "' and phonesetname = '" + FRM_PhoneSetList.Text.Trim() + "';";
                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        cmd = new SqlCommand(sql, conn);
                        conn.Open();
                        count = (int)cmd.ExecuteNonQuery();
                        if (count > 0)
                        {
                            Log("Removing PhoneSet done.", Color.Green);
                            ReloadPhoneBook();
                        }
                        if (conn != null) conn.Close();
                    }

                }
                else
                {
                    Log("Select a PhoneSet first!", Color.Red);
                }

            }
            catch (Exception ex)
            {
                Log("Removing PhoneSet error: " + ex.Message, Color.Red);
            }
            finally
            {
                if (conn != null) conn.Close();
                FRM_NewPhoneSetName.Text = string.Empty;
                FRM_AddPhoneSetGroup.Visible = false;
                FRM_AddPhoneSetButton.Enabled = true;
                FRM_RemovePhoneSetButton.Enabled = true;
                FRM_AddPhoneNumber.Enabled = true;
                FRM_RemovePhoneNumber.Enabled = true;
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void FRM_AddPhoneSetButtonCancel_Click(object sender, EventArgs e)
        {
            FRM_PhoneSetList.Enabled = true;
            FRM_NewPhoneSetName.Text = string.Empty;
            FRM_AddPhoneSetGroup.Visible = false;
            FRM_AddPhoneSetButton.Enabled = true;
            FRM_RemovePhoneSetButton.Enabled = true;
            FRM_AddPhoneNumber.Enabled = true;
            FRM_RemovePhoneNumber.Enabled = true;
        }

        private void FRM_AddNewPhoneSetButton_Click(object sender, EventArgs e)
        {

            string PhoneSet = FRM_NewPhoneSetName.Text.Trim();
            bool Exists = false;

            SqlConnection conn = null;
            try
            {

                string sql = "select phonesetname from phoneset where phonesetname = '" + PhoneSet + "';";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        Exists = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log("ReadPhoneBook error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }



            if (Exists == false)
            {
                conn = null;
                try
                {
                    if (PhoneSet != string.Empty)
                    {
                        string sql = "insert into phoneset (username, phonesetname) values  ('" + FRM_LoginUsername.Text.Trim() + "','" + PhoneSet + "');";
                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        conn.Open();
                        int count = (int)cmd.ExecuteNonQuery();
                        if (count > 0)
                        {
                            Log("Adding new PhoneSet done.", Color.Green);
                            ReloadPhoneBook();
                        }
                    }
                    else
                    {
                        Log("PhoneSet name must be non empty! ", Color.Red);

                    }

                }
                catch (Exception ex)
                {
                    Log("Adding new PhoneSet error: " + ex.Message, Color.Red);
                }
                finally
                {
                    if (conn != null) conn.Close();
                }
            } else
            {
                MessageBox.Show(PhoneSet + " is already used in the system. Choose a different one!");
            }
            FRM_NewPhoneSetName.Text = string.Empty;
            FRM_AddPhoneSetGroup.Visible = false;
            FRM_AddPhoneSetButton.Enabled = true;
            FRM_RemovePhoneSetButton.Enabled = true;
            FRM_AddPhoneNumber.Enabled = true;
            FRM_RemovePhoneNumber.Enabled = true;
            FRM_PhoneSetList.Enabled = true;

        }

        private void FRM_AddPhoneNumber_Click(object sender, EventArgs e)
        {
            FRM_PhoneSetList.Enabled = false;
            FRM_NewPhoneNumber.Text = string.Empty;
            FRM_NewPhoneOwner.Text = string.Empty;
            FRM_NewPhoneNumberGroup.Visible = true;
            FRM_AddPhoneSetButton.Enabled = false;
            FRM_RemovePhoneSetButton.Enabled = false;
            FRM_AddPhoneNumber.Enabled = false;
            FRM_RemovePhoneNumber.Enabled = false;

        }

        private void FRM_AddNewPhoneNumberButtonCancel_Click(object sender, EventArgs e)
        {
            FRM_NewPhoneNumber.Text = string.Empty;
            FRM_NewPhoneOwner.Text = string.Empty;
            FRM_NewPhoneNumberGroup.Visible = false;
            FRM_AddPhoneSetButton.Enabled = true;
            FRM_RemovePhoneSetButton.Enabled = true;
            FRM_AddPhoneNumber.Enabled = true;
            FRM_RemovePhoneNumber.Enabled = true;
            FRM_PhoneSetList.Enabled = true;

        }

        private void FRM_AddNewPhoneNumberButton_Click(object sender, EventArgs e)
        {
            string Owner = FRM_NewPhoneOwner.Text.Trim();
            string Number = FRM_NewPhoneNumber.Text.Trim();
            SqlConnection conn = null;
            try
            {
                if ((Owner != string.Empty) && (Number != string.Empty))
                {
                    if (Number.StartsWith("+"))
                    {
                        string sql = "insert into phonebook (username, phonesetname,phoneowner,phonenumber) values  ('" + FRM_LoginUsername.Text.Trim() + "','" + FRM_PhoneSetList.Text.Trim() + "','" + Owner + "','" + Number + "');";
                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        conn.Open();
                        int count = (int)cmd.ExecuteNonQuery();
                        if (count > 0)
                        {
                            Log("Adding new PhoneNumber to PhoneSet done.", Color.Green);
                            ReloadPhoneBook();
                        }
                    }
                    else
                    {
                        Log("PhoneNumber should start with + !", Color.Red);
                    }

                }
                else
                {
                    Log("PhoneOwner and PhoneNumber has to be non empty!", Color.Red);
                }

            }
            catch (Exception ex)
            {
                Log("Adding new phone number to PhoneSet error: " + ex.Message, Color.Red);
            }
            finally
            {
                if (conn != null) conn.Close();
                FRM_NewPhoneNumber.Text = string.Empty;
                FRM_NewPhoneOwner.Text = string.Empty;
                FRM_NewPhoneNumberGroup.Visible = false;
                FRM_AddPhoneSetButton.Enabled = true;
                FRM_RemovePhoneSetButton.Enabled = true;
                FRM_AddPhoneNumber.Enabled = true;
                FRM_RemovePhoneNumber.Enabled = true;
                FRM_PhoneSetList.Enabled = true;
            }

        }

        private void FRM_PhoneSetList_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (FRM_PhoneSetList.Text != string.Empty)
            {
                FRM_AddPhoneNumber.Enabled = true;
                FRM_RemovePhoneNumber.Enabled = true;
                ReloadPhoneNumbers();
            }
        }

        private void FRM_RemovePhoneNumber_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                if (FRM_PhoneNumberList.SelectedItems.Count > 0)
                {
                        string Owner  = FRM_PhoneNumberList.SelectedItems[0].Text;
                        string Number = FRM_PhoneNumberList.SelectedItems[0].SubItems[1].Text;
                        string sql = "delete from phonebook where username = '" + FRM_LoginUsername.Text.Trim() + "' and phonesetname = '" + FRM_PhoneSetList.Text.Trim() + "'  and phoneowner = '" + Owner + "' and phonenumber = '" + Number + "';";
                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        conn.Open();
                        int count = (int)cmd.ExecuteNonQuery();
                        if (count > 0)
                        {
                            Log("Removing PhoneNumber from PhoneSet done.", Color.Green);
                            ReloadPhoneBook();
                        }

                }
                else
                {
                    Log("Select a PhoneNumber first!", Color.Red);
                }

                FRM_NewPhoneNumber.Text = string.Empty;
                FRM_NewPhoneOwner.Text = string.Empty;
                FRM_NewPhoneNumberGroup.Visible = false;
                FRM_AddPhoneSetButton.Enabled = true;
                FRM_RemovePhoneSetButton.Enabled = true;
                FRM_AddPhoneNumber.Enabled = true;
                FRM_RemovePhoneNumber.Enabled = true;
            }
            catch (Exception ex)
            {
                Log("Removing phone number from PhoneSet error: " + ex.Message, Color.Red);
            }
            finally
            {
                if (conn != null) conn.Close();
            }

        }

        private void FRM_PhoneNumberList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FRM_AddPV_Click(object sender, EventArgs e)
        {
            FRM_PVList.Enabled = false;
            FRM_AddPV.Enabled = false;
            FRM_RemovePV.Enabled = false;
            FRM_AddPVGroup.Visible = true;
        }

        private void FRM_AddNewPVButtonCancel_Click(object sender, EventArgs e)
        {
            FRM_PVList.Enabled = true;
            FRM_AddPV.Enabled = true;
            FRM_RemovePV.Enabled = true;
            FRM_AddPVGroup.Visible = false;
            FRM_NewPVName.Text = string.Empty;

        }

        private void ReloadPVList()
        {
            SqlConnection conn = null;
            ListViewItem.ListViewSubItem _SubItem;
            try
            {

                string sql = "select pvname,vartype from pvlist where username = '" + FRM_LoginUsername.Text.Trim() + "' order by pvname ;";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                FRM_PVList.Items.Clear();
                int i = 0;
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        i++;
                        ListViewItem _Item = new ListViewItem(i.ToString());
                        _Item.ForeColor = Color.Navy;
                        _Item.UseItemStyleForSubItems = false;

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, SqlData.GetString(0));
                        _SubItem.ForeColor = Color.Navy;
                        _Item.SubItems.Add(_SubItem);

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, SqlData.GetString(1));
                        _SubItem.ForeColor = Color.Navy;
                        _Item.SubItems.Add(_SubItem);

                        FRM_PVList.Items.Add(_Item);

                    }
                }
            }
            catch (Exception ex)
            {
                Log("Refresh PV list error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }

        private void FRM_AddNewPVButton_Click(object sender, EventArgs e)
        {
            if (PVListFormWindow.SelectedPVID.Count == 1)
            {
                if ((FRM_NewPVName.Text != string.Empty) && (FRM_NewPVName.Text.Contains(":")))
                {
                    SqlConnection conn = null;
                    try
                    {
                        string sql = "insert into pvlist (username, pvname,ipaddress,vartype,lastvalue,lastread) values  ('" + FRM_LoginUsername.Text.Trim() + "','" + FRM_NewPVName.Text + "','" + FRM_NewPVIP.Text + "','" + FRM_NewPVType.Text + "','','');";
                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        conn.Open();
                        int count = (int)cmd.ExecuteNonQuery();
                        if (count > 0)
                        {
                            Log("Adding new PV done.", Color.Green);
                            ReloadPVList();
                        }

                    }
                    catch (Exception ex)
                    {
                        Log("Adding new PV error: " + ex.Message, Color.Red);
                    }
                    finally
                    {
                        if (conn != null) conn.Close();
                    }
                }
                else
                {
                    Log("PV name has a wrong format or empty!", Color.Red);
                }
            }
            if (PVListFormWindow.SelectedPVID.Count > 1)
            {
                for (int i = 0; i < PVListFormWindow.SelectedPVID.Count; i++)
                {
                    if ((PVListFormWindow.SelectedPVName[i] != string.Empty) && (PVListFormWindow.SelectedPVName[i].Contains(":")))
                    {
                        SqlConnection conn = null;
                        try
                        {
                            string sql = "insert into pvlist (username, pvname,ipaddress,vartype,lastvalue,lastread) values  ('" + FRM_LoginUsername.Text.Trim() + "','" + PVListFormWindow.SelectedPVName[i] + "','" + PVListFormWindow.SelectedPVIP[i] + "','" + PVListFormWindow.SelectedPVType[i] + "','','');";
                            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                            SqlCommand cmd = new SqlCommand(sql, conn);
                            conn.Open();
                            int count = (int)cmd.ExecuteNonQuery();
                            if (count > 0)
                            {
                                Log("Adding new PVs done.", Color.Green);
                                ReloadPVList();
                            }

                        }
                        catch (Exception ex)
                        {
                            Log("Adding new PVs error: " + ex.Message, Color.Red);
                        }
                        finally
                        {
                            if (conn != null) conn.Close();
                        }
                    }
                    else
                    {
                        Log("PV name ["+PVListFormWindow.SelectedPVName[i]+"] has a wrong format or empty!", Color.Red);
                    }
                }
            }

            FRM_AddPV.Enabled = true;
            FRM_RemovePV.Enabled = true;
            FRM_AddPVGroup.Visible = false;
            FRM_NewPVName.Text = string.Empty;
            FRM_PVList.Enabled = true;
        }

        private void FRM_RemovePV_Click(object sender, EventArgs e)
        {
            if (FRM_PVList.SelectedItems.Count>0)
            {
                SqlConnection conn = null;
                try
                {
                    string sql = "delete from pvlist where username = '" + FRM_LoginUsername.Text.Trim() + "' and pvname = '" + FRM_PVList.SelectedItems[0].SubItems[1].Text + "';";
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    conn.Open();
                    int count = (int)cmd.ExecuteNonQuery();
                    if (count > 0)
                    {
                        Log("Removing PV done.", Color.Green);
                        ReloadPVList();
                    }

                }
                catch (Exception ex)
                {
                    Log("Removing PV error: " + ex.Message, Color.Red);
                }
                finally
                {
                    if (conn != null) conn.Close();
                }
            }
            else
            {
                Log("Select a PV first!", Color.Red);
            }
            FRM_NewPVName.Text = string.Empty;

        }

        private void FRM_OpenPVSearchList_Click(object sender, EventArgs e)
        {
            if (OverallList.Count > 1)
            {
                FRM_NewPVName.Text = string.Empty;
                FRM_NewPVIP.Text = string.Empty;
                FRM_NewPVType.Text = string.Empty;
                RefreshPVs(-1);
                PVListFormWindow.FRM_SelectedPVName.Text = "";
                PVListFormWindow.FRM_PVList.Items.Clear();
                PVListFormWindow.FilterTimer.Interval = 20;
                PVListFormWindow.FRM_PVList.MultiSelect = true;
                PVListFormWindow.FRM_PVListIOCName.Text = "PV Finder by EASY ESS SCAN";
                PVListFormWindow.FRM_IOC_IP.Text = FRM_MainGateway.Text;
                PVListFormWindow.FRM_ChoosePV.Visible = true;
                PVListFormWindow.FRM_ReadPVs.Visible = false;
                PVListFormWindow.ShowDialog();

                if (PVListFormWindow.SelectedPVID.Count == 1)
                {
                    FRM_NewPVName.Text = PVListFormWindow.SelectedPVName[0];
                    FRM_NewPVIP.Text = PVListFormWindow.SelectedPVIP[0];
                    if (PVListFormWindow.SelectedPVType.Count > 0)
                    {
                        FRM_NewPVType.Text = PVListFormWindow.SelectedPVType[0];
                    }
                }
                if (PVListFormWindow.SelectedPVID.Count > 1)
                {
                    FRM_NewPVName.Text = "Click ADD to add all selected ("+ PVListFormWindow.SelectedPVID.Count.ToString() + ") PVs";
                    FRM_NewPVIP.Text = string.Empty;
                    FRM_NewPVType.Text = string.Empty;
                }
            }
            else
            {
                Log("Load Online or Offline Database first from the tab [Load Configuration from ESS tools]", Color.Red);
            }


        }

        private void copyPVNameToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FRM_PVList.SelectedItems.Count > 0)
            {
                System.Windows.Forms.Clipboard.SetText(FRM_PVList.SelectedItems[0].SubItems[1].Text);
            }

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (FRM_PhoneNumberList.SelectedItems.Count > 0)
            {
                System.Windows.Forms.Clipboard.SetText(FRM_PhoneNumberList.SelectedItems[0].SubItems[1].Text);
            }

        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FRM_CheckRule_Click(object sender, EventArgs e)
        {
            FRM_RulePanel.Visible = false;
            FRM_RulePanel.Width = 510;

        }

        private void ReloadRuleList()
        {
            
            SqlConnection conn = null;
            ListViewItem.ListViewSubItem _SubItem;
            try
            {

                string sql = "select rulename,syntax from monitor where username = '" + FRM_LoginUsername.Text.Trim() + "' order by rulename ;";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                FRM_RuleList.Items.Clear();
                int i = 0;
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        i++;
                        ListViewItem _Item = new ListViewItem(SqlData.GetString(0));
                        _Item.ForeColor = Color.Navy;
                        _Item.UseItemStyleForSubItems = false;

                        _SubItem = new ListViewItem.ListViewSubItem(_Item, SqlData.GetString(1));
                        _SubItem.ForeColor = Color.Navy;
                        _Item.SubItems.Add(_SubItem);

                        
                        _Item.ImageIndex = 0;
                        FRM_RuleList.Items.Add(_Item);

                    }
                }
            }
            catch (Exception ex)
            {
                Log("Refresh Rule list error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }


        private void FRM_AddNewRuleButton_Click(object sender, EventArgs e)
        {
            if ((FRM_RuleName.Text != string.Empty) && (FRM_RuleName.Text.Length>4))
            {
                SqlConnection conn = null;
                try
                {
                    string sql = "insert into monitor (username, rulename,phonesetname,syntax,statustext,rulebody,smstext,lasttrigger,pvlist,triggerstate,initvalue,lastvalue,triggercount,disctriggercount,controlstate,needdisconsms) values  ('" + FRM_LoginUsername.Text.Trim() + "','" + FRM_RuleName.Text+ "','','','NEW','','','','','','','',0,0,'','NO');";
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    conn.Open();
                    int count = (int)cmd.ExecuteNonQuery();
                    if (count > 0)
                    {
                        Log("Adding new Rule done.", Color.Green);
                        ReloadRuleList();
                    }

                }
                catch (Exception ex)
                {
                    Log("Adding new Rule error: " + ex.Message, Color.Red);
                }
                finally
                {
                    if (conn != null) conn.Close();
                }

            }
            else
            {
                Log("Rule add error: Rule name is empty or too short. (min 5 character)!", Color.Red);
            }
            FRM_FRM_AddRuleGroup.Visible = false;
            FRM_RuleList.Enabled = true;
            FRM_RemoveRuleButton.Enabled = true;
            FRM_RuleName.Text = string.Empty;

        }

        private void FRM_AddRuleButton_Click(object sender, EventArgs e)
        {
            FRM_FRM_AddRuleGroup.Visible = true;
            FRM_RuleList.Enabled = false;
            FRM_RemoveRuleButton.Enabled = false;
        }

        private void FRM_AddNewRuleButtonCancel_Click(object sender, EventArgs e)
        {
            FRM_RuleName.Text = string.Empty;
            FRM_FRM_AddRuleGroup.Visible = false;
            FRM_RuleList.Enabled = true;
            FRM_RemoveRuleButton.Enabled = true;

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (FRM_PhoneSetList.Items.Count > 0)
            {
                if (FRM_RuleList.SelectedItems.Count > 0)
                {
                    string phonesetname = string.Empty;
                    string syntax = string.Empty;
                    string statustext = string.Empty;
                    string rulebody = string.Empty;
                    string smstext = string.Empty;
                    string triggerstate = string.Empty;
                    string need_discon_sms = string.Empty;
                    FRM_PhoneBook.Visible = false;
                    SqlConnection conn = null;
                    try
                    {

                        string sql = "select phonesetname,syntax,statustext,rulebody,smstext,triggerstate,needdisconsms from monitor where username = '" + FRM_LoginUsername.Text.Trim() + "' and rulename= '" + FRM_RuleList.SelectedItems[0].Text + "'  ;";
                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        conn.Open();
                        SqlDataReader SqlData = cmd.ExecuteReader();
                        if (SqlData.HasRows)
                        {
                            while (SqlData.Read())
                            {
                                phonesetname = SqlData.GetString(0);
                                syntax = SqlData.GetString(1);
                                statustext = SqlData.GetString(2);
                                rulebody = SqlData.GetString(3);
                                smstext = SqlData.GetString(4);
                                triggerstate = SqlData.GetString(5);
                                need_discon_sms = SqlData.GetString(6);
                            }
string DefaultRule = @"//Example Rule
//Trigger if Pressure (PV1) is grater than 5.5bar OR High Alarm (PV2)
//Trigger only if the machine is on (PV3 == 1)
//@BIT1 = @PV1 > 5.5
//@BIT2 = @PV2 == 1
//@BIT3 = @BIT1 OR @BIT2
//@BIT4 = @PV3 == 1
//@RULE = @BIT3 AND @BIT4
";
                            PN_PhoneSet.Items.Clear();
                            for (int i = 0; i < FRM_PhoneSetList.Items.Count; i++)
                            {
                                PN_PhoneSet.Items.Add(FRM_PhoneSetList.Items[i]);
                            }
                            if (phonesetname != string.Empty)
                            {
                                PN_PhoneSet.Text = phonesetname;
                            }
                            if (rulebody == string.Empty)
                            {
                                shtb.Text = DefaultRule;
                            } else
                            {
                                shtb.Text = rulebody;
                            }
                            if (smstext != string.Empty)
                            {
                                PN_SMSText.Text = smstext;
                            }

                            FRM_AddPV.Enabled = false;
                            FRM_RemovePV.Enabled = false;
                            FRM_AddRuleButton.Enabled = false;
                            FRM_RemoveRuleButton.Enabled = false;
                            PN_RuleName.Text = FRM_RuleList.SelectedItems[0].Text;
                            FRM_RulePanel.Visible = true;
                            FRM_RulePanel.Width = 510;
                            CheckRuleSyntax(false);
                            PN_RuleErrors.BackColor = PN_RuleErrors.BackColor;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log("Read Rule properties error: " + ex.Message, Color.Red);
                        // Log your error
                    }
                    finally
                    {
                        if (conn != null) conn.Close();
                    }

                }
                else
                {
                    Log("Select a rule first!", Color.Red);
                }
            }
            else
            {
                Log("You can't edit rule until there are PhoneSets configured!", Color.Red);
            }

        }

        private void PN_SaveRule_Click(object sender, EventArgs e)
        {
            CheckRuleSyntax(true);

            if (PN_RuleErrors.Text == string.Empty)
            {
                for (int pv = 0; pv < FRM_PVList.Items.Count; pv++)
                {
                    for (int i = 0; i < RuleBIT.Count; i++)
                    {
                        if (RuleBIT[i].BitNumber>0)
                        {
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("@", "");
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("PV:" + FRM_PVList.Items[pv].SubItems[1].Text, "1");
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("OP:EQ", "=");
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("OP:LE", "<=");
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("OP:GE", ">=");
                        }
                    }
                    RuleMain.Expression = RuleMain.Expression.Replace("@", "");
                    RuleMain.Expression = RuleMain.Expression.Replace("PV:" + FRM_PVList.Items[pv].SubItems[1].Text, "1");
                    RuleMain.Expression = RuleMain.Expression.Replace("OP:EQ", "=");
                    RuleMain.Expression = RuleMain.Expression.Replace("OP:LE", "<=");
                    RuleMain.Expression = RuleMain.Expression.Replace("OP:GE", ">=");
                }



                RuleBIT = RuleBIT;
                RuleMain = RuleMain;
                try
                {
                    ExpressionContext context = new ExpressionContext();
                    context.Options.ParseCulture = CultureInfo.InvariantCulture;
                    VariableCollection variables = context.Variables;
                    for (int i = 0; i < RuleBIT.Count; i++)
                    {
                        if (RuleBIT[i].BitNumber > 0)
                        {
                            for (int key = 0; key < 100; key++)
                            {
                                if (RuleBIT[i].Expression.IndexOf("BIT" + key.ToString()) > -1)
                                {
                                    string bitvalue = string.Empty;
                                    for (int ii = 0; ii < RuleBIT.Count; ii++)
                                    {
                                        if (RuleBIT[ii].BitNumber == key)
                                        {
                                            bitvalue = RuleBIT[ii].Value.ToString();
                                        }
                                    }
                                    RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("BIT" + key.ToString(), bitvalue);
                                }
                            }
                            IGenericExpression<bool> exp = context.CompileGeneric<bool>("("+ RuleBIT[i].Expression + ")");
                            RuleBIT[i].Value = exp.Evaluate();
                        }
                    }

                    for (int i=0;i<100;i++)
                    {
                        string bitexp = string.Empty;
                        for (int ii=0;ii<RuleBIT.Count;ii++)
                        {
                            if (RuleBIT[ii].BitNumber==i)
                            {
                                bitexp = RuleBIT[ii].Value.ToString();
                            }
                        }
                        RuleMain.Expression = RuleMain.Expression.Replace("BIT" + i.ToString(), bitexp );
                    }

                    IGenericExpression<bool> mainexp = context.CompileGeneric<bool>("(" + RuleMain.Expression + ")");
                    RuleMain.Value = mainexp.Evaluate();

                }
                catch (Exception ex)
                {
                    PN_RuleErrors.AppendText("Compile error: " + ex.Message + "!\n");
                }
                FRM_PhoneBook.Visible = true;
            }


            string syntax = string.Empty;
            if (PN_RuleErrors.Text != string.Empty)
            {
                MessageBox.Show("There are sysntax errors in the script. This script can't be runned by the EasySMS engine!");
                syntax = "ERROR";
            } else
            {
                syntax = "OK";
            }



            string triggerstate = string.Empty;
            SqlConnection conn = null;
            try
            {
                string need_discon_sms = "NO";
                if (PN_NeedDisconnect.Checked) { need_discon_sms = "YES"; }
                string sql = "update monitor set phonesetname='"+PN_PhoneSet.Text+"',syntax='"+syntax+ "',statustext = 'MODIFIED',  rulebody='" + shtb.Text + "',smstext='" + PN_SMSText.Text + "',triggerstate ='" + triggerstate + "',needdisconsms = '"+need_discon_sms+"' where username = '" + FRM_LoginUsername.Text.Trim() + "' and rulename= '" + PN_RuleName.Text + "'  ;";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                int count = (int)cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    Log("Modifing "+PN_RuleName.Text+"  done.", Color.Green);
                    ReloadRuleList();
                }
            }
            catch (Exception ex)
            {
                Log("Modify Rule properties error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }

            FRM_AddPV.Enabled = true;
            FRM_RemovePV.Enabled = true;
            FRM_AddRuleButton.Enabled = true;
            FRM_RemoveRuleButton.Enabled = true;
            FRM_RulePanel.Visible = false;
            FRM_RulePanel.Width = 510;
        }

        public string Between(string STR, string FirstString, string LastString)
        {
            string FinalString;
            int Pos1 = STR.IndexOf(FirstString) + FirstString.Length;
            int Pos2 = STR.IndexOf(LastString);
            FinalString = STR.Substring(Pos1, Pos2 - Pos1);
            return FinalString;
        }

        private void CheckRuleSyntax(bool doreplace)
        {
            RuleBIT = new List<RuleBits>();
            RuleMain = new RuleBits();
            for (int i = 0; i < 100; i++)
            {
                RuleBits Item = new RuleBits();
                RuleBIT.Add(Item);
            }

            PN_RuleErrors.Text = string.Empty;
            
            int savepos = shtb.CurrentPosition;
            bool retvalOK = false;
            if (PN_PhoneSet.Text == string.Empty)
            {
                PN_RuleErrors.AppendText("Select a phoneset from the list below!\n");

            }
            if (PN_SMSText.Text == string.Empty)
            {
                PN_RuleErrors.AppendText("Enter an SMS text below!\n");

            }

            string[] shtblines = shtb.Text.Split('\n');
            string[] shtblineschk = shtb.Text.Replace("==","OP:EQ").Replace("<=", "OP:LE").Replace(">=", "OP:GE").Split('\n');
            bool change = false;
            int changelength = 0;
            bool PVFound = false;
            for (int lin = 0; lin < shtblines.Length; lin++)
            {
                if (!shtblines[lin].StartsWith("//"))
                {
                    if (doreplace)
                    {
                        for (int i = 1; i < 100; i++)
                        {
                            //Replace PV name
                            if ((shtblines[lin].IndexOf(@"@PV" + i.ToString() + " ") > -1) || (shtblines[lin].IndexOf(@"@PV" + i.ToString() + "\n") > -1))
                            {
                                if (FRM_PVList.Items.Count >= i)
                                {
                                    shtblines[lin] = shtblines[lin].Replace("@PV" + i.ToString(), "@PV:" + FRM_PVList.Items[i - 1].SubItems[1].Text + " ");
                                    change = true;
                                    changelength = FRM_PVList.Items[i - 1].SubItems[1].Text.Length;
                                    PVFound = true;
                                }
                                else
                                {
                                    PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": PV" + i.ToString() + " is not in the PV list!\n");
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i < 100; i++)
                        {
                            //Replace PV name
                            if ((shtblines[lin].IndexOf(@"@PV" + i.ToString() + " ") > -1) || (shtblines[lin].IndexOf(@"@PV" + i.ToString() + "\n") > -1))
                            {
                                if (FRM_PVList.Items.Count >= i)
                                {
                                    PVFound = true;
                                }
                                else
                                {
                                    PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": PV" + i.ToString() + " is not in the PV list!\n");
                                }
                            }
                        }
                    }
                    if ((shtblineschk[lin] != "\n") && (shtblineschk[lin] != string.Empty))
                    {
                        // Check operators
                        if ((shtblineschk[lin].Contains("(")) || (shtblineschk[lin].Contains(")")))
                        {
                            PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": not permitted characters '(' ')'!\n");
                        }
                        if ((shtblineschk[lin].Contains("{")) || (shtblineschk[lin].Contains("}")))
                        {
                            PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": not permitted characters '{' '}'!\n");
                        }
                        if ((shtblineschk[lin].Contains("[")) || (shtblineschk[lin].Contains("]")))
                        {
                            PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": not permitted characters '[' ']'!\n");
                        }
                        if ((shtblineschk[lin].Contains("&")) || (shtblineschk[lin].Contains("|")))
                        {
                            PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": not permitted characters '|' '&' use AND, OR instead!\n");
                        }
                        if (shtblineschk[lin].Contains(";"))
                        {
                            PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": not permitted character ';'\n");
                        }
                        if (shtblineschk[lin].Contains("="))
                        {
                            if ((shtblineschk[lin].Contains("<")) || (shtblineschk[lin].Contains(">")) || (shtblineschk[lin].Contains("==")) || (shtblineschk[lin].Contains("<=")) || (shtblineschk[lin].Contains(">=")))
                            {

                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": does not contain any '=' operator!\n");

                        }
                        if (shtblineschk[lin].IndexOf(@"@BIT ") > -1)
                        {
                            PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": BIT usage without a BIT number!\n");
                        }
                        for (int i = 1; i < 100; i++)
                        {
                            //Check BITs usage
                            if ((shtblineschk[lin].IndexOf(@"@BIT" + i.ToString() + " ") > -1) || (shtblineschk[lin].IndexOf(@"@BIT" + i.ToString()) > -1))
                            {

                                string[] parts = shtblineschk[lin].Split('=');
                                if (parts.Length == 1)
                                {

                                }
                                if (parts.Length > 0)
                                {
                                    if (parts.Length == 2)
                                    {
                                        if (parts[0].IndexOf(@"@BIT" + i.ToString()) > -1)
                                        {
                                            RuleBIT[i].BitNumber = i;
                                            RuleBIT[i].Created = true;
                                            RuleBIT[i].Expression = parts[1];
                                        }
                                        if (parts[1].IndexOf(@"@BIT" + i.ToString()) > -1)
                                        {
                                            RuleBIT[i].BitNumber = i;
                                            RuleBIT[i].Used = true;

                                        }
                                        if ((parts[0].IndexOf(@"@BIT" + i.ToString()) > -1)&& (parts[1].IndexOf(@"@BIT" + i.ToString()) > -1))
                                        {
                                            PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": @BIT" + i.ToString()+" can not use itself!\n");

                                        }
                                    }
                                    if (parts.Length > 2)
                                    {
                                        PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": contains more '=' operator!\n");

                                    }

                                }
                            }
                            else
                            { }

                        } // Bit usage

                        //Check PVs usage
                        if (shtblineschk[lin].IndexOf(@"@PV ") > -1)
                        {
                            PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": PV usage without a PV number!\n");
                        }
                        if (shtblineschk[lin].IndexOf(@"@PV") > -1)
                        {
                            string[] parts = shtblineschk[lin].Split('=');
                            if (parts.Length == 1)
                            {

                            }
                            if (parts.Length > 0)
                            {
                                if (parts.Length == 2)
                                {
                                    if (parts[0].IndexOf(@"@PV") > -1)
                                    {
                                        PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": PVs are read only!\n");

                                    }
                                }

                            }
                        }
                        //Check empty equal
                        string[] sublines = shtblineschk[lin].Split('=');
                        if (sublines.Length == 1)
                        {

                        }
                        if (sublines.Length > 0)
                        {
                            if (sublines.Length == 2)
                            {
                                if (sublines[0].Replace(" ", "").Length == 0)
                                {
                                    PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": Left hand side empty!\n");

                                }
                                if (!sublines[0].Contains("@"))
                                {
                                    PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": Left hand side has to contain @BIT, @PV, @RULE!\n");

                                }
                                if (sublines[1].Replace(" ", "").Length == 0)
                                {
                                    PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": Rigth hand side empty!\n");

                                }
                                //Check not permitted Variable names
                                if (sublines[0].IndexOf(@"@")>-1)
                                {
                                    if ((sublines[0].IndexOf(@"@PV") == -1) && (sublines[0].IndexOf(@"@BIT") == -1) && (sublines[0].IndexOf(@"@RULE") == -1))
                                    {
                                        PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": unpermitted @ usage on the left hand side!!\n");
                                    }

                                }
                                if (sublines[1].IndexOf(@"@") > -1)
                                {
                                    if ((sublines[1].IndexOf(@"@PV") == -1) && (sublines[1].IndexOf(@"@BIT") == -1) && (sublines[0].IndexOf(@"@RULE") == -1))
                                    {
                                        PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": unpermitted @ usage on the right hand side!!\n");
                                    }

                                }
                            }

                        }


                        //Check retval
                        if (shtblineschk[lin].IndexOf(@"@RULE") > -1)
                        {
                            string[] parts = shtblineschk[lin].Split('=');
                            if (parts.Length == 1)
                            {

                            }
                            if (parts.Length > 0)
                            {
                                if (parts.Length == 2)
                                {
                                    if (parts[0].IndexOf(@"@RULE") > -1)
                                    {
                                        retvalOK = true;
                                        RuleMain.Expression = parts[1];
                                    }
                                    if (parts[1].IndexOf(@"@RULE") > -1)
                                    {
                                        retvalOK = true;
                                        PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ": variable @RULE has to be on left hand side!\n");
                                    }
                                    if ((parts[0].IndexOf(@"@RULE") > -1) && (parts[1].IndexOf(@"@RULE") > -1))
                                    {
                                        PN_RuleErrors.AppendText("Line " + (lin + 1).ToString() + ":  @RULE can't use itself!\n");

                                    }

                                }

                            }
                        }


                    }// line not empty


                } // line not a comment
            }

            // Things to check after the whole rule was processed 
            for (int i = 1; i < 100; i++)
            {
                if (RuleBIT[i].BitNumber > 0)
                {
                    if ((RuleBIT[i].Created) && !(RuleBIT[i].Used))
                    {
                        PN_RuleErrors.AppendText("Warning: BIT" + i.ToString() + " value is never used!\n");

                    }
                    if (!(RuleBIT[i].Created) && (RuleBIT[i].Used))
                    {
                        PN_RuleErrors.AppendText("Error: BIT" + i.ToString() + " not declared!\n");

                    }
                }
            }
            if (retvalOK == false)
            {
                PN_RuleErrors.AppendText("Variable @RULE has to be used in this script!\n");
            }


            if (change)
            {
                shtb.Text = string.Join("\n", shtblines);
                shtb.SelectionStart = savepos + changelength;
                shtb.SelectionLength = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void FRM_RemoveRuleButton_Click(object sender, EventArgs e)
        {
            if (FRM_RuleList.SelectedItems.Count > 0)
            {
                SqlConnection conn = null;
                try
                {
                    string sql = "delete from monitor where username = '" + FRM_LoginUsername.Text.Trim() + "' and rulename = '" + FRM_RuleList.SelectedItems[0].Text + "';";
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    conn.Open();
                    int count = (int)cmd.ExecuteNonQuery();
                    if (count > 0)
                    {
                        Log("Removing Rule done.", Color.Green);
                        ReloadPVList();
                    }

                }
                catch (Exception ex)
                {
                    Log("Removing Rule error: " + ex.Message, Color.Red);
                }
                finally
                {
                    if (conn != null) conn.Close();
                }
            }
            else
            {
                Log("Select a Rule first!", Color.Red);
            }
            ReloadRuleList();
        }

        private void PN_SMSText_TextChanged(object sender, EventArgs e)
        {
            CheckRuleSyntax(false);
        }

        private void FRM_AddIOC_Click(object sender, EventArgs e)
        {
            if ((FRM_AddIOCName.Text != string.Empty) && (FRM_AddIOCIP.Text != string.Empty) && (FRM_AddIOCHostName.Text != string.Empty))
            {
                Overall OverallItem = new Overall();
                OverallItem.IP = FRM_AddIOCIP.Text;
                OverallItem.IOCName = FRM_AddIOCName.Text;
                OverallItem.HostName = FRM_AddIOCHostName.Text;
                OverallList.Add(OverallItem);
                RefreshOverallList("");

                SaveObjects();
                FRM_AddIOCName.Text = string.Empty;
                FRM_AddIOCIP.Text = string.Empty;
                FRM_AddIOCHostName.Text = string.Empty;
            } else
            {
                Log("IOC name, IP and HostName has to non empty to be able to add it to the list!", Color.Red);
            }

        }

        private void FRM_ManualAdd_Click(object sender, EventArgs e)
        {
            FRM_AddRemoveIOCGroup.Visible = !FRM_AddRemoveIOCGroup.Visible;
        }

        private void LoadTimer_Tick(object sender, EventArgs e)
        {
            LoadTimer.Enabled = false;
            FRM_LoadOffline.PerformClick();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            FRM_GetCCDB.Enabled = true;
            FRM_GetCSEntry.Enabled = true;
        }

        private void loadExternalPVListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((SelectedIOC >= 0) && (SelectedIOC < OverallList.Count))
            {
                OpenFileDialog theDialog = new OpenFileDialog();
                theDialog.Title = "Open Text File";
                theDialog.Filter = "TXT files|*.txt";
                if (theDialog.ShowDialog() == DialogResult.OK)
                {
                    Log("Opening PV list file.", Color.CadetBlue);
                    string filename = theDialog.FileName;
                    string[] filelines = File.ReadAllLines(filename);
                    List<string> PVList = new List<string>(filelines);
                    OverallList[SelectedIOC].PVs = new List<PV>();
                    for (int i = 0; i < PVList.Count; i++)
                    {
                            PV PVItem = new PV();
                            PVItem.name = Regex.Replace(PVList[i], @"\t|\n|\r", "");
                            PVItem.IOCName = OverallList[SelectedIOC].IOCName;
                            PVItem.IP = OverallList[SelectedIOC].IP;
                            OverallList[SelectedIOC].PVs.Add(PVItem);

                    }
                    if (OverallList[SelectedIOC].PVs.Count > 0)
                    {
                        Log("PV list pull done.", Color.CadetBlue);
                        RefreshPVs(SelectedIOC);
                        RefreshIOCDetails();
                        SaveObjects();
                    }
                }

            }
        }
    }
}
namespace Ini
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
                 string key, string def, StringBuilder retVal,
            int size, string filePath);

        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(100000);
            int i = GetPrivateProfileString(Section, Key, "", temp,
                                            100000, this.path);
            return temp.ToString();

        }
    }
}

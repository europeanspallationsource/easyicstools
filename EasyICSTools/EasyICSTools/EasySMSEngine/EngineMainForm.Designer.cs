﻿namespace EasySMSEngine
{
    partial class ENG_MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ENG_MainForm));
            this.label36 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ENG_StartStopGroup = new System.Windows.Forms.GroupBox();
            this.ENG_Progress = new System.Windows.Forms.ProgressBar();
            this.ENG_EngineState = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.ENG_STOP = new System.Windows.Forms.Button();
            this.ENG_START = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.FRM_RuleList = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.FRM_PVList = new System.Windows.Forms.ListView();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ENG_PVImageList = new System.Windows.Forms.ImageList(this.components);
            this.MainTimer = new System.Windows.Forms.Timer(this.components);
            this.FRM_LogList = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ENG_StateMachineGroup = new System.Windows.Forms.GroupBox();
            this.ENG_StateMachine = new System.Windows.Forms.Label();
            this.FRM_MainGateway = new System.Windows.Forms.Label();
            this.FRM_ProxyPort = new System.Windows.Forms.Label();
            this.FRM_ProxyURL = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.ENG_StartStopGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ENG_StateMachineGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label36.ForeColor = System.Drawing.Color.Silver;
            this.label36.Location = new System.Drawing.Point(12, 128);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(240, 16);
            this.label36.TabIndex = 46;
            this.label36.Text = "source @ spallationsource BitBucket";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.Color.Silver;
            this.label5.Location = new System.Drawing.Point(12, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(213, 16);
            this.label5.TabIndex = 45;
            this.label5.Text = "Created 2018 by Miklos BOROS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::EasySMSEngine.Properties.Resources.ess;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(192, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 44;
            this.pictureBox1.TabStop = false;
            // 
            // ENG_StartStopGroup
            // 
            this.ENG_StartStopGroup.Controls.Add(this.ENG_Progress);
            this.ENG_StartStopGroup.Controls.Add(this.ENG_EngineState);
            this.ENG_StartStopGroup.Controls.Add(this.label33);
            this.ENG_StartStopGroup.Controls.Add(this.ENG_STOP);
            this.ENG_StartStopGroup.Controls.Add(this.ENG_START);
            this.ENG_StartStopGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ENG_StartStopGroup.Location = new System.Drawing.Point(271, 3);
            this.ENG_StartStopGroup.Name = "ENG_StartStopGroup";
            this.ENG_StartStopGroup.Size = new System.Drawing.Size(346, 125);
            this.ENG_StartStopGroup.TabIndex = 47;
            this.ENG_StartStopGroup.TabStop = false;
            this.ENG_StartStopGroup.Text = "Start/Stop service";
            // 
            // ENG_Progress
            // 
            this.ENG_Progress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ENG_Progress.Location = new System.Drawing.Point(19, 25);
            this.ENG_Progress.MarqueeAnimationSpeed = 1000;
            this.ENG_Progress.Name = "ENG_Progress";
            this.ENG_Progress.Size = new System.Drawing.Size(306, 14);
            this.ENG_Progress.TabIndex = 39;
            // 
            // ENG_EngineState
            // 
            this.ENG_EngineState.AutoSize = true;
            this.ENG_EngineState.BackColor = System.Drawing.Color.Transparent;
            this.ENG_EngineState.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ENG_EngineState.ForeColor = System.Drawing.Color.SeaGreen;
            this.ENG_EngineState.Location = new System.Drawing.Point(117, 96);
            this.ENG_EngineState.Name = "ENG_EngineState";
            this.ENG_EngineState.Size = new System.Drawing.Size(31, 16);
            this.ENG_EngineState.TabIndex = 40;
            this.ENG_EngineState.Text = "N/A";
            this.ENG_EngineState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(16, 96);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(86, 16);
            this.label33.TabIndex = 39;
            this.label33.Text = "Actual state:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ENG_STOP
            // 
            this.ENG_STOP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ENG_STOP.Location = new System.Drawing.Point(174, 42);
            this.ENG_STOP.Name = "ENG_STOP";
            this.ENG_STOP.Size = new System.Drawing.Size(151, 48);
            this.ENG_STOP.TabIndex = 1;
            this.ENG_STOP.Text = "STOP";
            this.toolTip1.SetToolTip(this.ENG_STOP, "Stop SMS engine");
            this.ENG_STOP.UseVisualStyleBackColor = false;
            this.ENG_STOP.Click += new System.EventHandler(this.ENG_STOP_Click);
            // 
            // ENG_START
            // 
            this.ENG_START.BackColor = System.Drawing.Color.Lime;
            this.ENG_START.Location = new System.Drawing.Point(17, 42);
            this.ENG_START.Name = "ENG_START";
            this.ENG_START.Size = new System.Drawing.Size(151, 48);
            this.ENG_START.TabIndex = 0;
            this.ENG_START.Text = "START";
            this.toolTip1.SetToolTip(this.ENG_START, "Start SMS engine");
            this.ENG_START.UseVisualStyleBackColor = false;
            this.ENG_START.Click += new System.EventHandler(this.ENG_START_Click);
            // 
            // FRM_RuleList
            // 
            this.FRM_RuleList.AllowColumnReorder = true;
            this.FRM_RuleList.AutoArrange = false;
            this.FRM_RuleList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader8,
            this.columnHeader9});
            this.FRM_RuleList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FRM_RuleList.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_RuleList.ForeColor = System.Drawing.Color.Navy;
            this.FRM_RuleList.FullRowSelect = true;
            this.FRM_RuleList.Location = new System.Drawing.Point(0, 0);
            this.FRM_RuleList.MultiSelect = false;
            this.FRM_RuleList.Name = "FRM_RuleList";
            this.FRM_RuleList.Size = new System.Drawing.Size(1016, 314);
            this.FRM_RuleList.TabIndex = 50;
            this.toolTip1.SetToolTip(this.FRM_RuleList, "Right click to Edit Rule");
            this.FRM_RuleList.UseCompatibleStateImageBehavior = false;
            this.FRM_RuleList.View = System.Windows.Forms.View.Details;
            this.FRM_RuleList.SelectedIndexChanged += new System.EventHandler(this.FRM_RuleList_SelectedIndexChanged);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "#";
            this.columnHeader7.Width = 47;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Rule name";
            this.columnHeader12.Width = 246;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Syntax";
            this.columnHeader13.Width = 69;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "PVList";
            this.columnHeader4.Width = 74;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Last trigger";
            this.columnHeader5.Width = 114;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "STATUS";
            this.columnHeader6.Width = 165;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "EvaluationVal";
            this.columnHeader8.Width = 106;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "TriggerState";
            this.columnHeader9.Width = 160;
            // 
            // FRM_PVList
            // 
            this.FRM_PVList.AllowColumnReorder = true;
            this.FRM_PVList.AutoArrange = false;
            this.FRM_PVList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader14,
            this.columnHeader1});
            this.FRM_PVList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FRM_PVList.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_PVList.FullRowSelect = true;
            this.FRM_PVList.Location = new System.Drawing.Point(0, 0);
            this.FRM_PVList.MultiSelect = false;
            this.FRM_PVList.Name = "FRM_PVList";
            this.FRM_PVList.Size = new System.Drawing.Size(680, 314);
            this.FRM_PVList.SmallImageList = this.ENG_PVImageList;
            this.FRM_PVList.TabIndex = 38;
            this.FRM_PVList.UseCompatibleStateImageBehavior = false;
            this.FRM_PVList.View = System.Windows.Forms.View.Details;
            this.FRM_PVList.SelectedIndexChanged += new System.EventHandler(this.FRM_PVList_SelectedIndexChanged);
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "#";
            this.columnHeader10.Width = 47;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "PV Name";
            this.columnHeader11.Width = 306;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Value";
            this.columnHeader14.Width = 103;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "State";
            this.columnHeader1.Width = 120;
            // 
            // ENG_PVImageList
            // 
            this.ENG_PVImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ENG_PVImageList.ImageStream")));
            this.ENG_PVImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.ENG_PVImageList.Images.SetKeyName(0, "arrow_rigth.jpg");
            this.ENG_PVImageList.Images.SetKeyName(1, "ok.jpg");
            this.ENG_PVImageList.Images.SetKeyName(2, "remove.png");
            // 
            // MainTimer
            // 
            this.MainTimer.Interval = 2000;
            this.MainTimer.Tick += new System.EventHandler(this.MainTimer_Tick);
            // 
            // FRM_LogList
            // 
            this.FRM_LogList.AutoArrange = false;
            this.FRM_LogList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader3});
            this.FRM_LogList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.FRM_LogList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_LogList.FullRowSelect = true;
            this.FRM_LogList.Location = new System.Drawing.Point(0, 467);
            this.FRM_LogList.MultiSelect = false;
            this.FRM_LogList.Name = "FRM_LogList";
            this.FRM_LogList.Size = new System.Drawing.Size(1577, 116);
            this.FRM_LogList.TabIndex = 49;
            this.FRM_LogList.UseCompatibleStateImageBehavior = false;
            this.FRM_LogList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Time";
            this.columnHeader2.Width = 135;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Message";
            this.columnHeader3.Width = 760;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 147);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.splitContainer1.Panel1.Controls.Add(this.FRM_RuleList);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.splitContainer1.Panel2.Controls.Add(this.FRM_PVList);
            this.splitContainer1.Size = new System.Drawing.Size(1700, 314);
            this.splitContainer1.SplitterDistance = 1016;
            this.splitContainer1.TabIndex = 51;
            // 
            // ENG_StateMachineGroup
            // 
            this.ENG_StateMachineGroup.Controls.Add(this.ENG_StateMachine);
            this.ENG_StateMachineGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ENG_StateMachineGroup.Location = new System.Drawing.Point(638, 9);
            this.ENG_StateMachineGroup.Name = "ENG_StateMachineGroup";
            this.ENG_StateMachineGroup.Size = new System.Drawing.Size(364, 118);
            this.ENG_StateMachineGroup.TabIndex = 52;
            this.ENG_StateMachineGroup.TabStop = false;
            this.ENG_StateMachineGroup.Text = "StateMachine";
            // 
            // ENG_StateMachine
            // 
            this.ENG_StateMachine.BackColor = System.Drawing.Color.Transparent;
            this.ENG_StateMachine.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ENG_StateMachine.ForeColor = System.Drawing.Color.SeaGreen;
            this.ENG_StateMachine.Location = new System.Drawing.Point(6, 36);
            this.ENG_StateMachine.Name = "ENG_StateMachine";
            this.ENG_StateMachine.Size = new System.Drawing.Size(352, 62);
            this.ENG_StateMachine.TabIndex = 41;
            this.ENG_StateMachine.Text = "N/A";
            this.ENG_StateMachine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FRM_MainGateway
            // 
            this.FRM_MainGateway.AutoSize = true;
            this.FRM_MainGateway.BackColor = System.Drawing.Color.Transparent;
            this.FRM_MainGateway.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_MainGateway.ForeColor = System.Drawing.Color.Silver;
            this.FRM_MainGateway.Location = new System.Drawing.Point(1008, 17);
            this.FRM_MainGateway.Name = "FRM_MainGateway";
            this.FRM_MainGateway.Size = new System.Drawing.Size(94, 16);
            this.FRM_MainGateway.TabIndex = 53;
            this.FRM_MainGateway.Text = "MainGateway";
            this.FRM_MainGateway.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_ProxyPort
            // 
            this.FRM_ProxyPort.AutoSize = true;
            this.FRM_ProxyPort.BackColor = System.Drawing.Color.Transparent;
            this.FRM_ProxyPort.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_ProxyPort.ForeColor = System.Drawing.Color.Silver;
            this.FRM_ProxyPort.Location = new System.Drawing.Point(1008, 50);
            this.FRM_ProxyPort.Name = "FRM_ProxyPort";
            this.FRM_ProxyPort.Size = new System.Drawing.Size(69, 16);
            this.FRM_ProxyPort.TabIndex = 55;
            this.FRM_ProxyPort.Text = "ProxyPort";
            this.FRM_ProxyPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FRM_ProxyURL
            // 
            this.FRM_ProxyURL.AutoSize = true;
            this.FRM_ProxyURL.BackColor = System.Drawing.Color.Transparent;
            this.FRM_ProxyURL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FRM_ProxyURL.ForeColor = System.Drawing.Color.Silver;
            this.FRM_ProxyURL.Location = new System.Drawing.Point(1008, 33);
            this.FRM_ProxyURL.Name = "FRM_ProxyURL";
            this.FRM_ProxyURL.Size = new System.Drawing.Size(71, 16);
            this.FRM_ProxyURL.TabIndex = 54;
            this.FRM_ProxyURL.Text = "ProxyURL";
            this.FRM_ProxyURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ENG_MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1577, 583);
            this.Controls.Add(this.FRM_ProxyPort);
            this.Controls.Add(this.FRM_ProxyURL);
            this.Controls.Add(this.FRM_MainGateway);
            this.Controls.Add(this.ENG_StateMachineGroup);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.FRM_LogList);
            this.Controls.Add(this.ENG_StartStopGroup);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ENG_MainForm";
            this.Text = "SMS Engine for EasyICSTools";
            this.Load += new System.EventHandler(this.ENG_MainForm_Load);
            this.Shown += new System.EventHandler(this.ENG_MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ENG_StartStopGroup.ResumeLayout(false);
            this.ENG_StartStopGroup.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ENG_StateMachineGroup.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox ENG_StartStopGroup;
        private System.Windows.Forms.Button ENG_STOP;
        private System.Windows.Forms.Button ENG_START;
        private System.Windows.Forms.Label ENG_EngineState;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.ListView FRM_PVList;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ImageList ENG_PVImageList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Timer MainTimer;
        public System.Windows.Forms.ListView FRM_LogList;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ProgressBar ENG_Progress;
        public System.Windows.Forms.ListView FRM_RuleList;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.GroupBox ENG_StateMachineGroup;
        private System.Windows.Forms.Label ENG_StateMachine;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.Label FRM_MainGateway;
        private System.Windows.Forms.Label FRM_ProxyPort;
        private System.Windows.Forms.Label FRM_ProxyURL;
    }
}


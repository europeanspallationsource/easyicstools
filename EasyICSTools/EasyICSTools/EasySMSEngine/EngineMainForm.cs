﻿using EpicsSharp.ChannelAccess.Client;
using Flee.PublicTypes;
using Ini;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasySMSEngine
{
    public partial class ENG_MainForm : Form
    {
        public ENG_MainForm()
        {
            InitializeComponent();
            if (File.Exists(Application.StartupPath + "\\LocalSettings.ini"))
            {
                inifile = new IniFile(Application.StartupPath + "\\LocalSettings.ini");
            }
            else
            {
                var myFile = File.Create(Application.StartupPath + "\\LocalSettings.ini");
                myFile.Close();
                inifile = new IniFile(Application.StartupPath + "\\LocalSettings.ini");
                inifile.IniWriteValue("LocalData", "LastOnlineUpdate", "never");
                inifile.IniWriteValue("LocalData", "MainGateway", "10.0.16.85");
                inifile.IniWriteValue("LocalData", "ProxyIP", "10.0.16.5");
                inifile.IniWriteValue("LocalData", "ProxyPort", "8888");
            }

        }

        public class PV
        {
            public string name { get; set; }
            public string IP { get; set; }
            public string type { get; set; }
            public string value { get; set; }
            public string lastread { get; set; }
            public bool connected { get; set; }
            public PV()
            {
                name = string.Empty;
                IP = string.Empty;
                type = string.Empty;
                value = string.Empty;
                lastread = string.Empty;
                connected = false;
            }
        }

        public class PhoneNumber
        {
            public string NumberToSend { get; set; }
            public PhoneNumber()
            {
                NumberToSend = string.Empty;
            }
        }

        public class Rule
        {
            public string rulename { get; set; }
            public string phonesetname { get; set; }
            public string syntax { get; set; }
            public string statustext { get; set; }
            public string rulebody { get; set; }
            public string smstext { get; set; }
            public string lasttrigger { get; set; }
            public string pvlist { get; set; }
            public string triggerstate { get; set; }
            public string InitValue { get; set; }
            public string LastValue { get; set; }
            public int TriggerCount { get; set; }
            public int DiscTriggerCount { get; set; }
            public string ControlState { get; set; }
            public string NeedDisconSMS { get; set; }
            public Rule()
            {
                rulename = string.Empty;
                phonesetname = string.Empty;
                syntax = string.Empty;
                statustext = string.Empty;
                rulebody = string.Empty;
                smstext = string.Empty;
                lasttrigger = string.Empty;
                pvlist = string.Empty;
                triggerstate = string.Empty;
                InitValue = string.Empty;
                LastValue = string.Empty;
                TriggerCount = 0;
                DiscTriggerCount = 0;
                ControlState = string.Empty;
                NeedDisconSMS = string.Empty;
            }
        }

        public class RuleBits
        {
            public int BitNumber { get; set; }
            public bool Created { get; set; }
            public bool Used { get; set; }
            public string Expression { get; set; }
            public bool Value { get; set; }
            public RuleBits()
            {
                BitNumber = 0;
                Created = false;
                Used = false;
                Expression = string.Empty;
                Value = false;
            }
        }

        List<RuleBits> RuleBIT = new List<RuleBits>();

        RuleBits RuleMain = new RuleBits();

        List<Rule> Rules = new List<Rule>();

        List<PV> PVs = new List<PV>();

        List<PhoneNumber> PhoneNumbers = new List<PhoneNumber>();

        public IniFile inifile;

        bool ControlStateZero = false;



        private void LoadPVDataBase()
        {
            SqlConnection conn = null;
            PVs = new List<PV>();
            try
            {

                string sql = "select pvname,ipaddress,vartype from pvlist ;";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        PV PVItem = new PV();
                        PVItem.connected = false;
                        PVItem.name = SqlData.GetString(0);
                        PVItem.IP = SqlData.GetString(1);
                        PVItem.type = SqlData.GetString(2);
                        PVItem.value = "";
                        PVItem.lastread = "";
                        PVs.Add(PVItem);

                    }
                }
            }
            catch (Exception ex)
            {
                Log("Refresh PV list error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }

        private void LoadPhoneBook(string PhonesetName)
        {
            SqlConnection conn = null;
            PhoneNumbers = new List<PhoneNumber>();

            try
            {

                string sql = "select phonenumber from phonebook where phonesetname = '"+PhonesetName+"';";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        PhoneNumber phonenumber = new PhoneNumber(); 
                        phonenumber.NumberToSend = SqlData.GetString(0);
                        PhoneNumbers.Add(phonenumber);
                    }
                }
            }
            catch (Exception ex)
            {
                Log("Load phonenumbers error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }


        private void LoadRuleDataBase()
        {
            SqlConnection conn = null;
            Rules = new List<Rule>();
            try
            {

                string sql = "select rulename,phonesetname,syntax,statustext,rulebody,smstext,lasttrigger,pvlist,triggerstate,initvalue,lastvalue,triggercount,disctriggercount,controlstate,needdisconsms from monitor ;";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        Rule RuleItem = new Rule();
                        RuleItem.rulename = SqlData.GetString(0);
                        RuleItem.phonesetname = SqlData.GetString(1);
                        RuleItem.syntax = SqlData.GetString(2);
                        RuleItem.statustext = SqlData.GetString(3);
                        RuleItem.rulebody = SqlData.GetString(4);
                        RuleItem.smstext = SqlData.GetString(5);
                        RuleItem.lasttrigger = SqlData.GetString(6);
                        RuleItem.pvlist = SqlData.GetString(7);
                        RuleItem.triggerstate = SqlData.GetString(8);
                        RuleItem.InitValue = SqlData.GetString(9);
                        RuleItem.LastValue = SqlData.GetString(10);
                        RuleItem.TriggerCount = SqlData.GetInt32(11);
                        RuleItem.DiscTriggerCount = SqlData.GetInt32(12);
                        RuleItem.ControlState = SqlData.GetString(13);
                        RuleItem.NeedDisconSMS = SqlData.GetString(14);
                        Rules.Add(RuleItem);
                        if (RuleItem.ControlState.Length<2)
                        {
                            ControlStateZero = true;
                        } else
                        {
                            ControlStateZero = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Log("Load rule database error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }

        private bool SendSMS(string PhoneNumber, string MessageTitle, string MessageBody)
        {
            bool retval = false;
            if (PhoneNumber.Length > 6)
            {
                if (PhoneNumber.StartsWith("+"))
                {
                    if ((MessageTitle != string.Empty) && (MessageBody != string.Empty))
                    {
                        if (MessageTitle.Length <= 11)
                        {
                            try
                            {
                                byte[] AuthBytes = Encoding.ASCII.GetBytes("passwordreset:45HBBp9M");
                                string sAuth = Convert.ToBase64String(AuthBytes);
                                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://sms-pro.net:44343/services/ERICESS/sendsms");
                                request.Headers.Add("Authorization", "Basic " + sAuth);
                                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None;
                                request.Proxy = new WebProxy(FRM_ProxyURL.Text, Convert.ToInt32(FRM_ProxyPort.Text));
                                request.PreAuthenticate = true;

                                request.Method = "POST";
                                request.ContentType = "text/xml";

                                byte[] bytes;
                                bytes = System.Text.Encoding.ASCII.GetBytes(@"<?xml version=""1.0"" encoding=""ISO-8859-1""?><mobilectrl_sms><header><customer_id>ERICESS</customer_id><password>yQvR3s68</password><from_alphanumeric>" + MessageTitle + @"</from_alphanumeric></header><payload><sms account = ""71700""><message><![CDATA[" + MessageBody + @"]]></message><to_msisdn>" + PhoneNumber + @"</to_msisdn></sms></payload></mobilectrl_sms>");

                                Stream requestStream = request.GetRequestStream();
                                requestStream.Write(bytes, 0, bytes.Length);
                                requestStream.Close();
                                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                                string responseStr = string.Empty;
                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    Stream responseStream = response.GetResponseStream();
                                    responseStr = new StreamReader(responseStream).ReadToEnd();
                                }
                                if (responseStr.Contains("SMS request is being processed"))
                                {
                                    retval = true;
                                    Log("SMS sent on number " + PhoneNumber, Color.DarkGreen);
                                }
                                else
                                {
                                    Log("SMS sending error on " + PhoneNumber + " response from Telenor: " + responseStr, Color.DarkGreen);
                                    MessageBox.Show(responseStr);
                                }
                            }
                            catch (Exception ex)
                            {

                                Log("SMS send error:" + ex.Message, Color.Red);
                            }
                        }
                        else
                        {
                            Log("SMS title too long:" + MessageTitle + " (max 11 char)!", Color.Red);
                        }

                    }
                    else
                    {
                        Log("SMS title and body has to non empty strings!", Color.Red);
                    }
                }
                else
                {
                    Log("SMS phone number has to start with +", Color.Red);
                }
            }
            else
            {
                Log("SMS phone number too short!", Color.Red);
            }

            return retval;
        }

        private void ReloadPVList()
        {
            ListViewItem.ListViewSubItem _SubItem;
            int i = 0;
            FRM_PVList.Items.Clear();
            try
            {
                for (int pv = 0; pv < PVs.Count; pv++)
                {

                    i++;
                    ListViewItem _Item = new ListViewItem(i.ToString());
                    _Item.ForeColor = Color.Navy;
                    _Item.UseItemStyleForSubItems = false;

                    _SubItem = new ListViewItem.ListViewSubItem(_Item, PVs[pv].name);
                    _SubItem.ForeColor = Color.Navy;
                    _Item.SubItems.Add(_SubItem);

                    _SubItem = new ListViewItem.ListViewSubItem(_Item, PVs[pv].value);
                    _SubItem.ForeColor = Color.Navy;
                    _Item.SubItems.Add(_SubItem);

                    if (PVs[pv].connected)
                    {
                        _SubItem = new ListViewItem.ListViewSubItem(_Item, "OK");
                        _SubItem.ForeColor = Color.DarkGreen;
                        _Item.SubItems.Add(_SubItem);
                    } else
                    {
                        _SubItem = new ListViewItem.ListViewSubItem(_Item, "DISCON");
                        _SubItem.ForeColor = Color.Red;
                        _Item.SubItems.Add(_SubItem);

                    }
                    FRM_PVList.Items.Add(_Item);
                }
            }
            catch (Exception ex)
            {
                Log("Reload PV list error: " + ex.Message, Color.Red);
                // Log your error
            }
        }

        private void ReloadRuleList()
        {
            ListViewItem.ListViewSubItem _SubItem;
            int i = 0;
            FRM_RuleList.Items.Clear();
            try
            {
                for (int rule = 0; rule < Rules.Count; rule++)
                {

                    i++;
                    ListViewItem _Item = new ListViewItem(i.ToString());
                    _Item.ForeColor = Color.Navy;
                    _Item.UseItemStyleForSubItems = false;

                    _SubItem = new ListViewItem.ListViewSubItem(_Item, Rules[rule].rulename);
                    _SubItem.ForeColor = Color.Navy;
                    _Item.SubItems.Add(_SubItem);

                    _SubItem = new ListViewItem.ListViewSubItem(_Item, Rules[rule].syntax);
                    _SubItem.ForeColor = Color.Navy;
                    _Item.SubItems.Add(_SubItem);

                    _SubItem = new ListViewItem.ListViewSubItem(_Item, Rules[rule].pvlist);
                    _SubItem.ForeColor = Color.Navy;
                    _Item.SubItems.Add(_SubItem);

                    _SubItem = new ListViewItem.ListViewSubItem(_Item, Rules[rule].lasttrigger);
                    _SubItem.ForeColor = Color.Navy;
                    _Item.SubItems.Add(_SubItem);

                    _SubItem = new ListViewItem.ListViewSubItem(_Item, Rules[rule].statustext);
                    _SubItem.ForeColor = Color.Navy;
                    _Item.SubItems.Add(_SubItem);

                    _SubItem = new ListViewItem.ListViewSubItem(_Item, Rules[rule].LastValue);
                    _SubItem.ForeColor = Color.Navy;
                    _Item.SubItems.Add(_SubItem);

                    _SubItem = new ListViewItem.ListViewSubItem(_Item, Rules[rule].triggerstate);
                    _SubItem.ForeColor = Color.Navy;
                    _Item.SubItems.Add(_SubItem);

                    FRM_RuleList.Items.Add(_Item);
                }
            }
            catch (Exception ex)
            {
                Log("Reload PV list error: " + ex.Message, Color.Red);
                // Log your error
            }
        }

        private void FullRestart()
        {
            ENG_StateMachine.Text = "INITIALIZING";
            ENG_EngineState.Text = "RUNNING";
            LoadPVDataBase();
            LoadRuleDataBase();
            ENG_Progress.Value = 100;
        }

        private void ENG_START_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                string sql = string.Empty;
                SqlCommand cmd = null;
                conn.Open();


                sql = "update monitor set triggerstate= 'RESTART' , triggercount = 0, disctriggercount = 0 ;";
                cmd = new SqlCommand(sql, conn);
                int count = (int)cmd.ExecuteNonQuery();
                if (count <= 0)
                {
                    Log("Database error: update initial state!", Color.Green);
                }



            }
            catch (Exception ex)
            {
                Log("Load rule database error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }

            FullRestart();
            ReloadPVList();
            ReloadRuleList();
            MainTimer.Enabled = true;
        }

        private void ENG_STOP_Click(object sender, EventArgs e)
        {
            MainTimer.Enabled = false;
            ENG_Progress.Value = 0;
            ENG_StateMachine.Text = "STOP";
            ENG_EngineState.Text = "STOPPED";
        }

        public void Log(string message, Color messagecolor)
        {
            ListViewItem _Item = new ListViewItem(DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture));
            _Item.ForeColor = Color.Black;
            _Item.UseItemStyleForSubItems = false;

            ListViewItem.ListViewSubItem _SubItem = new ListViewItem.ListViewSubItem(_Item, message);
            _SubItem.ForeColor = messagecolor;
            _Item.SubItems.Add(_SubItem);


            FRM_LogList.Items.Add(_Item);
            FRM_LogList.Items[FRM_LogList.Items.Count - 1].EnsureVisible();

            Application.DoEvents();
        }

        public string GetPV(string PVName, string Field)
        {
            string retval = "N/A";
            using (CAClient client = new CAClient())
            {

                try
                {
                    Channel<string> channel2 = client.CreateChannel<string>(PVName + "." + Field);
                    client.Configuration.WaitTimeout = 1500;
                    retval = channel2.Get<string>();
                    channel2.Dispose();
                }
                catch (Exception e)
                {

                }
            }
            return retval;

        }

        public bool GetPVReal(string PVName, string Field,out float RealVal )
        {
            bool retval = true;
            using (CAClient client = new CAClient())
            {
                RealVal = 0.0F;
                try
                {
                    Channel<float> channel2 = client.CreateChannel<float>(PVName + "." + Field);
                    client.Configuration.WaitTimeout = 1500;
                    client.Configuration.SearchAddress = FRM_MainGateway.Text; 
                    RealVal = channel2.Get<float>();
                    channel2.Dispose();
                }
                catch (Exception e)
                {
                    retval = false;
                }
            }
            return retval;

        }

        public List<int> AllIndexesOf(string str, string value)
        {
            List<int> indexes = new List<int>();
            if (String.IsNullOrEmpty(value))
            {
                return indexes;
            }
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    return indexes;
                indexes.Add(index);
            }
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            MainTimer.Enabled = false;

            FullRestart();

            ENG_Progress.Value = 0;

            if (PVs.Count > 0)
            {
                for (int i = 0; i < PVs.Count; i++)
                {
                    ENG_StateMachine.Text = "UPDATING PV:"+i.ToString(); ENG_StateMachine.Refresh();
                    ENG_StateMachine.Refresh();
                    Application.DoEvents();
                    //Gateway = PVs[i].IP;
                    float PVVal = 0.0F;
                    
                    if (!GetPVReal(PVs[i].name, "",out PVVal))
                    {
                        PVs[i].value = "0.0";
                        PVs[i].connected = false;
                    }
                    else
                    {
                        //PVs[i].type = GetPV(PVs[i].name, "INP");
                        //if (PVs[i].type != "N/A")
                        //{
                        //    PVs[i].connected = true;
                        //    if (PVs[i].type.Contains("T="))
                        //    {
                        //        PVs[i].type = PVs[i].type.Remove(0, PVs[i].type.IndexOf("T=") + 2);
                        //    }
                        //}
                        //else
                        //{
                        //    PVs[i].type = "Parameter";
                        //}
                        PVs[i].connected = true;
                        PVs[i].value = PVVal.ToString("0.0000", CultureInfo.InvariantCulture);
                        PVs[i].lastread = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss");
                    }
                }
            }
            ENG_StateMachine.Text = "EXECUTING RULES"; ENG_StateMachine.Refresh();

            for (int rule = 0; rule < Rules.Count; rule++)
            {
                if (Rules[rule].syntax == "OK")
                {

                    //Check PV names in body
                    int foundpvs = 0;
                    bool allconnected = true;
                    List<int> indexes = AllIndexesOf(Rules[rule].rulebody, "@PV:");
                    if (indexes.Count > 0)
                    {
                        for (int i = 0; i < indexes.Count; i++)
                        {
                            string PVName = Rules[rule].rulebody.Substring(indexes[i], Rules[rule].rulebody.IndexOf(" ", indexes[i] + 1) - indexes[i]);

                            for (int pv = 0; pv < PVs.Count; pv++)
                            {
                                if (PVName == "@PV:" + PVs[pv].name)
                                {
                                    foundpvs++;
                                    if (PVs[pv].connected == false)
                                    {
                                        allconnected = false;
                                    }
                                }
                            }
                        }
                        if (indexes.Count == foundpvs)
                        {
                            if (allconnected)
                            {
                                Rules[rule].pvlist = "OK";
                                switch (Rules[rule].ControlState)
                                {
                                    case "ENABLING":
                                        Rules[rule].statustext = "ENABLING";
                                        Rules[rule].ControlState = "";
                                        break;
                                    case "DISABLING":
                                        Rules[rule].statustext = "DISABLING";
                                        Rules[rule].ControlState = "";
                                        break;
                                }
                                switch (Rules[rule].statustext)
                                {
                                    case "ENABLING":
                                        Rules[rule].triggerstate = "INIT";
                                        Rules[rule].statustext = "INIT";
                                        break;
                                    case "DISABLING":
                                        Rules[rule].triggerstate = "DISABLED";
                                        Rules[rule].statustext = "DISABLED";
                                        break;
                                    case "INIT":
                                        if (EvaluateRule(rule))
                                        {
                                            Rules[rule].InitValue = Rules[rule].LastValue;
                                            Rules[rule].statustext = "RUN";
                                        }
                                        break;
                                    case "RUN":
                                        EvaluateRule(rule);
                                        CheckTriggerEvent(rule);
                                        break;
                                    case "DISABLED":
                                        break;
                                    default:
                                        Rules[rule].statustext = "INIT";
                                        break;
                                }
                            }
                            else
                            {
                                Rules[rule].triggerstate = "DISCONNECTED";
                                Rules[rule].pvlist = "DISCON";
                                CheckTriggerEvent(rule);
                            }
                        }
                        else
                        {
                            Rules[rule].pvlist = "DISCON";
                        }
                    }
                    else
                    {
                        Rules[rule].triggerstate = "NOPV_ERROR";
                    }
                }
                else
                {
                    Rules[rule].triggerstate = "SYNTAX_ERROR";
                }
            }



            //Start new cycle
            ENG_Progress.Value = 100;
            ENG_StateMachine.Text = "IDLE"; ENG_StateMachine.Refresh();

            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                string sql = string.Empty;
                SqlCommand cmd = null;
                conn.Open();


                for (int rule = 0; rule < Rules.Count; rule++)
                {
                    if (ControlStateZero)
                    {
                        sql = "update monitor set statustext ='" + Rules[rule].statustext + "', lasttrigger= '" + Rules[rule].lasttrigger + "', pvlist= '" + Rules[rule].pvlist + "', triggerstate= '" + Rules[rule].triggerstate + "',initvalue = '" + Rules[rule].InitValue + "', lastvalue = '" + Rules[rule].LastValue + "',triggercount = " + Rules[rule].TriggerCount + ",disctriggercount = " + Rules[rule].DiscTriggerCount + "    where rulename = '" + Rules[rule].rulename + "' ;";
                    } else
                    {
                        sql = "update monitor set statustext ='" + Rules[rule].statustext + "', lasttrigger= '" + Rules[rule].lasttrigger + "', pvlist= '" + Rules[rule].pvlist + "', triggerstate= '" + Rules[rule].triggerstate + "',initvalue = '" + Rules[rule].InitValue + "', lastvalue = '" + Rules[rule].LastValue + "',triggercount = " + Rules[rule].TriggerCount + ",disctriggercount = " + Rules[rule].DiscTriggerCount + ",controlstate = '" + Rules[rule].ControlState + "'    where rulename = '" + Rules[rule].rulename + "' ;";

                    }
                    cmd = new SqlCommand(sql, conn);
                    int count = (int)cmd.ExecuteNonQuery();
                    if (count <= 0)
                    {
                        Log("Database error during the update of Rule:" + rule.ToString() + ".", Color.Green);
                    }

                }

                for (int pv = 0; pv < PVs.Count; pv++)
                {
                    sql = "update pvlist set lastvalue ='" + PVs[pv].value + "', lastread='" + PVs[pv].lastread + "' where pvname = '" + PVs[pv].name + "' ;";
                    cmd = new SqlCommand(sql, conn);
                    int count = (int)cmd.ExecuteNonQuery();
                    if (count <= 0)
                    {
                        Log("Database error during the update of PV values:" + pv.ToString() + ".", Color.Green);
                    }

                }

            }
            catch (Exception ex)
            {
                Log("Update Rule list error: " + ex.Message, Color.Red);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
            ReloadPVList();
            ReloadRuleList();

            MainTimer.Enabled = true;

        }

        public string Between(string STR, string FirstString, string LastString)
        {
            string FinalString;
            int Pos1 = STR.IndexOf(FirstString) + FirstString.Length;
            int Pos2 = STR.IndexOf(LastString);
            FinalString = STR.Substring(Pos1, Pos2 - Pos1);
            return FinalString;
        }


        private bool EvaluateRule(int rule)
        {
            bool retval = true;

            RuleBIT = new List<RuleBits>();
            RuleMain = new RuleBits();
            for (int i = 0; i < 100; i++)
            {
                RuleBits Item = new RuleBits();
                RuleBIT.Add(Item);
            }


            bool retvalOK = false;

            string[] shtblines = Rules[rule].rulebody.Split('\n');
            string[] shtblineschk = Rules[rule].rulebody.Replace("==", "OP:EQ").Replace("<=", "OP:LE").Replace(">=", "OP:GE").Split('\n');
            int changelength = 0;
            bool PVFound = false;
            for (int lin = 0; lin < shtblines.Length; lin++)
            {
                if (!shtblines[lin].StartsWith("//"))
                {
                    for (int i = 1; i < 100; i++)
                    {
                        //Replace PV name
                        if ((shtblines[lin].IndexOf(@"@PV" + i.ToString() + " ") > -1) || (shtblines[lin].IndexOf(@"@PV" + i.ToString() + "\n") > -1))
                        {
                            if (FRM_PVList.Items.Count >= i)
                            {
                                shtblines[lin] = shtblines[lin].Replace("@PV" + i.ToString(), "@PV:" + FRM_PVList.Items[i - 1].SubItems[1].Text + " ");
                                changelength = FRM_PVList.Items[i - 1].SubItems[1].Text.Length;
                                PVFound = true;
                            }
                            else
                            {
                                retval = false;
                                Log("Line " + (lin + 1).ToString() + ": PV" + i.ToString() + " is not in the PV list!\n", Color.Red);
                            }
                        }
                    }
                    if ((shtblineschk[lin] != "\n") && (shtblineschk[lin] != string.Empty))
                    {
                        // Check operators
                        if ((shtblineschk[lin].Contains("(")) || (shtblineschk[lin].Contains(")")))
                        {
                            retval = false;
                            Log("Line " + (lin + 1).ToString() + ": not permitted characters '(' ')'!\n", Color.Red);
                        }
                        if ((shtblineschk[lin].Contains("{")) || (shtblineschk[lin].Contains("}")))
                        {
                            retval = false;
                            Log("Line " + (lin + 1).ToString() + ": not permitted characters '{' '}'!\n", Color.Red);
                        }
                        if ((shtblineschk[lin].Contains("[")) || (shtblineschk[lin].Contains("]")))
                        {
                            retval = false;
                            Log("Line " + (lin + 1).ToString() + ": not permitted characters '[' ']'!\n", Color.Red);
                        }
                        if ((shtblineschk[lin].Contains("&")) || (shtblineschk[lin].Contains("|")))
                        {
                            retval = false;
                            Log("Line " + (lin + 1).ToString() + ": not permitted characters '|' '&' use AND, OR instead!\n", Color.Red);
                        }
                        if (shtblineschk[lin].Contains(";"))
                        {
                            retval = false;
                            Log("Line " + (lin + 1).ToString() + ": not permitted character ';'\n", Color.Red);
                        }
                        if (shtblineschk[lin].Contains("="))
                        {
                        }
                        else
                        {
                            retval = false;
                            Log("Line " + (lin + 1).ToString() + ": does not contain any '=' operator!\n", Color.Red);

                        }
                        if (shtblineschk[lin].IndexOf(@"@BIT ") > -1)
                        {
                            retval = false;
                            Log("Line " + (lin + 1).ToString() + ": BIT usage without a BIT number!\n", Color.Red);
                        }
                        for (int i = 1; i < 100; i++)
                        {
                            //Check BITs usage
                            if ((shtblineschk[lin].IndexOf(@"@BIT" + i.ToString() + " ") > -1) || (shtblineschk[lin].IndexOf(@"@BIT" + i.ToString()) > -1))
                            {

                                string[] parts = shtblineschk[lin].Split('=');
                                if (parts.Length == 1)
                                {

                                }
                                if (parts.Length > 0)
                                {
                                    if (parts.Length == 2)
                                    {
                                        if (parts[0].IndexOf(@"@BIT" + i.ToString()) > -1)
                                        {
                                            RuleBIT[i].BitNumber = i;
                                            RuleBIT[i].Created = true;
                                            RuleBIT[i].Expression = parts[1];
                                        }
                                        if (parts[1].IndexOf(@"@BIT" + i.ToString()) > -1)
                                        {
                                            RuleBIT[i].BitNumber = i;
                                            RuleBIT[i].Used = true;

                                        }
                                        if ((parts[0].IndexOf(@"@BIT" + i.ToString()) > -1) && (parts[1].IndexOf(@"@BIT" + i.ToString()) > -1))
                                        {
                                            retval = false;
                                            Log("Line " + (lin + 1).ToString() + ": @BIT" + i.ToString() + " can not use itself!\n", Color.Red);

                                        }
                                    }
                                    if (parts.Length > 2)
                                    {
                                        retval = false;
                                        Log("Line " + (lin + 1).ToString() + ": contains more '=' operator!\n", Color.Red);

                                    }

                                }
                            }
                            else
                            { }

                        } // Bit usage

                        //Check PVs usage
                        if (shtblineschk[lin].IndexOf(@"@PV ") > -1)
                        {
                            retval = false;
                            Log("Line " + (lin + 1).ToString() + ": PV usage without a PV number!\n", Color.Red);
                        }
                        if (shtblineschk[lin].IndexOf(@"@PV") > -1)
                        {
                            string[] parts = shtblineschk[lin].Split('=');
                            if (parts.Length == 1)
                            {

                            }
                            if (parts.Length > 0)
                            {
                                if (parts.Length == 2)
                                {
                                    if (parts[0].IndexOf(@"@PV") > -1)
                                    {
                                        Log("Line " + (lin + 1).ToString() + ": PVs are read only!\n", Color.Red);
                                        retval = false;

                                    }
                                }

                            }
                        }
                        //Check empty equal
                        string[] sublines = shtblineschk[lin].Split('=');
                        if (sublines.Length == 1)
                        {

                        }
                        if (sublines.Length > 0)
                        {
                            if (sublines.Length == 2)
                            {
                                if (sublines[0].Replace(" ", "").Length == 0)
                                {
                                    retval = false;
                                    Log("Line " + (lin + 1).ToString() + ": Left hand side empty!\n", Color.Red);

                                }
                                if (!sublines[0].Contains("@"))
                                {
                                    retval = false;
                                    Log("Line " + (lin + 1).ToString() + ": Left hand side has to contain @BIT, @PV, @RULE!\n", Color.Red);

                                }
                                if (sublines[1].Replace(" ", "").Length == 0)
                                {
                                    retval = false;
                                    Log("Line " + (lin + 1).ToString() + ": Rigth hand side empty!\n", Color.Red);

                                }
                                //Check not permitted Variable names
                                if (sublines[0].IndexOf(@"@") > -1)
                                {
                                    if ((sublines[0].IndexOf(@"@PV") == -1) && (sublines[0].IndexOf(@"@BIT") == -1) && (sublines[0].IndexOf(@"@RULE") == -1))
                                    {
                                        retval = false;
                                        Log("Line " + (lin + 1).ToString() + ": unpermitted @ usage on the left hand side!!\n", Color.Red);
                                    }

                                }
                                if (sublines[1].IndexOf(@"@") > -1)
                                {
                                    if ((sublines[1].IndexOf(@"@PV") == -1) && (sublines[1].IndexOf(@"@BIT") == -1) && (sublines[0].IndexOf(@"@RULE") == -1))
                                    {
                                        retval = false;
                                        Log("Line " + (lin + 1).ToString() + ": unpermitted @ usage on the right hand side!!\n", Color.Red);
                                    }

                                }
                            }
                        }


                        //Check retval
                        if (shtblineschk[lin].IndexOf(@"@RULE") > -1)
                        {
                            string[] parts = shtblineschk[lin].Split('=');
                            if (parts.Length == 1)
                            {

                            }
                            if (parts.Length > 0)
                            {
                                if (parts.Length == 2)
                                {
                                    if (parts[0].IndexOf(@"@RULE") > -1)
                                    {
                                        retvalOK = true;
                                        RuleMain.Expression = parts[1];
                                    }
                                    if (parts[1].IndexOf(@"@RULE") > -1)
                                    {
                                        retvalOK = true;
                                        retval = false;
                                        Log("Line " + (lin + 1).ToString() + ": variable @RULE has to be on left hand side!\n", Color.Red);
                                    }
                                    if ((parts[0].IndexOf(@"@RULE") > -1) && (parts[1].IndexOf(@"@RULE") > -1))
                                    {
                                        retval = false;
                                        Log("Line " + (lin + 1).ToString() + ":  @RULE can't use itself!\n", Color.Red);

                                    }

                                }

                            }
                        }


                    }// line not empty


                } // line not a comment
            }

            // Things to check after the whole rule was processed 
            for (int i = 1; i < 100; i++)
            {
                if (RuleBIT[i].BitNumber > 0)
                {
                    if ((RuleBIT[i].Created) && !(RuleBIT[i].Used))
                    {
                        retval = false;
                        Log("Warning: BIT" + i.ToString() + " value is never used!\n", Color.Red);

                    }
                    if (!(RuleBIT[i].Created) && (RuleBIT[i].Used))
                    {
                        retval = false;
                        Log("Error: BIT" + i.ToString() + " not declared!\n", Color.Red);

                    }
                }
            }
            if (retvalOK == false)
            {
                retval = false;
                Log("Variable @RULE has to be used in this script!\n", Color.Red);
            }


            if (retval == true)
            {
                for (int pv = 0; pv < PVs.Count; pv++)
                {
                    for (int i = 0; i < RuleBIT.Count; i++)
                    {
                        if (RuleBIT[i].BitNumber > 0)
                        {
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("@", "");
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("PV:" + PVs[pv].name, PVs[pv].value);
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("OP:EQ", "=");
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("OP:LE", "<=");
                            RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("OP:GE", ">=");
                        }
                    }
                    RuleMain.Expression = RuleMain.Expression.Replace("@", "");
                    RuleMain.Expression = RuleMain.Expression.Replace("PV:" + PVs[pv].name, PVs[pv].value);
                    RuleMain.Expression = RuleMain.Expression.Replace("OP:EQ", "=");
                    RuleMain.Expression = RuleMain.Expression.Replace("OP:LE", "<=");
                    RuleMain.Expression = RuleMain.Expression.Replace("OP:GE", ">=");
                }



                RuleBIT = RuleBIT;
                RuleMain = RuleMain;
                try
                {
                    ExpressionContext context = new ExpressionContext();
                    context.Options.ParseCulture = CultureInfo.InvariantCulture;
                    VariableCollection variables = context.Variables;
                    for (int i = 0; i < RuleBIT.Count; i++)
                    {
                        if (RuleBIT[i].BitNumber > 0)
                        {
                            for (int key = 0; key < 100; key++)
                            {
                                if (RuleBIT[i].Expression.IndexOf("BIT" + key.ToString()) > -1)
                                {
                                    string bitvalue = string.Empty;
                                    for (int ii = 0; ii < RuleBIT.Count; ii++)
                                    {
                                        if (RuleBIT[ii].BitNumber == key)
                                        {
                                            bitvalue = RuleBIT[ii].Value.ToString();
                                        }
                                    }
                                    RuleBIT[i].Expression = RuleBIT[i].Expression.Replace("BIT" + key.ToString(), bitvalue);
                                }
                            }
                            IGenericExpression<bool> exp = context.CompileGeneric<bool>("(" + RuleBIT[i].Expression + ")");
                            RuleBIT[i].Value = exp.Evaluate();
                        }
                    }

                    for (int i = 0; i < 100; i++)
                    {
                        string bitexp = string.Empty;
                        for (int ii = 0; ii < RuleBIT.Count; ii++)
                        {
                            if (RuleBIT[ii].BitNumber == i)
                            {
                                bitexp = RuleBIT[ii].Value.ToString();
                            }
                        }
                        RuleMain.Expression = RuleMain.Expression.Replace("BIT" + i.ToString(), bitexp);
                    }

                    IGenericExpression<bool> mainexp = context.CompileGeneric<bool>("(" + RuleMain.Expression + ")");
                    RuleMain.Value = mainexp.Evaluate();

                    Rules[rule].LastValue = RuleMain.Value.ToString();


                }
                catch (Exception ex)
                {
                    Log("Compile error: " + ex.Message + "!\n", Color.Red);
                }
            }



            return retval;
        }

        private void CheckTriggerEvent(int rule)
        {
            if (Rules[rule].pvlist == "OK")
            {
                if (Rules[rule].DiscTriggerCount == 5)
                {
                    Log(Rules[rule].rulename+ " IOC is now back online!", Color.DarkGreen);

                }
                Rules[rule].DiscTriggerCount = 0;
                if (Rules[rule].LastValue.ToUpper() == "TRUE")
                {
                    if (Rules[rule].InitValue.ToUpper() == "TRUE")
                    {
                        Rules[rule].triggerstate = "Waiting for falling edge";
                    }
                    else
                    {
                        if (Rules[rule].TriggerCount < 5)
                        {
                            Rules[rule].TriggerCount += 1;
                            Rules[rule].triggerstate = "Rule trigger counting up";
                        }
                        else
                        { 
                            Rules[rule].InitValue = Rules[rule].LastValue;
                            Rules[rule].triggerstate = "Rule !Trigger!";
                            Rules[rule].lasttrigger =  DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss");
                            Log("TRIGGER:  [" + Rules[rule].rulename + "] triggered because of condition is TRUE!", Color.DarkRed);
                            LoadPhoneBook(Rules[rule].phonesetname);
                            for (int i=0;i<PhoneNumbers.Count;i++)
                            {
                                Log("Number:  [" + PhoneNumbers[i].NumberToSend + "]  Text:'" + Rules[rule].smstext + "'", Color.DarkRed);
                                SendSMS(PhoneNumbers[i].NumberToSend, "ICSSMSTOOL", "Rule: " + Rules[rule].rulename + " Message: " + Rules[rule].smstext);
                            }
                        }
                    }

                }
                else
                {
                    Rules[rule].InitValue = Rules[rule].LastValue;
                    Rules[rule].triggerstate = "LISTENING";
                    Rules[rule].TriggerCount = 0;

                }
            } else
            { // Disconnected Trigger
                if (Rules[rule].DiscTriggerCount < 5)
                {
                    Rules[rule].triggerstate = "Disconnect counting up";
                    Rules[rule].DiscTriggerCount += 1;
                }
                if (Rules[rule].DiscTriggerCount == 5)
                {
                    Rules[rule].DiscTriggerCount += 1;
                    if (Rules[rule].NeedDisconSMS == "YES")
                    {
                        Rules[rule].triggerstate = "Disconnected !Trigger!";
                        Log("TRIGGER:  [" + Rules[rule].rulename + "] triggered because of one or more DISCONNECTED PVs!", Color.DarkRed);
                        Rules[rule].lasttrigger = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss");
                        LoadPhoneBook(Rules[rule].phonesetname);
                        for (int i = 0; i < PhoneNumbers.Count; i++)
                        {
                            Log("Number:  [" + PhoneNumbers[i].NumberToSend + "] "+"Rule: "+Rules[rule].rulename+" IOC is disconnected!", Color.DarkRed);
                            SendSMS(PhoneNumbers[i].NumberToSend, "ICSSMSTOOL", "Rule: "+Rules[rule].rulename+" IOC is down.");
                        }
                    }
                }


            }

        }

        private void ENG_MainForm_Load(object sender, EventArgs e)
        {

        }

        private void FRM_PVList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FRM_RuleList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ENG_MainForm_Shown(object sender, EventArgs e)
        {
            FRM_MainGateway.Text = inifile.IniReadValue("LocalData", "MainGateway");
            FRM_ProxyURL.Text = inifile.IniReadValue("LocalData", "ProxyIP");
            FRM_ProxyPort.Text = inifile.IniReadValue("LocalData", "ProxyPort");

        }

    }
}

namespace Ini
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
                 string key, string def, StringBuilder retVal,
            int size, string filePath);

        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(100000);
            int i = GetPrivateProfileString(Section, Key, "", temp,
                                            100000, this.path);
            return temp.ToString();

        }
    }
}



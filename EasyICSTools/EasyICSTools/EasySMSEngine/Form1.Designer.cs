﻿namespace EasySMSEngine
{
    partial class ENG_MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label36 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ENG_StartStopGroup = new System.Windows.Forms.GroupBox();
            this.ENG_START = new System.Windows.Forms.Button();
            this.ENG_STOP = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.ENG_StartStopGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label36.ForeColor = System.Drawing.Color.Silver;
            this.label36.Location = new System.Drawing.Point(12, 128);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(240, 16);
            this.label36.TabIndex = 46;
            this.label36.Text = "source @ spallationsource BitBucket";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.Color.Silver;
            this.label5.Location = new System.Drawing.Point(12, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(213, 16);
            this.label5.TabIndex = 45;
            this.label5.Text = "Created 2018 by Miklos BOROS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::EasySMSEngine.Properties.Resources.ess;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(192, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 44;
            this.pictureBox1.TabStop = false;
            // 
            // ENG_StartStopGroup
            // 
            this.ENG_StartStopGroup.Controls.Add(this.ENG_STOP);
            this.ENG_StartStopGroup.Controls.Add(this.ENG_START);
            this.ENG_StartStopGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ENG_StartStopGroup.Location = new System.Drawing.Point(271, 3);
            this.ENG_StartStopGroup.Name = "ENG_StartStopGroup";
            this.ENG_StartStopGroup.Size = new System.Drawing.Size(346, 125);
            this.ENG_StartStopGroup.TabIndex = 47;
            this.ENG_StartStopGroup.TabStop = false;
            this.ENG_StartStopGroup.Text = "Start/Stop service";
            // 
            // ENG_START
            // 
            this.ENG_START.BackColor = System.Drawing.Color.Lime;
            this.ENG_START.Location = new System.Drawing.Point(17, 42);
            this.ENG_START.Name = "ENG_START";
            this.ENG_START.Size = new System.Drawing.Size(151, 48);
            this.ENG_START.TabIndex = 0;
            this.ENG_START.Text = "START";
            this.ENG_START.UseVisualStyleBackColor = false;
            // 
            // ENG_STOP
            // 
            this.ENG_STOP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ENG_STOP.Location = new System.Drawing.Point(174, 42);
            this.ENG_STOP.Name = "ENG_STOP";
            this.ENG_STOP.Size = new System.Drawing.Size(151, 48);
            this.ENG_STOP.TabIndex = 1;
            this.ENG_STOP.Text = "STOP";
            this.ENG_STOP.UseVisualStyleBackColor = false;
            // 
            // ENG_MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1213, 687);
            this.Controls.Add(this.ENG_StartStopGroup);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.Name = "ENG_MainForm";
            this.Text = "SMS Engine for EasyICSTools";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ENG_StartStopGroup.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox ENG_StartStopGroup;
        private System.Windows.Forms.Button ENG_STOP;
        private System.Windows.Forms.Button ENG_START;
    }
}


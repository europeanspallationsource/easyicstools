﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Member.aspx.cs" Inherits="MembershipSite.Members.Member" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 133px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h2>Configuration Page for Members</h2>
        <br />
        <asp:Label ID="Label1" runat="server" Text="Welcome "></asp:Label>
        <asp:LoginName ID="LoginName" runat="server" />
        (<asp:LoginStatus ID="LoginStatus" runat="server" LogoutAction="Redirect" LogoutPageUrl="~/default.aspx" OnLoggingOut="LoginStatus_LoggingOut" />)
        <asp:Button ID="BTN_TurnPasswdChangeONOFF" runat="server" Font-Size="8pt" Height="20px" OnClick="BTN_TurnPasswdChangeONOFF_Click" Text="Change your password" Width="144px" />
        &nbsp;
        <asp:Button ID="BTN_TurnUserAddRemoveONOFF" runat="server" Font-Size="8pt" Height="20px" OnClick="BTN_TurnUserAddRemoveONOFF_Click" Text="Add remove User" Width="128px" Visible="False" />
        <br />
        <asp:Label ID="LBL_UserFullName" runat="server" Text="N/A"></asp:Label>
        <br />
        <br />
        <asp:Panel ID="FRM_UserMGMTPanel" runat="server" BackColor="#CCCCCC" Height="207px" Visible="False">
            <asp:Label ID="Label5" runat="server" Font-Size="18pt" Text="Administrator user management"></asp:Label>
            <br />
            <asp:Label ID="Label2" runat="server" Text="Add user"></asp:Label>
            &nbsp;
            <asp:TextBox ID="FRM_NewUserName" runat="server" Height="22px"></asp:TextBox>
            <asp:Label ID="Label3" runat="server" Text="with the password"></asp:Label>
            &nbsp;
            <asp:TextBox ID="FRM_NewUserPasswd" runat="server" placeholder="Enter your password"  TextMode="Password" Height="22px"></asp:TextBox>
            <asp:Label ID="Label7" runat="server" Text="User full name:"></asp:Label>
            <asp:TextBox ID="FRM_NewUserFullName" runat="server" Height="22px"></asp:TextBox>
            &nbsp;
            <asp:Button ID="BTN_AddUser" runat="server" OnClick="BTN_AddUser_Click" Text="ADD" Width="104px" />
            <asp:Label ID="Label6" runat="server" Font-Size="8pt" Text="* (The password has to be at least 5 character long! This software stores the password encrypted)"></asp:Label>
            <br />
            <asp:Label ID="Label4" runat="server" Text="Remove user"></asp:Label>
            &nbsp;
            <asp:TextBox ID="FRM_UserNameToRemove" runat="server" Height="22px"></asp:TextBox>
            <asp:Button ID="BTN_RemoveUser" runat="server" Text="REMOVE" Width="104px" OnClick="BTN_RemoveUser_Click" />
            <br />
            <asp:ListBox ID="FRM_UserList" runat="server" Height="74px" Width="401px" Font-Size="25px"></asp:ListBox>
            <br />
            <br />
        </asp:Panel>
        <br />
        <asp:Panel ID="FRM_PasswordChangePanel" runat="server" BackColor="#CCCCCC" Height="100px" Visible="False" Width="1290px">
            <asp:Label ID="Label10" runat="server" Font-Size="18pt" Text="Change login password"></asp:Label>
            <br />
            <asp:Label ID="Label8" runat="server" Text="Change Password"></asp:Label>
            &nbsp;<asp:TextBox ID="FRM_ChangePasswd1" runat="server" placeholder="Enter your new password"  TextMode="Password" Height="22px"></asp:TextBox>
            &nbsp;<asp:Label ID="Label9" runat="server" Text="repeat password"></asp:Label>
            &nbsp;
            <asp:TextBox ID="FRM_ChangePasswd2" runat="server" placeholder="Enter your new password"  TextMode="Password" Height="22px"></asp:TextBox>
            &nbsp;
            <asp:Button ID="BTN_ChangePasswd" runat="server" OnClick="BTN_ChangePasswd_Click" Text="CHANGE" Width="104px" />
        </asp:Panel>
        <asp:ListBox ID="FRM_RuleList" runat="server" AutoPostBack="true" Height="171px" OnSelectedIndexChanged="FRM_RuleList_SelectedIndexChanged" Width="890px" Font-Size="25px"></asp:ListBox>
        <asp:Button ID="BTN_RefreshRuleList" runat="server" BackColor="#CCCCCC" Height="41px" Text="REFRESH" Width="142px" OnClick="BTN_RefreshRuleList_Click" Font-Size="25px" />
        <br />
        <br />
        <asp:Button ID="BTN_ENABLE" runat="server" BackColor="Lime" Height="60px" Text="ENABLE" Width="250px" OnClick="BTN_ENABLE_Click" Font-Size="25px" />
&nbsp;
        <asp:Button ID="BTN_DISABLE" runat="server" BackColor="#FF6600" Height="60px" Text="DISABLE" Width="250px" OnClick="BTN_DISABLE_Click" Font-Size="25px" />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="BTN_TurnPVListONOFF" runat="server" Font-Size="25px" Height="48px" OnClick="BTN_TurnPVListONOFF_Click" Text="Show PV list" Width="186px" />
        <br />
        <br />
        <asp:ListBox ID="FRM_PVList" runat="server" AutoPostBack="true" Height="285px" Width="887px" Visible="False"></asp:ListBox>
        <asp:Button ID="BTN_RefreshPVList" runat="server" BackColor="#CCCCCC" Height="41px" Text="REFRESH" Width="139px" OnClick="BTN_RefreshPVList_Click" Visible="False" Font-Size="25px" />
        <br />
    </div>
    </form>
</body>
</html>

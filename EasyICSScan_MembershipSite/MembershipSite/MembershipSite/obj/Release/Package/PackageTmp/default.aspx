﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="MembershipSite._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center" lang="en" title="EasySMSforESS" style="background-color: #000000; background-image: url('Images/fusion.gif'); background-repeat: no-repeat; background-position: 50% 100px; width: 100%; height: 971px;"  >  
        <h2>
            <asp:Image ID="Image1" runat="server" Height="113px" ImageUrl="~/Images/ESS_Logo.png" Width="213px" />
        </h2>
        <br />
        <br />
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="ICS ToolKit" Font-Bold="True" Font-Size="30pt" ForeColor="#FFFF66" Font-Names="Euphorigenic"></asp:Label>
        <br />

        <asp:Label ID="Label3" runat="server" Text="v1.1.0" Font-Bold="True" Font-Size="8pt" ForeColor="#FFFF66"></asp:Label>

        <br />
        <br />
        <br />
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/LogInSMS.aspx" Font-Size="20pt" Font-Names="BankGothic Md BT" ForeColor="#FF9900">EasySMS Configuration</asp:HyperLink>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/LogInAlarm.aspx" Font-Size="20pt" Font-Names="BankGothic Md BT" ForeColor="#FF9900">EasyAlarm Configuration</asp:HyperLink>
        <br />
        <br />
        <br />
        <br />
        <asp:Label ID="Label1" runat="server" Text="Created by Miklos Boros 2018 -  ICS/ESS" ForeColor="#3399FF"></asp:Label>
        <br />
        <asp:HyperLink ID="SourceURL" runat="server" NavigateUrl="https://bitbucket.org/europeanspallationsource/easyicstools/src/master/" ForeColor="#99FF99">SourceCode@BitBucket</asp:HyperLink>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
    </form>
</body>
</html>

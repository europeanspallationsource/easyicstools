﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="MembershipSite.LogInPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center" style="background-color: #000000" title="EasySMSforESS_login">
        <h2>
            <asp:Image ID="Image1" runat="server" Height="121px" ImageUrl="~/Images/ESS_Logo.png" Width="224px" />
        </h2>
        <asp:Label ID="Label1" runat="server" Text="EasySMS for ESS LogIn Page" Font-Bold="True" Font-Overline="False" Font-Size="22pt" ForeColor="#FF9900"></asp:Label>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Please log in below to access the membership area." ForeColor="#FF9900"></asp:Label>
        <br />
        <br />
        <br />
        <br />
        <asp:Login ID="LoginControl" runat="server" 
            onauthenticate="LoginControl_Authenticate" Height="159px" Width="512px" ForeColor="#FF9900" DisplayRememberMe="False" Font-Size="25px">
        </asp:Login>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Created by Miklos Boros 2018 -  ICS/ESS" ForeColor="#FFCC00"></asp:Label>
        <asp:HyperLink ID="SourceURL" runat="server" NavigateUrl="https://bitbucket.org/europeanspallationsource/easyiocscan/src/master/">SourceCode@BitBucket</asp:HyperLink>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
    </form>
</body>
</html>

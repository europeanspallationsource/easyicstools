#region using
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using static MembershipSite.Members.AlarmManagement;

#endregion

namespace Goldtect.ASTreeViewDemo
{
	public partial class BeastTreeDeleteNodeProvider : PageBase
	{
		ASTreeViewAjaxReturnCode returnCode;
		string errorMessage = string.Empty;

		public string DeleteNodeValues
		{
			get
			{
				return HttpUtility.UrlDecode( Request.QueryString["deleteNodeValues"] );
			}
		}


        protected void Page_Load( object sender, EventArgs e )
		{
			if( string.IsNullOrEmpty( this.DeleteNodeValues ) || string.IsNullOrEmpty( this.DeleteNodeValues ) )
			{
				returnCode = ASTreeViewAjaxReturnCode.ERROR;
				return;
			}

			try
			{
				this.DeleteNodes();
			}
			catch( Exception ex )
			{
				this.returnCode = ASTreeViewAjaxReturnCode.ERROR;
				this.errorMessage = ex.Message;
			}
		}

		protected override void Render( HtmlTextWriter writer )
		{
			if( this.returnCode == ASTreeViewAjaxReturnCode.OK )
				writer.Write( (int)this.returnCode );
			else
				writer.Write( this.errorMessage );
		}

        private string NorthWindDBPath(string TableName)
        {

            return GlobalVariables.NorthWindDBDirectory + TableName + ".mdb";
        }

        public string NorthWindConnectionString(string TableName)
        {

            return @"Provider=Microsoft.Jet.OLEDB.4.0;Data source=" + NorthWindDBPath(TableName);
        }


        protected void DeleteNodes()
		{
			string sql = string.Format( "DELETE FROM [beasttree] WHERE [NodeID] in ( {0} )", this.DeleteNodeValues );
			OleDbHelper.ExecuteNonQuery( NorthWindConnectionString(GlobalVariables.CurrentTable)
				, CommandType.Text
				, sql );
		}
	}
}

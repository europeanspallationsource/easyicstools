﻿using HashLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MembershipSite.Members
{
    public partial class Member : System.Web.UI.Page
    {
        public static class GlobalVariables
        {
            public static int SelectedRule { get; set; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.Name == "miklosboros")
            {
                BTN_TurnUserAddRemoveONOFF.Visible = true;
            }
            else
            {
                //Put here VIP users
                FRM_UserMGMTPanel.Visible = false;
                if (User.Identity.Name == "krisztianloki")
                {
                }
            }
            SqlConnection conn = null;
            try
            {
                string sql = "select fullname from users where username = '"+ User.Identity.Name + "' ;";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        LBL_UserFullName.Text =  SqlData.GetString(0);
                    }
                }
                else
                {
                    LBL_UserFullName.Text = "N/A";
                }
                SqlData.Close();

                sql = "select username from users ;";
                cmd = new SqlCommand(sql, conn);
                SqlData = cmd.ExecuteReader();
                FRM_UserList.Items.Clear();
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        FRM_UserList.Items.Add(SqlData.GetString(0));
                    }
                }
                SqlData.Close();

                if (!Page.IsPostBack)
                {
                    //sql = "select rulename,statustext from monitor where username = '" + User.Identity.Name + "' ;";
                    //cmd = new SqlCommand(sql, conn);
                    //SqlData = cmd.ExecuteReader();
                    //FRM_RuleList.Items.Clear();
                    //if (SqlData.HasRows)
                    //{
                    //    while (SqlData.Read())
                    //    {
                    //        FRM_RuleList.Items.Add(new ListItem("[" + SqlData.GetString(1)+"]     "+SqlData.GetString(0), SqlData.GetString(0)));
                    //    }
                    //}
                    //SqlData.Close();
                    ReloadMonitorTable();
                    ReloadPVTable();
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }


        }


        private void ReloadMonitorTable()
        {

            SqlConnection conn = null;

            try
            {

                string sql = "select rulename,statustext,pvlist,lasttrigger from monitor where username = '" + User.Identity.Name + "' ;";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                FRM_RuleList.Items.Clear();
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        string rulename = SqlData.GetString(0);
                        string statustext = SqlData.GetString(1);
                        string pvlist = SqlData.GetString(2);
                        string lasttrigger = SqlData.GetString(3);
                        FRM_RuleList.Items.Add(new ListItem("[" + statustext + "]     " + rulename + "         Connected:(" + pvlist + ")        LastTrigger:(" + lasttrigger+")", rulename));
                    }
                }
                SqlData.Close();

            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }
        private void ReloadPVTable()
        {

            SqlConnection conn = null;

            try
            {

                string sql = "select pvname,vartype,lastvalue,lastread from pvlist where username = '" + User.Identity.Name + "' ;";
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader SqlData = cmd.ExecuteReader();
                FRM_PVList.Items.Clear();
                if (SqlData.HasRows)
                {
                    while (SqlData.Read())
                    {
                        string pvname = SqlData.GetString(0);
                        string vartype = SqlData.GetString(1);
                        string lastvalue = SqlData.GetString(2);
                        string lastread = SqlData.GetString(3);
                        FRM_PVList.Items.Add(new ListItem("[" + vartype + "]     " + pvname + "           Value:(" + lastvalue + ")            TimeStamp:(" + lastread + ")", pvname));
                    }
                }
                SqlData.Close();

            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }

        protected void LoginStatus_LoggingOut(object sender, LoginCancelEventArgs e)
        {

        }

        public bool IsAlphaNumeric(string text)
        {
            return Regex.IsMatch(text, "^[a-zA-Z0-9]+$");
        }
        protected void BTN_AddUser_Click(object sender, EventArgs e)
        {
            if (IsAlphaNumeric( FRM_NewUserName.Text))
            {
                if (FRM_NewUserPasswd.Text.Length > 4)
                {
                    SqlConnection conn = null;

                    try
                    {
                        string sql = "insert into users (username,password,fullname) VALUES ('" + FRM_NewUserName.Text.Trim() + "','" + Hasher.HashString(FRM_NewUserPasswd.Text.Trim()) + "','" + FRM_NewUserFullName.Text.Trim() + "');";

                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        conn.Open();
                        int count = (int)cmd.ExecuteNonQuery();
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('OK: " + count.ToString() + " rows affected.');", true);

                    }
                    catch (Exception ex)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                        // Log your error
                    }
                    finally
                    {
                        if (conn != null) conn.Close();
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('The password has to be at least 5 character long!');", true);
                }

            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('AddUser warning: Username contins non alphanumeric characters!');", true);
            }
        }

        protected void BTN_RemoveUser_Click(object sender, EventArgs e)
        {
            if (IsAlphaNumeric(FRM_NewUserName.Text))
            {
                    SqlConnection conn = null;

                    try
                    {
                        string sql = "delete from users where username = '" + FRM_NewUserName.Text.Trim() +";";

                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);


                        conn.Open();
                        int count = (int)cmd.ExecuteNonQuery();

                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('OK: " + count.ToString() + " rows affected.');", true);

                    }
                    catch (Exception ex)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                        // Log your error
                    }
                    finally
                    {
                        if (conn != null) conn.Close();
                    }

            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('AddUser warning: Username contins non alphanumeric characters!');", true);
            }

        }

        protected void BTN_TurnPasswdChangeONOFF_Click(object sender, EventArgs e)
        {
            FRM_PasswordChangePanel.Visible = !FRM_PasswordChangePanel.Visible;
        }

        protected void BTN_ChangePasswd_Click(object sender, EventArgs e)
        {
            if (FRM_ChangePasswd1.Text.Trim().Length>4)
            {
                if (FRM_ChangePasswd1.Text.Trim() == FRM_ChangePasswd2.Text.Trim())
                {
                    SqlConnection conn = null;

                    try
                    {
                        string sql = " update users set password = '" + Hasher.HashString(FRM_ChangePasswd1.Text.Trim())+ "' where username = '"+ User.Identity.Name + "';";

                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);


                        conn.Open();
                        int count = (int)cmd.ExecuteNonQuery();

                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('OK: " + count.ToString() + " rows affected.');", true);

                    }
                    catch (Exception ex)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                        // Log your error
                    }
                    finally
                    {
                        if (conn != null) conn.Close();
                    }

                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('The two password has to be match!');", true);
                    FRM_ChangePasswd1.Text = string.Empty; FRM_ChangePasswd2.Text = string.Empty;
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('The new password has to be at least 5 character long!');", true);
            }

        }

        protected void BTN_TurnUserAddRemoveONOFF_Click(object sender, EventArgs e)
        {
            FRM_UserMGMTPanel.Visible = !FRM_UserMGMTPanel.Visible;
        }

        protected void FRM_RuleList_SelectedIndexChanged(object sender, EventArgs e)
        {

            GlobalVariables.SelectedRule = FRM_RuleList.SelectedIndex;
        }

        protected void BTN_ENABLE_Click(object sender, EventArgs e)
        {
            if (FRM_RuleList.Items.Count > 0)
            {
                SqlConnection conn = null;

                try
                {

                    string sql = " update monitor set controlstate = 'ENABLING' where username = '" + User.Identity.Name + "' AND rulename ='" + FRM_RuleList.Items[GlobalVariables.SelectedRule].Value + "';";

                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(sql, conn);


                    conn.Open();
                    int count = (int)cmd.ExecuteNonQuery();

                    ReloadMonitorTable();
                    FRM_RuleList.SelectedIndex = GlobalVariables.SelectedRule;
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                    // Log your error
                }
                finally
                {
                    if (conn != null) conn.Close();
                }
            }
        }

        protected void BTN_DISABLE_Click(object sender, EventArgs e)
        {
            if (FRM_RuleList.Items.Count > 0)
            {
                SqlConnection conn = null;

                try
                {
                    string sql = " update monitor set controlstate = 'DISABLING' where username = '" + User.Identity.Name + "' AND rulename ='" + FRM_RuleList.Items[GlobalVariables.SelectedRule].Value + "';";

                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(sql, conn);


                    conn.Open();
                    int count = (int)cmd.ExecuteNonQuery();


                    ReloadMonitorTable();

                    FRM_RuleList.SelectedIndex = GlobalVariables.SelectedRule;

                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                    // Log your error
                }
                finally
                {
                    if (conn != null) conn.Close();
                }
            }
        }


        protected void BTN_RefreshRuleList_Click(object sender, EventArgs e)
        {

            ReloadMonitorTable();
        }

        protected void BTN_TurnPVListONOFF_Click(object sender, EventArgs e)
        {
            if (BTN_TurnPVListONOFF.Text == "Show PV list")
            {
                FRM_PVList.Visible = true;
                BTN_RefreshPVList.Visible = true;
                BTN_TurnPVListONOFF.Text = "Hide PV list";
            } else
            {
                FRM_PVList.Visible = false;
                BTN_RefreshPVList.Visible = false;
                BTN_TurnPVListONOFF.Text = "Show PV list";
            }
        }

        protected void BTN_RefreshPVList_Click(object sender, EventArgs e)
        {
            ReloadPVTable();
        }

    }
}

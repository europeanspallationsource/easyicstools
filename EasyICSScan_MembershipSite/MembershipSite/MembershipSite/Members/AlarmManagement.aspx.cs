﻿using Goldtect;
using Goldtect.ASTreeViewDemo;
using HashLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

namespace MembershipSite.Members
{
    public partial class AlarmManagement : System.Web.UI.Page
    {
        public class NonStaticClass
        {
            public void NonStaticFunction()
            {
                int i = 1;
                
            }
        }

            public static class GlobalVariables
        {
            public static int SelectedRule { get; set; }
            public static string NorthWindDBDirectory = @"C:\EasyDatabase\Template\";
            public static string CurrentTable = "Template";
            public static DataSet BeastTreeDataSet { get; set; }
            public static DataSet PVTreeDataSet { get; set; }
            public static string PVTreeSelectedID { get; set; }
            public static bool PVTreeContextClickSelectAll_OnClick { get; set; }
            public static bool PVTreeContextClickOpenBrowse_OnClick { get; set; }
        }

        [WebMethod]
        public static string PVTreeContextClickSelectAll(string inp)
        {
            GlobalVariables.PVTreeContextClickSelectAll_OnClick = true;
            NonStaticClass NewClass = new NonStaticClass();
            NewClass.NonStaticFunction();
            return inp;
        }

        [WebMethod]
        public static string PVTreeContextClickOpenBrowse(string inp)
        {
            GlobalVariables.PVTreeContextClickOpenBrowse_OnClick = true;
            return inp;
        }


        public string CsVariable = "hello world";

        protected override void Render(HtmlTextWriter writer)
        {
            if (Request.QueryString["t"] == "ajaxAdd")
            {
                string addNodeText = HttpUtility.UrlDecode(Request.QueryString["addNodeText"]);
                int parentNodeValue = int.Parse(HttpUtility.UrlDecode(Request.QueryString["parentNodeValue"]));

                string maxSql = "select max( NodeId ) from beasttree";
                int max = (int)OleDbHelper.ExecuteScalar(NorthWindConnectionString(GlobalVariables.CurrentTable), CommandType.Text, maxSql);
                int newId = max + 1;

//                string sql = string.Format(@"INSERT INTO beasttree( nodeid,  parentid, nodename, nodetype ) values( {0}, {1}, '{2}', '{3}')"
//                    , newId, parentNodeValue, addNodeText.Replace("'", "''"),"CONT");

//                int i = OleDbHelper.ExecuteNonQuery(NorthWindConnectionString(GlobalVariables.CurrentTable), CommandType.Text, sql);

                ASTreeViewNode root = new ASTreeViewNode("root");

                ASTreeViewLinkNode node = new ASTreeViewLinkNode(addNodeText, newId.ToString());
                node.NavigateUrl = "#";
                node.AdditionalAttributes.Add(new KeyValuePair<string, string>("onclick", "return false;"));
                node.NodeIcon = "~/Images/CONT_ICON.png";
                root.AppendChild(node);

                HtmlGenericControl ulRoot = new HtmlGenericControl("ul");
                astvMyTree1.TreeViewHelper.ConvertTree(ulRoot, root, false);
                foreach (Control c in ulRoot.Controls)
                    c.RenderControl(writer);

            }
            else
            {
                base.Render(writer);
            }
        }

        protected void LoginStatus_LoggingOut(object sender, LoginCancelEventArgs e)
        {

        }

        private string NorthWindDBPath(string TableName)        {

            return GlobalVariables.NorthWindDBDirectory + TableName + ".mdb";
        }

        public string NorthWindConnectionString(string TableName)
        {

            return @"Provider=Microsoft.Jet.OLEDB.4.0;Data source="+NorthWindDBPath(TableName);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.Name == "miklosboros")
            {
                BTN_TurnUserAddRemoveONOFF.Visible = true;
            }
            else
            {
                //Put here VIP users
                if (User.Identity.Name == "krisztianloki")
                {
                }
            }
            SqlConnection conn = null;
            
            try
            {
                if (!Page.IsPostBack)
                {
                    string sql = "select fullname from users where username = '" + User.Identity.Name + "' ;";
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    conn.Open();
                    SqlDataReader SqlData = cmd.ExecuteReader();
                    if (SqlData.HasRows)
                    {
                        while (SqlData.Read())
                        {
                            LBL_UserFullName.Text = SqlData.GetString(0);
                        }
                    }
                    else
                    {
                        LBL_UserFullName.Text = "N/A";
                    }
                    SqlData.Close();

                    sql = "select username from users ;";
                    cmd = new SqlCommand(sql, conn);
                    SqlData = cmd.ExecuteReader();
                    FRM_UserList.Items.Clear();
                    if (SqlData.HasRows)
                    {
                        while (SqlData.Read())
                        {
                            FRM_UserList.Items.Add(SqlData.GetString(0));
                        }
                    }
                    SqlData.Close();


                    sql = "select alarmtablename from beast ;";
                    cmd = new SqlCommand(sql, conn);
                    SqlData = cmd.ExecuteReader();
                    FRM_BeastList.Items.Clear();
                    if (SqlData.HasRows)
                    {
                        while (SqlData.Read())
                        {
                            FRM_BeastList.Items.Add(SqlData.GetString(0));
                        }
                    }
                    SqlData.Close();

                    GlobalVariables.CurrentTable = "Template";
                    LoadBeastAndPVsFromDB(GlobalVariables.CurrentTable);
                }
                this.astvMyTree1.ContextMenu.MenuItems.Clear();
                this.astvMyTree2.ContextMenu.MenuItems.Clear();
                this.astvMyTree1.EnableContextMenu = true;
                this.astvMyTree1.EnableContextMenuAdd = true;
                this.astvMyTree1.EnableContextMenuDelete = true;
                this.astvMyTree1.EnableContextMenuEdit = true;
                this.astvMyTree1.ContextMenu.MenuItems.Add(new ASContextMenuItem("Open PV properties", "alert('current value:' + " + this.astvMyTree1.ContextMenuClientID + ".getSelectedItem().parentNode.getAttribute('treeNodeValue')" + ");return false;", "otherevent"));
                this.astvMyTree1.ContextMenu.MenuItems.Add(new ASContextMenuItem("Custom Menu 2", "alert('current text:' + " + this.astvMyTree1.ContextMenuClientID + ".getSelectedItem().innerHTML" + ");return false;", "otherevent"));


                this.astvMyTree2.EnableContextMenu = true;
                this.astvMyTree2.EnableContextMenuAdd = false;
                this.astvMyTree2.EnableContextMenuDelete = false;
                this.astvMyTree2.EnableContextMenuEdit = false;
                this.astvMyTree2.EnableNodeIcon = true;
                this.astvMyTree2.EnableHorizontalLock = true;
                this.astvMyTree2.EnableContainerDragDrop = false;
                this.astvMyTree2.EnableFixedDepthDragDrop = true;
                this.astvMyTree2.EnableFixedParentDragDrop = true;

                //                    this.astvMyTree2.ContextMenu.MenuItems.Add(new ASContextMenuItem("Select all PV in this Container", @"var elem = document.getElementById('PVTreeSelectedID');  elem.value = "+this.astvMyTree2.ContextMenuClientID + ".getSelectedItem().parentNode.getAttribute('treeNodeValue');return false; ", "otherevent"));HF_PVTreeContextMenuID
                this.astvMyTree2.ContextMenu.MenuItems.Add(new ASContextMenuItem("Show File Upload Panel", @"var elem = document.getElementById('HF_PVTreeContextMenu_ShowBrowsePanel');  elem.value = " + this.astvMyTree2.ContextMenuClientID + ".getSelectedItem().parentNode.getAttribute('treeNodeValue'); document.getElementById('AlarmForm').submit(); return false; ", "otherevent"));
                this.astvMyTree2.ContextMenu.MenuItems.Add(new ASContextMenuItem("Select all PV in this Container", @"var elem = document.getElementById('HF_PVTreeContextMenu_SelectAll');  elem.value = " + this.astvMyTree2.ContextMenuClientID + ".getSelectedItem().parentNode.getAttribute('treeNodeValue'); document.getElementById('AlarmForm').submit(); return false; ", "otherevent"));


            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                // Log your error
            }
            finally
            {
                if (conn != null) conn.Close();
            }


        }

        private void LoadBeastAndPVsFromDB(string TableName)
        {
            
            if (File.Exists(NorthWindDBPath(TableName)))
            {
                try
                {
                   GlobalVariables.BeastTreeDataSet =  OleDbHelper.ExecuteDataset(NorthWindConnectionString(TableName), CommandType.Text, "select * from [BeastTree]");

                    ASTreeViewDataTableColumnDescriptor descriptertable = new ASTreeViewDataTableColumnDescriptor("NodeName"
                        , "NodeID"
                        , "ParentID");

                    this.astvMyTree1.DataSourceDescriptor = descriptertable;
                    this.astvMyTree1.DataSource = GlobalVariables.BeastTreeDataSet.Tables[0];                    
                    this.astvMyTree1.DataBind();




                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                    // Log your error
                }
                finally
                {
                }

                try
                {
                    GlobalVariables.PVTreeDataSet = OleDbHelper.ExecuteDataset(NorthWindConnectionString(TableName), CommandType.Text, "select * from [PVList]");

                    ASTreeViewDataTableColumnDescriptor descriptertable = new ASTreeViewDataTableColumnDescriptor("NodeName"
                        , "NodeID"
                        , "ParentID");
                    

                    this.astvMyTree2.DataSourceDescriptor = descriptertable;
                    this.astvMyTree2.DataSource = GlobalVariables.PVTreeDataSet.Tables[0];
                    this.astvMyTree2.DataBind();


                    PaintBeastTreeIcons(this.astvMyTree1.RootNode);
                    PaintPVTreeIcons(this.astvMyTree2.RootNode);

                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                    // Log your error
                }
                finally
                {
                }


            } else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('The database named:" + NorthWindDBPath(TableName) + " does not exists!');", true);
            }
        }

        public void SaveAlarmTreeinDB(ASTreeViewNode oParentNode)
        {

            string IconPath = string.Empty;
            bool FindNode = false;
            bool ParentMatch = false;
            bool NameMatch = false;

            foreach (DataTable table in GlobalVariables.BeastTreeDataSet.Tables)
            {
                foreach (DataRow row in table.Rows)
                {
                    foreach (DataColumn column in table.Columns)
                    {
                        if (FindNode == false)
                        {
                            string ColumnName = column.ColumnName.ToString();
                            if (ColumnName == "NodeID")
                            {
                                object item = row[column];
                                string ActualNodeID = oParentNode.NodeValue;
                                string ActualNodeText = oParentNode.NodeText;
                                if (item.ToString() == ActualNodeID)
                                {
                                    FindNode = true;
                                }
                            }
                        }

                        if (FindNode == true)
                        {
                            string ColumnName = column.ColumnName.ToString();
                            if (ColumnName == "ParentID")
                            {
                                object item = row[column];
                                if (oParentNode.ParentNode.NodeValue.ToString().Replace("root","0")==item.ToString())
                                {
                                    ParentMatch = true;
                                }

                            }
                        }
                        if  (FindNode == true)
                        {
                            string ColumnName = column.ColumnName.ToString();
                            if (ColumnName == "NodeName")
                            {
                                object item = row[column];
                                if (oParentNode.NodeText.ToString() == item.ToString())
                                {
                                    NameMatch = true;
                                }

                            }
                        }
                    }
                }
            }
            string NActualNodeID = oParentNode.NodeValue;
            string NActualNodeText = oParentNode.NodeText;
            if (oParentNode.NodeValue != "root")
            {
                if (FindNode == true)
                { //Node Exists

                    if (!ParentMatch)
                    { //Moved in the tree

                    }
                    if (!NameMatch)
                    { //Renamed in the tree

                    }
                }
                else
                { // New node

                }
            }

            // Start recursion on all subnodes.
            foreach (ASTreeViewNode oSubNode in oParentNode.ChildNodes)
            {
                SaveAlarmTreeinDB(oSubNode);
            }
        }

        public void PaintPVTreeIcons(ASTreeViewNode oParentNode)
        {

            string IconPath = string.Empty;
            int FindNode = 0;
            foreach (DataTable table in GlobalVariables.PVTreeDataSet.Tables)
            {
                foreach (DataRow row in table.Rows)
                {
                    foreach (DataColumn column in table.Columns)
                    {
                        if (FindNode == 0)
                        {
                            string ColumnName = column.ColumnName.ToString();
                            if (ColumnName == "NodeID")
                            {
                                object item = row[column];
                                string ActualNodeID = oParentNode.NodeValue;
                                string ActualNodeText = oParentNode.NodeText;
                                if (item.ToString() == ActualNodeID)
                                {
                                    FindNode = 1;
                                }
                            }
                        }

                            if (FindNode == 1)
                        {
                            string ColumnName = column.ColumnName.ToString();
                            if (ColumnName == "NodeType")
                            {
                                FindNode = 2;
                                object item = row[column];

                                switch (item.ToString())
                                {
                                    case "ROOT":
                                        IconPath = "~/Images/ROOT_ICON.png";
                                        break;
                                    case "DIR_PLCF":
                                        IconPath = "~/Images/PLCF_ICON.png";
                                        break;
                                    case "DIR_TEXT":
                                        IconPath = "~/Images/TEXT_ICON.png";
                                        break;
                                    case "DIR_EXCEL":
                                        IconPath = "~/Images/EXCEL_ICON.png";
                                        break;
                                    case "DIR_XML":
                                        IconPath = "~/Images/XML_ICON.png";
                                        break;
                                    case "DEV":
                                        IconPath = "~/Images/DEVICE_ICON.png";
                                        break;
                                    case "PV":
                                        IconPath = "~/Images/PV_ICON.png";
                                        break;


                                }

                            }
                        }
                    }
                }
            }
            oParentNode.NodeIcon = IconPath;

            // Start recursion on all subnodes.
            foreach (ASTreeViewNode oSubNode in oParentNode.ChildNodes)
            {
                PaintPVTreeIcons(oSubNode);
            }
        }

        public void PaintBeastTreeIcons(ASTreeViewNode oParentNode)
        {

            string IconPath = string.Empty;
            int FindNode = 0;
            foreach (DataTable table in GlobalVariables.BeastTreeDataSet.Tables)
            {
                foreach (DataRow row in table.Rows)
                {
                    foreach (DataColumn column in table.Columns)
                    {
                        if (FindNode == 0)
                        {
                            string ColumnName = column.ColumnName.ToString();
                            if (ColumnName == "NodeID")
                            {
                                object item = row[column];
                                string ActualNodeID = oParentNode.NodeValue;
                                string ActualNodeText = oParentNode.NodeText;
                                if (item.ToString() == ActualNodeID)
                                {
                                    FindNode = 1;
                                }
                            }
                        }

                        if (FindNode == 1)
                        {
                            string ColumnName = column.ColumnName.ToString();
                            if (ColumnName == "NodeType")
                            {
                                FindNode = 2;
                                object item = row[column];

                                switch (item.ToString())
                                {
                                    case "ROOT":
                                        IconPath = "~/Images/ROOT_ICON.png";
                                        break;
                                    case "DEV":
                                        IconPath = "~/Images/DEVICE_ICON.png";
                                        break;
                                    case "CONT":
                                        IconPath = "~/Images/CONT_ICON.png";
                                        break;
                                    case "PV":
                                        IconPath = "~/Images/PV_ICON.png";
                                        break;
                                }

                            }
                        }
                    }
                }
            }
            oParentNode.NodeIcon = IconPath;

            // Start recursion on all subnodes.
            foreach (ASTreeViewNode oSubNode in oParentNode.ChildNodes)
            {
                PaintBeastTreeIcons(oSubNode);
            }
        }

        private void GenerateTree()
        {
            try
            {
                DataSet ds = OleDbHelper.ExecuteDataset(NorthWindConnectionString("Template"), CommandType.Text, "select * from [Alarms]");

                ASTreeViewDataTableColumnDescriptor descriptertable = new ASTreeViewDataTableColumnDescriptor("AlarmPVName"
                    , "AlarmID"
                    , "ParentID");

                this.astvMyTree1.DataSourceDescriptor = descriptertable;
                this.astvMyTree1.DataSource = ds.Tables[0];
                this.astvMyTree1.DataBind();

                if (this.astvMyTree1.RootNode.ChildNodes.Count > 0)
                    this.astvMyTree1.RootNode.ChildNodes[0].EnableDeleteContextMenu = false;

            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                // Log your error
            }
            finally
            {
            }




            XmlDocument doc2 = new XmlDocument();
            string path2 = Server.MapPath("~/PVList.xml");
            doc2.Load(path2);

            ASTreeViewXMLDescriptor descripter = new ASTreeViewXMLDescriptor();





            this.astvMyTree2.DataSourceDescriptor = descripter;
            this.astvMyTree2.DataSource = doc2;
            this.astvMyTree2.DataBind();
            //this.astvMyTree2.EnableHorizontalLock = true;
            //this.astvMyTree2.EnableContainerDragDrop = true;
            //this.astvMyTree2.EnableFixedDepthDragDrop = true;
            //this.astvMyTree2.EnableFixedParentDragDrop = false;
            
            this.astvMyTree2.EnableContextMenuAdd = false;
            this.astvMyTree2.EnableContextMenuEdit = false;
            this.astvMyTree2.EnableContextMenuDelete = false;
            this.astvMyTree2.EnableCheckbox = false;

            this.astvMyTree1.ContextMenu.MenuItems.Add(new ASContextMenuItem("Custom Menu 1", "alert('current value:' + " + this.astvMyTree1.ContextMenuClientID + ".getSelectedItem().parentNode.getAttribute('treeNodeValue')" + ");return false;", "otherevent"));
            this.astvMyTree1.ContextMenu.MenuItems.Add(new ASContextMenuItem("Custom Menu 2", "alert('current text:' + " + this.astvMyTree1.ContextMenuClientID + ".getSelectedItem().innerHTML" + ");return false;", "otherevent"));

        }

        protected void BTN_AddUser_Click(object sender, EventArgs e)
        {
            if (IsAlphaNumeric(FRM_NewUserName.Text))
            {
                if (FRM_NewUserPasswd.Text.Length > 4)
                {
                    SqlConnection conn = null;

                    try
                    {
                        string sql = "insert into users (username,password,fullname) VALUES ('" + FRM_NewUserName.Text.Trim() + "','" + Hasher.HashString(FRM_NewUserPasswd.Text.Trim()) + "','" + FRM_NewUserFullName.Text.Trim() + "');";

                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        conn.Open();
                        int count = (int)cmd.ExecuteNonQuery();
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('OK: " + count.ToString() + " rows affected.');", true);

                    }
                    catch (Exception ex)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                        // Log your error
                    }
                    finally
                    {
                        if (conn != null) conn.Close();
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('The password has to be at least 5 character long!');", true);
                }

            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('AddUser warning: Username contins non alphanumeric characters!');", true);
            }
        }

        protected void BTN_RemoveUser_Click(object sender, EventArgs e)
        {
            if (IsAlphaNumeric(FRM_NewUserName.Text))
            {
                SqlConnection conn = null;

                try
                {
                    string sql = "delete from users where username = '" + FRM_NewUserName.Text.Trim() + ";";

                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(sql, conn);


                    conn.Open();
                    int count = (int)cmd.ExecuteNonQuery();

                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('OK: " + count.ToString() + " rows affected.');", true);

                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                    // Log your error
                }
                finally
                {
                    if (conn != null) conn.Close();
                }

            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('AddUser warning: Username contins non alphanumeric characters!');", true);
            }

        }

        protected void BTN_TurnPasswdChangeONOFF_Click(object sender, EventArgs e)
        {
            FRM_PasswordChangePanel.Visible = !FRM_PasswordChangePanel.Visible;
        }

        protected void BTN_ChangePasswd_Click(object sender, EventArgs e)
        {
            if (FRM_ChangePasswd1.Text.Trim().Length > 4)
            {
                if (FRM_ChangePasswd1.Text.Trim() == FRM_ChangePasswd2.Text.Trim())
                {
                    SqlConnection conn = null;

                    try
                    {
                        string sql = " update users set password = '" + Hasher.HashString(FRM_ChangePasswd1.Text.Trim()) + "' where username = '" + User.Identity.Name + "';";

                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);


                        conn.Open();
                        int count = (int)cmd.ExecuteNonQuery();

                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('OK: " + count.ToString() + " rows affected.');", true);

                    }
                    catch (Exception ex)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                        // Log your error
                    }
                    finally
                    {
                        if (conn != null) conn.Close();
                    }

                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('The two password has to be match!');", true);
                    FRM_ChangePasswd1.Text = string.Empty; FRM_ChangePasswd2.Text = string.Empty;
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('The new password has to be at least 5 character long!');", true);
            }

        }

        protected void BTN_TurnUserAddRemoveONOFF_Click(object sender, EventArgs e)
        {
            FRM_UserMGMTPanel.Visible = !FRM_UserMGMTPanel.Visible;
        }

        public bool IsAlphaNumeric(string text)
        {
            return Regex.IsMatch(text, "^[a-zA-Z0-9]+$");
        }

        protected void FRM_GetTree_Click(object sender, EventArgs e)
        {
        }

        protected void FRM_OpenClientText_Click(object sender, EventArgs e)
        {
            if (FRM_IFAUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(FRM_IFAUpload.FileName);
                    byte[] fileInput = new byte[FRM_IFAUpload.PostedFile.ContentLength];
                    FRM_IFAUpload.FileContent.Read(fileInput, 0, FRM_IFAUpload.PostedFile.ContentLength);
                    List<string> myLines = null;
                    List<string> myPVs = new List<string>();
                    myLines = Encoding.UTF8.GetString(fileInput).Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    
                    this.astvMyTree1.EnableCustomizedNodeIcon = true;
                    for (int i = 0;i< myLines.Count;i++)
                    {
                        if (myLines[i] == "DEVICE")
                        {
                            if (myPVs.Count > 0)
                            {
                                ASTreeViewNode NewNode = new ASTreeViewNode(myLines[i + 1]);
                                NewNode.NodeIcon = "~/Images/d.png";
                                this.astvMyTree1.RootNode.ChildNodes.Add(NewNode);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Upload status: The file could not be uploaded.The following error occured: " + ex.Message+" ');", true);
                }
            }
        }


        protected void BTN_HideBrowsePanel_Click(object sender, EventArgs e)
        {
            FileImportPanel.Visible = false;
        }


        protected void HF_PVTreeContextMenu_ShowBrowsePanel_ValueChanged(object sender, EventArgs e)
        {
            FileImportPanel.Visible = true;
        }

        protected void HF_PVTreeContextMenu_SelectAll_ValueChanged(object sender, EventArgs e)
        {
            int NodeID = Convert.ToInt32(HF_PVTreeContextMenu_SelectAll.Value);
            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('"+HF_PVTreeContextMenu_SelectAll.Value.ToString()+"');", true);

        }

        protected void FRM_AddNewBeastConfig_Click(object sender, EventArgs e)
        {
            if (FRM_BeastName.Text != string.Empty)
            {
                SqlConnection conn = null;

                try
                {

                    string sql = "select alarmtablename from beast where alarmtablename='"+FRM_BeastName.Text+"'";
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    conn.Open();
                    int count = 0;
                    SqlDataReader SqlData = cmd.ExecuteReader();
                    if (SqlData.HasRows)
                    {
                        while (SqlData.Read())
                        {
                            count++;
                        }
                    }
                    SqlData.Close();
                    if (count == 0)
                    {

                        sql = " insert into beast (alarmtablename,user_accesslevel,approve_status,approve_date,approve_user,approve_message,current_tag,lastmodified_date,lastmodified_user) values ('" + FRM_BeastName.Text + "','','','','','','','" + DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture) + "','" + User.Identity.Name + "') ;";
                        cmd = new SqlCommand(sql, conn);


                        count = (int)cmd.ExecuteNonQuery();


                        sql = "select alarmtablename from beast ;";
                        cmd = new SqlCommand(sql, conn);
                        SqlData = cmd.ExecuteReader();
                        FRM_BeastList.Items.Clear();
                        if (SqlData.HasRows)
                        {
                            while (SqlData.Read())
                            {
                                FRM_BeastList.Items.Add(SqlData.GetString(0));
                            }
                        }
                        SqlData.Close();

                        GlobalVariables.CurrentTable = "Template";
                        LoadBeastAndPVsFromDB(GlobalVariables.CurrentTable);
                    } else
                    {

                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('This BEAST name already exists in the DataBase!');", true);
                    }
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                    // Log your error
                }
                finally
                {
                    if (conn != null) conn.Close();
                }


            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('To add a new BEAST configuration enter a valid name for [BEAST Name]');", true);

            }
        }

        protected void FRM_RenameBeastConfig_Click(object sender, EventArgs e)
        {
            if (FRM_BeastList.Text != string.Empty)
            {
                if (FRM_BeastName.Text != string.Empty)
                {
                    SqlConnection conn = null;

                    try
                    {

                        string sql = "select alarmtablename from beast where alarmtablename='" + FRM_BeastName.Text + "'";
                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MembershipSiteConStr"].ConnectionString);
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        conn.Open();
                        int count = 0;
                        SqlDataReader SqlData = cmd.ExecuteReader();
                        if (SqlData.HasRows)
                        {
                            while (SqlData.Read())
                            {
                                count++;
                            }
                        }
                        SqlData.Close();
                        if (count == 0)
                        {

                            sql = " update beast  set alarmtablename='" + FRM_BeastName.Text + "', lastmodified_date='" + DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture) + "',lastmodified_user='" + User.Identity.Name + "' where alarmtablename='" + FRM_BeastList.Text + "';";
                            cmd = new SqlCommand(sql, conn);


                            count = (int)cmd.ExecuteNonQuery();


                            sql = "select alarmtablename from beast ;";
                            cmd = new SqlCommand(sql, conn);
                            SqlData = cmd.ExecuteReader();
                            FRM_BeastList.Items.Clear();
                            if (SqlData.HasRows)
                            {
                                while (SqlData.Read())
                                {
                                    FRM_BeastList.Items.Add(SqlData.GetString(0));
                                }
                            }
                            SqlData.Close();

                            GlobalVariables.CurrentTable = "Template";
                            LoadBeastAndPVsFromDB(GlobalVariables.CurrentTable);
                        }
                        else
                        {

                            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('This BEAST name already exists in the DataBase, can not rename to this!');", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + ex.Message + "');", true);
                        // Log your error
                    }
                    finally
                    {
                        if (conn != null) conn.Close();
                    }


                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('To change the BEAST configuration name, enter a valid name for [BEAST Name]');", true);

                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('To change BEAST configuration name, select one from the ComboBox!');", true);

            }

        }

        protected void FRM_BeastList_SelectedIndexChanged(object sender, EventArgs e)
        {
            FRM_BeastName.Text = FRM_BeastList.Text;
        }
    }
}

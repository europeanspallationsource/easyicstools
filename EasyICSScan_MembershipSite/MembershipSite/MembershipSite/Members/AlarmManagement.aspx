﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlarmManagement.aspx.cs" Inherits="MembershipSite.Members.AlarmManagement" %>

<%@ Register Assembly="Goldtect.ASTreeView" Namespace="Goldtect" TagPrefix="Goldtect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>EasyAlarm Manager</title>
	<link href="<%=ResolveUrl("~/Scripts/astreeview/astreeview.css")%>" type="text/css" rel="stylesheet" />
	<link href="<%=ResolveUrl("~/Scripts/astreeview/contextmenu.css")%>" type="text/css" rel="stylesheet" />
	
	<script src="<%=ResolveUrl("~/Scripts/astreeview/astreeview.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/astreeview/contextmenu.min.js")%>" type="text/javascript"></script>
    <script type="text/javascript" src="cValidation.js"></script>
    <style type="text/css">
        .auto-style1 {
            width: 317px;
        }
        #AlarmForm {
            height: 482px;
            width: 1257px;
        }
        .auto-style2 {
            height: 44px;
        }
        .auto-style3 {
            width: 166px;
            height: 44px;
        }
        .auto-style4 {
            width: 625px;
        }
        .auto-style5 {
            height: 410px;
            width: 1320px;
        }
        .auto-style6 {
            height: 13px;
        }
        .auto-style7 {
            height: 7px;
        }
    </style>
	<script type="text/javascript">
		//parameter must be "elem"
		function addedHandler( elem ){
		}
		
		//parameter must be "elem"
		function editedHandler( elem ){
		}
		
		//parameter must be "val"
		function deletedHandler( val ){
		}
	</script>

</head>
<body>
    <form id="AlarmForm" runat="server" enctype="multipart/form-data" class="auto-style5">
    <div>
        <h2 class="auto-style6">EasyAlarm management for Members<asp:Image ID="EPICS" runat="server" Height="90px" ImageAlign="Right" ImageUrl="~/Images/EPICS_Logo.png" Width="190px" />
            <asp:Image ID="ESS" runat="server" Height="90px" ImageAlign="Right" ImageUrl="~/Images/ESS_Logo.png" Width="190px" />
        </h2>
        <asp:Label ID="Label1" runat="server" Text="Welcome "></asp:Label>
        <asp:LoginName ID="LoginName" runat="server" />
        (<asp:LoginStatus ID="LoginStatus" runat="server" LogoutAction="Redirect" LogoutPageUrl="~/default.aspx" OnLoggingOut="LoginStatus_LoggingOut" />)
        <asp:Button ID="BTN_TurnPasswdChangeONOFF" runat="server" Font-Size="8pt" Height="20px" OnClick="BTN_TurnPasswdChangeONOFF_Click" Text="Change your password" Width="144px" />
        &nbsp;
        <asp:Button ID="BTN_TurnUserAddRemoveONOFF" runat="server" Font-Size="8pt" Height="20px" OnClick="BTN_TurnUserAddRemoveONOFF_Click" Text="Add remove User" Width="128px" Visible="False" />
        <br />
        <asp:Image ID="Image5" runat="server" Height="36px" ImageUrl="~/Images/user.jpg" Width="36px" />
        <asp:Label ID="LBL_UserFullName" runat="server" Text="N/A"></asp:Label>
        <br />
        <br />
        <asp:Panel ID="FRM_UserMGMTPanel" runat="server" BackColor="#CCCCCC" Height="197px" Visible="False">
            <asp:Label ID="Label5" runat="server" Font-Size="18pt" Text="Administrator user management"></asp:Label>
            <br />
            <asp:Label ID="Label2" runat="server" Text="Add user"></asp:Label>
            &nbsp;
            <asp:TextBox ID="FRM_NewUserName" runat="server" Height="22px"></asp:TextBox>
            <asp:Label ID="Label3" runat="server" Text="with the password"></asp:Label>
            &nbsp;
            <asp:TextBox ID="FRM_NewUserPasswd" runat="server" placeholder="Enter your password"  TextMode="Password" Height="22px"></asp:TextBox>
            <asp:Label ID="Label7" runat="server" Text="User full name:"></asp:Label>
            <asp:TextBox ID="FRM_NewUserFullName" runat="server" Height="22px"></asp:TextBox>
            &nbsp;
            <asp:Button ID="BTN_AddUser" runat="server" OnClick="BTN_AddUser_Click" Text="ADD" Width="104px" />
            <asp:Label ID="Label6" runat="server" Font-Size="8pt" Text="* (The password has to be at least 5 character long! This software stores the password encrypted)"></asp:Label>
            <br />
            <asp:Label ID="Label4" runat="server" Text="Remove user"></asp:Label>
            &nbsp;
            <asp:TextBox ID="FRM_UserNameToRemove" runat="server" Height="22px"></asp:TextBox>
            <asp:Button ID="BTN_RemoveUser" runat="server" Text="REMOVE" Width="104px" OnClick="BTN_RemoveUser_Click" />
            <br />
            <asp:ListBox ID="FRM_UserList" runat="server" Height="98px" Width="401px" Font-Size="15px"></asp:ListBox>
            <br />
            <br />
        </asp:Panel>
        <asp:Panel ID="FRM_PasswordChangePanel" runat="server" BackColor="#CCCCCC" Height="71px" Visible="False">
            <asp:Label ID="Label10" runat="server" Font-Size="18pt" Text="Change login password"></asp:Label>
            <br />
            <asp:Label ID="Label8" runat="server" Text="Change Password"></asp:Label>
            &nbsp;<asp:TextBox ID="FRM_ChangePasswd1" runat="server" placeholder="Enter your new password"  TextMode="Password" Height="22px"></asp:TextBox>
            &nbsp;<asp:Label ID="Label9" runat="server" Text="repeat password"></asp:Label>
            &nbsp;
            <asp:TextBox ID="FRM_ChangePasswd2" runat="server" placeholder="Enter your new password"  TextMode="Password" Height="22px"></asp:TextBox>
            &nbsp;
            <asp:Button ID="BTN_ChangePasswd" runat="server" OnClick="BTN_ChangePasswd_Click" Text="CHANGE" Width="104px" />
        </asp:Panel>
        </div>
        <asp:Panel ID="MenuPanel" runat="server" BackColor="#99CCFF" Height="93px">
            <asp:Label ID="Label11" runat="server" Font-Size="18pt" Text="EasyAlarm main menu"></asp:Label>
            <br />
            &nbsp;&nbsp;<asp:Image ID="Image3" runat="server" Height="36px" ImageUrl="~/Images/manage.png" Width="36px" />
&nbsp;<asp:Button ID="BNT_ManageBeast" runat="server" Height="37px" Text="Manage Beast configuration " Width="227px" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Image ID="Image4" runat="server" Height="37px" ImageUrl="~/Images/merge.png" Width="35px" />
&nbsp;<asp:Button ID="BNT_MergeBeast" runat="server" Height="37px" Text="Merge Beast Configurations" Width="227px" />
        </asp:Panel>
        <asp:Panel ID="FileImportPanel" runat="server" BackColor="#FFFFCC" Height="111px" Visible="False">
            <div align="right" class="auto-style7">
            <asp:Button ID="BTN_HideBrowsePanel" runat="server" Text="Hide" OnClick="BTN_HideBrowsePanel_Click" />
             </div>
            <asp:Label ID="Label12" runat="server" Text="Open and overwrite rigth side of the tree (PVs from Text File)"></asp:Label>
            <br />
            <asp:FileUpload ID="FRM_TextFileUpload" runat="server" Width="288px" />
            &nbsp;
            <asp:Button ID="BTN_OpenTextFile" runat="server" Text="Open from TextFile" />
            <br />
            <asp:Label ID="Label13" runat="server" Text="Open and overwrite rigth side of the tree (PVs from PLCFactory)"></asp:Label>
            <br />
            <asp:FileUpload ID="FRM_IFAUpload" runat="server" Width="288px" />
            &nbsp;
            <asp:Button ID="BTN_OpenIFA" runat="server" Text="Open from PLCFactory" Width="165px" />
        </asp:Panel>
        <asp:Panel ID="SelectBeastPanel" runat="server" BackColor="#CCFFFF" Height="94px">
            <asp:Label ID="Label14" runat="server" Text="Open and overwrite left side of the tree (Alarm Tree)"></asp:Label>
            <br />
            <asp:DropDownList ID="FRM_BeastList" runat="server" Font-Size="20px" Height="29px" Width="306px" AutoPostBack="True" OnSelectedIndexChanged="FRM_BeastList_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;
            <br />
            <asp:Label ID="Label15" runat="server" Text="BEAST Name"></asp:Label>
            &nbsp;<asp:TextBox ID="FRM_BeastName" runat="server" Width="218px"></asp:TextBox>
            &nbsp;<asp:Button ID="FRM_AddNewBeastConfig" runat="server" OnClick="FRM_AddNewBeastConfig_Click" Text="Add new empty BEAST" Width="160px" />
            &nbsp;<asp:Button ID="FRM_RenameBeastConfig" runat="server" OnClick="FRM_RenameBeastConfig_Click" Text="Rename BEAST" Width="121px" />
        </asp:Panel>
                <asp:ScriptManager ID="smMain" runat="server" EnablePageMethods="true" />
        <asp:HiddenField ID="HF_PVTreeContextMenu_ShowBrowsePanel" ClientIDMode="Static" runat="server" OnValueChanged="HF_PVTreeContextMenu_ShowBrowsePanel_ValueChanged" />
        <asp:HiddenField ID="HF_PVTreeContextMenu_SelectAll" ClientIDMode="Static" runat="server" OnValueChanged="HF_PVTreeContextMenu_SelectAll_ValueChanged" />
        <table>
            <tr>
                <th class="auto-style2">
                    <h3>Alarm tree</h3>
                </th>
                <th class="auto-style3">
                    <h3 class="auto-style4">PV Tree</h3>
                </th>
            </tr>
            <tr valign="top">
                <td width="300">
                    <Goldtect:ASTreeView ID="astvMyTree1" 							
                            runat="server"
							BasePath="~/Scripts/astreeview/"
							DataTableRootNodeValue="0"
							EnableRoot="false" 
							EnableNodeSelection="true" 
							EnableCheckbox="false" 
							EnableDragDrop="true" 
							EnableTreeLines="true"
							EnableNodeIcon="true"
							EnableCustomizedNodeIcon="true"
							AutoPostBack="false"
							EnableDebugMode="false"
							EnableContextMenu="true"
                            RelatedTrees="astvMyTree2" 
                            RootNodeText="BEAST Tree" 
   							EnableAjaxOnEditDelete="true"
							EditNodeProvider="~/ASTreeViewRenameNodeHandler.aspx"
							DeleteNodeProvider="~/BeastTreeDeleteNodeProvider.aspx"
							AddNodeProvider="~/Members/AlarmManagement.aspx"
							AdditionalAddRequestParameters="{'t':'ajaxAdd'}"
							AddNodePromptDefaultValue="New Node" 
							AddNodePromptMessage="Hello, please add a new node:"
							AddNodeDataValueProvider="return prompt('new ndoe? under:' + elem.getAttribute('treeNodeValue'),'new node name');"
							OnNodeAddedScript="addedHandler(elem)"	
							OnNodeEditedScript="editedHandler(elem)"
							OnNodeDeletedScript="deletedHandler(val)" Height="228px" Width="611px"  />

                </td>
                <td class="auto-style1">
                    <Goldtect:ASTreeView ID="astvMyTree2" 
                        runat="server" 
                        BasePath="~/Scripts/astreeview/" 
                        DataTableRootNodeValue="0" 
                        EnableCheckbox="false" 
                        EnableContextMenuAdd="false" 
                        EnableCustomizedNodeIcon="true" 
                        EnableDebugMode="false" 
                        EnableDragDrop="true" 
                        EnableNodeIcon="true" 
                        EnableNodeSelection="true" 
                        EnableRoot="false" 
                        EnableTreeLines="true" 
                        Height="227px" 
                        RelatedTrees="astvMyTree1" 
                        RootNodeText="PV list" 
                        style="margin-right: 0px" 
                        Width="635px" 
                        EnableMultiLineEdit="True" BorderStyle="Dotted" BorderWidth="3px" />
                </td>
            </tr>
        </table>
        <p>
            &nbsp;</p>
    </form>
</body>
</html>
